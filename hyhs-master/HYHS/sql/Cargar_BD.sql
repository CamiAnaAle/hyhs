﻿USE tpproyecto;


/* IDIOMAS*/ 
INSERT INTO `idiomas` (`descripcion`,`estado`) VALUES ('Ingles', true);
INSERT INTO `idiomas` (`descripcion`,`estado` ) VALUES ('Español', true);


/* MEDIO PAGO */

INSERT INTO `medioPago` (`descripcion`,`estado` )VALUES ('Tarjeta de Credito', true);
INSERT INTO `medioPago` (`descripcion`,`estado` )VALUES ('Efectivo', true);
INSERT INTO `medioPago` (`descripcion`,`estado` )VALUES ('Tarjeta de Debito', true);
INSERT INTO `medioPago` (`descripcion`,`estado` )VALUES ('Fiado', true);


/* CATEGORIA */

INSERT INTO `categoria` (`descripcion`,`estado` )VALUES ('Tintura y Decoloración', true);
INSERT INTO `categoria` (`descripcion`,`estado` )VALUES ('Accesorios', true);
INSERT INTO `categoria` (`descripcion`,`estado` )VALUES ('Aparatos electrónicos', true);
INSERT INTO `categoria` (`descripcion`,`estado` )VALUES ('Shampoos y Acondicionadores', true);
INSERT INTO `categoria` (`descripcion`,`estado` )VALUES ('Kits', true);
INSERT INTO `categoria`(`descripcion`,`estado` )VALUES ('Tratamiento para el cabello', true);
INSERT INTO `categoria`(`descripcion`,`estado` )VALUES ('Crema para peinar', true);


/* PRODUCTO */

INSERT INTO `producto`  ( `id`,  `nombre`,  `descripcion` ,   `stock` ,    `precioCosto`,  `precioVenta` , `id_categoria` , `estado`)   
VALUES (1,'Maquina cortadora de pelo', 'Wahl Senior Cordless',5,11340,11340,3,true);

INSERT INTO `producto`  ( `id`,  `nombre`,  `descripcion` ,   `stock` ,    `precioCosto`,   `precioVenta` , `id_categoria` , `estado`)   
VALUES (2,'Gorro metalizado termico', 'para tintura o decoloración',10,610,610,1,true);

INSERT INTO `producto`  (  `nombre`,  `descripcion` ,   `stock` ,    `precioCosto`,   `precioVenta` , `id_categoria` , `estado`)  
VALUES ('CREMA ENJUAGUE MATIZADOR', 'MATIZADOR GRIS SALOON IN 250ml',30,145,145,4,true);

INSERT INTO `producto` (`nombre`,  `descripcion` , `stock` , `precioCosto`,   `precioVenta` , `id_categoria` , `estado`)   
VALUES ('ACEITE Natural De ALMENDRAS', 'marca OTOWIL por 9ml',30,30.38,30.38,6,true);




/* PAIS */

INSERT INTO `pais`  (`descripcion`) VALUES ('Argentina');
INSERT INTO `pais`  (`descripcion`) VALUES ('Mexico');
INSERT INTO `pais`  (`descripcion`) VALUES ('Colombia');
INSERT INTO `pais`  (`descripcion`) VALUES ('Estados Unidos');


/* PROVINCIA */

INSERT INTO `provincia`  (`descripcion`, `id_pais` ) VALUES ('Buenos Aires', 1);
INSERT INTO `provincia`  (`descripcion`, `id_pais` ) VALUES ('Misiones', 1);
INSERT INTO `provincia`  (`descripcion`, `id_pais` ) VALUES ('Quintana Roo', 2);
INSERT INTO `provincia`  (`descripcion`, `id_pais` ) VALUES ('California', 4);
INSERT INTO `provincia`  (`descripcion`, `id_pais` ) VALUES ('Bogotá', 3);



/* LOCALIDAD */

INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`)  
VALUES (1613,'Polvorines', 1);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (1619,'Garin', 1);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (1663,'San Miguel', 1);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (1667,'Tortuguitas', 1);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (1667,'Iguazú', 2);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (3370,'Iguazú', 2);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (74526,'Cancún', 3);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (111941,'Bolivar', 5);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (91605,'Hollywood', 4);
INSERT INTO `localidad`  ( `cp`, `descripcion`, `id_provincia`) 
VALUES (1611,'Don torcuato', 1);

/* ROL */

INSERT INTO `rol`  (`descripcion`, `estado`) VALUES ('Administrador',true);
INSERT INTO `rol`  (`descripcion`, `estado`) VALUES ('Encargado de Local',true);
INSERT INTO `rol`  (`descripcion`, `estado` ) VALUES ('Profesional',true);
INSERT INTO `rol`  (`descripcion`, `estado`) VALUES ('Contador',true);
INSERT INTO `rol`  (`descripcion`, `estado` ) VALUES ('Recepcionista',true);
INSERT INTO `rol`  (`descripcion`, `estado` ) VALUES ('Cliente',true);




/* Tipo cliente */

INSERT INTO `tipocliente` (`descripcion`,`estado`) VALUES ('VIP',true);
INSERT INTO `tipocliente` (`descripcion`,`estado`) VALUES ('Comun',true);
INSERT INTO `tipocliente` (`descripcion`,`estado`)  VALUES ('Activo',true);
INSERT INTO `tipocliente` (`descripcion`,`estado`)  VALUES ('Moroso',true);
INSERT INTO `tipocliente` (`descripcion`,`estado`)  VALUES ('Propenso a faltar',true);








/* Cliente */

INSERT INTO `cliente` (`dni`, `nombre`,   `apellido`,   `fechaNto`,  `direccion`,  `telefono`, `celular`, `email`,   `fechaActualizacionDeuda` , `totalAdeudado`,  `id_localidad`, `id_tipo`, `cantTurnoSolicitado`, `cantPresentes`, `cantAusentes`,`cantCanceladosCliente`, `cantCanceladosEmpresa`, `cantPuntos`, `estado` ) 
VALUES (24534434,'Claudia','Acuña', CAST('1972-06-02' AS Date),'mitre 252','654633565','1559192245','claudiaacua@hotmail.com',CAST('2019-10-06' AS Date),0,2,1,0,0,0,0,0,0,true);

INSERT INTO `cliente` (`dni`, `nombre`,   `apellido`,   `fechaNto`,  `direccion`,  `telefono`, `celular`, `email`,   `fechaActualizacionDeuda` , `totalAdeudado`,  `id_localidad`, `id_tipo`, `cantTurnoSolicitado`, `cantPresentes`, `cantAusentes`,`cantCanceladosCliente`, `cantCanceladosEmpresa`, `cantPuntos`, `estado` ) 
VALUES (93956943,'Max','Huamani', CAST('1992-09-15' AS Date),'Felix Frias 39','65654564','1129120121','maxhuama@gmail.com',CAST('2019-10-06' AS Date),0,10,1,0,0,0,0,0,0,true);

INSERT INTO `cliente` (`dni`, `nombre`,   `apellido`,   `fechaNto`,  `direccion`,  `telefono`, `celular`, `email`,   `fechaActualizacionDeuda` , `totalAdeudado`,  `id_localidad`, `id_tipo`, `cantTurnoSolicitado`, `cantPresentes`, `cantAusentes`,`cantCanceladosCliente`, `cantCanceladosEmpresa`, `cantPuntos`, `estado` ) 
VALUES (3383016,'Gabriel','Alegre', CAST('2001-09-10' AS Date),'Mitre 22','32432423','1187219181','gaby_2001@gmail.com',CAST('2019-10-06' AS Date),0,2,1,0,0,0,0,0,0,true);


INSERT INTO `cliente` (`dni`, `nombre`,   `apellido`,   `fechaNto`,  `direccion`,  `telefono`, `celular`, `email`,   `fechaActualizacionDeuda` , `totalAdeudado`,  `id_localidad`, `id_tipo`, `cantTurnoSolicitado`, `cantPresentes`, `cantAusentes`,`cantCanceladosCliente`, `cantCanceladosEmpresa`, `cantPuntos`, `estado` ) 
VALUES (93987012,'Lucia','Quispe', CAST('1991-06-22' AS Date),'Felix frias 200','65465464','1118619102','luucy1991@gmail.com',CAST('2019-10-06' AS Date),0,10,1,0,0,0,0,0,0,true);




/*ESTADO PROMOCION*/
INSERT INTO `estadoPromocion`  (`descripcion`, `estado` ) VALUES ('cerrado',true);
INSERT INTO `estadoPromocion`  (`descripcion`, `estado` ) VALUES ('abierto',true);


/*TIPO DE PROMOCION*/
INSERT INTO `tipoPromocion`  (`descripcion`, `estado`) VALUES ('Promociones de precio',true);
INSERT INTO `tipoPromocion`  (`descripcion`, `estado`) VALUES ('Promociones de regalo',true);
INSERT INTO `tipoPromocion`  (`descripcion`, `estado`) VALUES ('Puntos',true);


/*PROMOCIONES*/

INSERT INTO `promocion`  (`descripcion` , `precio`, `puntos`, `fechaDesde`, `fechaHasta`,    `id_Estado`,    `id_tipo` ) 
VALUES ('Botox + corte de pelo',500,0,CAST('2019-09-29' AS Date),CAST('2019-10-03' AS Date),true,1);

INSERT INTO `promocion`  (`descripcion` , `precio`, `puntos`, `fechaDesde`, `fechaHasta`,    `id_Estado`,    `id_tipo` ) 
VALUES ('Alisado Permanente 2x1',1400,0,CAST('2019-09-30' AS Date),CAST('2019-10-03' AS Date),true,1);



/* EMPLEADOS */



INSERT `empleado` (`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ,
  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado`,
    `id_localidad`)
    VALUES (86547548, 'Ignacio', 'González', CAST('1990-09-18' AS Date), '25 de Mayo 2762', '', 1141508921, 'ignaciongonzales@gmail.com', CAST('2019-09-29' AS Date), 16000, true, 1);


INSERT `empleado` (`dni`, `nombre`, `apellido`, `fecha_nacimiento`, `direccion` , `telefono`,`celular`,`email`, `fecha_ingreso`, `sueldo_inicial`,`estado`,`id_localidad`) 
VALUES ( 6921213,'Ramiro', 'Acosta', CAST('1985-07-15' AS Date), 'España 1552', '', '02478-15403467', 'RamiAcosta1983@gmail.com', CAST('2019-09-29' AS Date), 15000 , true, 3);

INSERT  `empleado` (`dni`, `nombre`,  `apellido`,   `fecha_nacimiento`,`direccion` ,
 `telefono`,   `celular`,   `email`,  `fecha_ingreso`,     `sueldo_inicial`,    `estado`,   `id_localidad`)
VALUES (78455685, 'Camila', 'Rodriguez', CAST('1978-06-15' AS Date),  'Alamos 880',  '', 1170539239,'camirodriguez@hotmail.com',CAST('2019-09-29' AS Date), 30444 , true, 4);


INSERT `empleado` (`dni`, `nombre`, `apellido`, `fecha_nacimiento`, `direccion`, `telefono`,    `celular`,    `email`,     `fecha_ingreso`,     `sueldo_inicial`,    `estado`,
    `id_localidad`) 
    VALUES (87654218, 'Max', 'Alegre', CAST('1992-09-15' AS Date), 'Napoles 3801', '4772605', '1126154854', 'max@hotmail.com', CAST('2019-09-29' AS Date),50000, true, 3);


INSERT `empleado` (`dni`,   `nombre`,   `apellido`,   `fecha_nacimiento`,   `direccion`,  `telefono`,  `celular`,  `email`,   `fecha_ingreso`,   `sueldo_inicial`,  `estado`,  `id_localidad`)
 VALUES (27565787, 'Sofia', 'Perez', CAST('1992-09-15' AS Date), 'Manuel Estrada 1120', '',1131211872, 'sofia1992@yahoo.com.ar', CAST('2019-09-29' AS Date), 12902, true,10);



INSERT `empleado` (`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ,
  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado`,
    `id_localidad`) VALUES ('33261892', 'Federico', 'Fronteras', CAST('1990-09-18' AS Date), 'Manuel Estrada 102','',1131232212, 'fede1990@gmail.com', CAST('2019-09-29' AS Date), 19000, true,10);


INSERT `empleado` (`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ,
  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado`,
    `id_localidad`) VALUES ('32826059','Ramona', 'Aguilar', CAST('1985-02-10' AS Date), 'Napoles 120','',1192282212, 'ramonita1985@gmail.com', CAST('2019-09-29' AS Date), 20000, true,3)
;

INSERT `empleado` (`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ,
  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado`,
    `id_localidad`) VALUES ('21776632','Marcela', 'Aguirre', CAST('1973-02-10' AS Date), 'Napoles 200','',1192282212, 'marcela_aguirre@gmail.com', CAST('2019-09-29' AS Date), 20000, true,3)
;



INSERT `empleado` (`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ,
  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado`,
    `id_localidad`) VALUES ('37102010','Lautaro', 'Verbauwede', CAST('1994-04-10' AS Date), '25 de Mayo 271','',1112485512, 'lauty_verbauwede@gmail.com', CAST('2019-09-29' AS Date), 20000, true,1);




/* SERVICIOS */

INSERT INTO `servicio` ( `descripcion`,    `precio`,    `tiempo_estimado`,    `estado`) 
VALUES ('Corte Mujer',250, CAST('0:30' AS TIME),true);

INSERT INTO `servicio` ( `descripcion`,    `precio`,    `tiempo_estimado`,    `estado`) 
VALUES ('Botox',750,CAST('1:00' AS TIME),true);


INSERT INTO `servicio` ( `descripcion`,    `precio`,    `tiempo_estimado`,    `estado`) 
VALUES ( 'Plastificado',800, CAST('1:00' AS TIME),true);

INSERT INTO `servicio` (  `descripcion`,   `precio`,   `tiempo_estimado`,   `estado`) 
VALUES ( 'Alisado Permanente',1200, CAST('2:00' AS TIME),true);

INSERT INTO `servicio` ( `descripcion`,   `precio`,  `tiempo_estimado`,   `estado`) 
VALUES ( 'Corte de hombre',150, CAST('0:30' AS TIME),true);



/* TIPO EGRESOS */


INSERT INTO  `tipoEgresos` ( `descripcion`,  `estado`) VALUES ('viatico',true);
INSERT INTO  `tipoEgresos`( `descripcion`,  `estado`) VALUES ('sueldo',true);
INSERT INTO  `tipoEgresos`( `descripcion`,  `estado`) VALUES ('gastos de compra de insumo',true);






/* DIAS */
INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Lunes',true);

INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Martes',true);

INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Miercoles',true);

INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Jueves',true);

INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Viernes',true);

INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Sabado',true);
 
 INSERT INTO `dia` (`descripcion`,`estado`)
 VALUES ('Domingo',true);







/*ROL EMPLEADO*/
INSERT INTO `rol_empleado` (`id_rol`, `legajo`,  `fechaHasta`, `estado`)
 VALUES (3, '1',null,true);

INSERT INTO `rol_empleado` (`id_rol`, `legajo`,  `fechaHasta`, `estado`)
 VALUES (3, '2',null,true);



/* Estado detalle de turno */
/*
INSERT INTO `estadoDetalleTurno`(`descripcion`,  `estado`) VALUES ('Vigente',true);
INSERT INTO `estadoDetalleTurno`(`descripcion`,  `estado`) VALUES ('Pagado',true);
INSERT INTO `estadoDetalleTurno`( `descripcion`,  `estado`) VALUES ('No pagado',true);
INSERT INTO `estadoDetalleTurno`( `descripcion`,  `estado`) VALUES ('Cancelado',true);
*/


/* SERVICIO EMPLEADO */


INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES (1, 1, true);

INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES (1, 2, true);

INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES ('1', '3', true);

INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES (2, 1, true);


INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES (2, 2, true);

INSERT INTO `servicio_empleado` (`legajo`,  `id_servicio`, `estado`) 
VALUES (2, 5, true);


INSERT INTO `tpproyecto`.`sucursal` (`nombre`, `direccion`, `telefono`, `email`, `id_localidad`, `estado`) VALUES ('Sucursal Haedo', 'Haedo 123', '66667777', 'sucursalHaedo@hyh.com', '1', '1');
INSERT INTO `tpproyecto`.`sucursal` (`nombre`, `direccion`, `telefono`, `email`, `id_localidad`, `estado`) VALUES ('Sucursal pacheco', 'Pacheco 123', '77778888', 'sucursalpacheco@hyh.com', '1', '1');

INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '1', '08:00', '22:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '2', '08:00', '22:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '3', '08:00', '22:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '4', '08:00', '22:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '5', '08:00', '22:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '6', '09:00', '23:00', '1');
INSERT INTO `tpproyecto`.`sucursalpordias` (`id_sucursal`, `id_dia`, `horaInicio`, `horaFin`, `estado`) VALUES ('1', '7', '09:00', '23:00', '1');

INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '1', '08:00', '15:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '2', '08:00', '16:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '3', '08:00', '17:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '4', '08:00', '18:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '5', '16:00', '22:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '6', '12:00', '22:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('1', '1', '7', '18:00', '22:00', '1');

INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('2', '1', '6', '12:00', '22:00', '1');
INSERT INTO `tpproyecto`.`dias_laborales` (`legajo`, `id_sucursal`, `id_dia`, `horaDesde`, `horaHasta`, `estado`) VALUES ('2', '1', '7', '13:00', '22:00', '1');

INSERT INTO `tpproyecto`.`estadoasistencia` (`id`, `descripcion`, `estado`) VALUES ('1', 'Presente', '1');
INSERT INTO `tpproyecto`.`estadoasistencia` (`id`, `descripcion`, `estado`) VALUES ('2', 'Ausente', '1');

INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-07', '08:00', '15:00', '1', '1', '1', '1', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-08', '08:00', '16:00', '1', '1', '1', '2', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-09', '08:00', '17:00', '1', '1', '1', '3', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-10', '08:00', '18:00', '1', '1', '1', '4', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-11', '16:00', '20:00', '1', '1', '1', '5', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-12', '17:00', '23:00', '1', '1', '1', '6', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-13', '18:00', '22:00', '1', '1', '1', '7', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-12', '12:00', '22:00', '1', '2', '1', '6', '1');
INSERT INTO `tpproyecto`.`asistencia` (`fecha`, `horaDesde`, `horaHasta`, `idEstado`, `legajo`, `id_sucursal`, `id_dia`, `estado`) VALUES ('2019-10-13', '13:00', '22:00', '1', '2', '1', '7', '1');

/*Primer turno reserva un servicio */
INSERT INTO `tpproyecto`.`turno` (`fecha`, `tiempoTotal`, `precioTotal`, `pagoRealizado`, `id_medioPago`, `id_cliente`, `id_sucursal`, `id_legajo`, `plataReal`, `plataCanjeada`, `puntosCanjeados`, `estado`) VALUES ('2019-12-07', '00:30:00', '250', '1', '1', '3', '1', '3', '250', '0', '0', '1');
INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_asistencia`, `id_turno`, `id_servicio`, `precio`) VALUES ('21:00', '21:30', '00:30', '00:25', '1', '6', '1', '1', '250');


/* Segundo turno, reserva dos servicios*/
INSERT INTO `tpproyecto`.`turno` (`fecha`, `tiempoTotal`, `precioTotal`, `pagoRealizado`, `id_medioPago`, `id_cliente`, `id_sucursal`, `id_legajo`, `plataReal`, `plataCanjeada`, `puntosCanjeados`, `puntosTotalGanados`, `estado`) VALUES ('2019-12-07', '01:30', '1000.00', '1', '1', '1', '1', '3', '1000', '0', '0', '2', '1');

INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_asistencia`, `id_turno`, `id_servicio`, `precio`) VALUES ('18:00', '18:30', '00:30', '00:25', '1', '6', '1', '1', '250');
INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_asistencia`, `id_turno`, `id_servicio`, `precio`) VALUES ('18:30', '19:30', '01:00', '01:00', '1', '6', '1', '2', '750');

/* tercer turno, compra dos productos*/
INSERT INTO `tpproyecto`.`turno` (`fecha`, `tiempoTotal`, `precioTotal`, `pagoRealizado`, `id_medioPago`, `id_cliente`, `id_sucursal`, `id_legajo`, `plataReal`, `plataCanjeada`, `puntosCanjeados`, `puntosTotalGanados`,`estado`) VALUES ('2019-12-07', '00:00', '11950', '1', '1', '2', '1', '3', '11950', '0', '0', '24','1');
INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_turno`, `id_producto`, `precio`) VALUES ('00:00', '00:00', '00:00', '00:00', '1', '2', '1', '11340');
INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_turno`, `id_producto`, `precio`) VALUES ('00:00', '00:00', '00:00', '00:00', '1', '2', '2', '610');

/* cuarto turno, compra un producto y un servicio*/
INSERT INTO `tpproyecto`.`turno` (`fecha`, `tiempoTotal`, `precioTotal`, `pagoRealizado`, `id_medioPago`, `id_cliente`, `id_sucursal`, `id_legajo`, `plataReal`, `plataCanjeada`, `puntosCanjeados`, `estado`) VALUES ('2019-12-07', '00:30:00', '395', '1', '1', '3', '1', '3', '395', '0', '0', '1');

INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_asistencia`, `id_turno`, `id_servicio`, `precio`) VALUES ('13:00', '13:30', '00:30', '00:30', '1', '8', '3', '1', '250');
INSERT INTO `tpproyecto`.`detalleturno` (`horaInicio`, `horaFin`, `tiempo_estimado`, `tiempo_real`, `estado`, `id_turno`, `id_producto`, `precio`) VALUES ('00:00', '00:00', '00:00', '00:00', '1', '3', '3', '145');

