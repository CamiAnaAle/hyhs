DROP DATABASE IF EXISTS `TPProyecto`;
CREATE DATABASE `TPProyecto`;
USE TPProyecto;

create table `idiomas`
(
	`id` int(2) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(50) not null,
    `estado` bool,
    primary key(`id`)
);

create table `pais`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    primary key(`id`)
);
create table `provincia`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `id_pais` int(4) not null,
    foreign key(`id_pais`) references `pais`(`id`),
    primary key(`id`)
);


create table `localidad`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
	`cp` int(4),
    `descripcion` varchar(30) not null,
    `id_provincia` int(4) not null,
    primary key(`id`),
    foreign key(`id_provincia`) references `provincia`(`id`)
);


create table `empleado`
(
	`legajo` int (6)  NOT NULL AUTO_INCREMENT,
	`dni` int(8) not null,
    `nombre` varchar(30) not null,
    `apellido` varchar(30) not null,
    `fecha_nacimiento` date not null,
    `direccion` varchar(30) not null,
    `telefono` varchar(30),
    `celular` varchar(30),
    `email` varchar(30),
     `fecha_ingreso` date not null,
     `sueldo_inicial` long,
    `id_localidad` int(4) not null,
    `estado` bool,
	primary key(`legajo`),
    foreign key(`id_localidad`) references `localidad`(`id`)
    
);


create table `rol`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(50) not null,
    `estado` bool,
    primary key(`id`)
   
);

create table `rol_empleado`
(
	`id_rol` int(5) not null,
    `legajo` int(6) not null,
    `fechaHasta` date null,
    `estado` bool,
    primary key(`legajo`,`id_rol`),
    foreign key(`legajo`) references `empleado`(`legajo`),
    foreign key(`id_rol`) references `rol`(`id`)
);

create table `user`
(
	`username` int(6) not null,
	`password` int(6) not null,
    `id_rol` int(5) not null,
    `legajo` int(6) not null,
    `id_idioma` int(2) not null,
    `estado` bool,
     primary key(`username`),
     foreign key (`id_rol`,`legajo`) references `rol_empleado`(`id_rol`,`legajo`) ,
     foreign key(`id_idioma`) references `idiomas`(`id`)
);
    

create table `sucursal`
(
	`id` int(6) NOT NULL AUTO_INCREMENT,
    `nombre` varchar(30) not null,
    `direccion` varchar(30) not null,
    `telefono` varchar(30),
    `email` varchar(30),
    `id_localidad` int(5) not null,
    `estado` bool,
    primary  key(`id`)
);



create table `estadoAsistencia`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
    
    
);


create table `dia`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
    
);


create table  `asistencia`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
    `fecha` date,
    `horaDesde` time,
    `horaHasta` time,
    `idEstado` int(4) not null,
    `legajo` int(5) not null,
    `id_sucursal` int (6) not null,
    `id_dia` int (4) not null,
    `estado` boolean,
    primary key(`id`),
    foreign key(`id_sucursal`) references `sucursal`(`id`),    
    foreign key(`idEstado`) references `estadoAsistencia`(`id`),
    foreign key(`id_dia`) references `dia`(`id`),
    foreign key(`legajo`) references `empleado`(`legajo`)
);



create table `servicio`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `precio`long,
    `tiempo_estimado` time,
    `estado` bool,
    primary key(`id`)
);

create table `servicio_empleado`
(
	`legajo` int(5) not null ,
    `id_servicio` int(5) not null,
    `estado` bool,
    primary key(`legajo`,`id_servicio`),
    foreign key(`legajo`) references `empleado`(`legajo`),
    foreign key(`id_servicio`) references `servicio`(`id`)
);




create table `tipoPromocion`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
	`descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
);

create table `estadoPromocion`
(
	`id` int(4) NOT NULL AUTO_INCREMENT,
	`descripcion` varchar(30) not null,
    `estado` bool,
     primary key(`id`)
);

create table `promocion`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `precio`long not null,
	`puntos`int(6) not null,
    `fechaDesde` date not null,
    `fechaHasta` date not null,
    `id_Estado` int(4) not null,
    `id_tipo` int(4) not null,
     primary key(`id`),
     foreign key(`id_Estado`) references `estadoPromocion`(`id`),
     foreign key(`id_tipo`) references  `tipoPromocion`(`id`)
);


create table `detallePromocion`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `id_servicio` int(6) not null,
    `id_promocion` int(5) not null ,
    `estado` bool,
    primary key(`id`),
    foreign key(`id_promocion`) references `promocion`(`id`),
    foreign key(`id_servicio`) references `servicio`(`id`)
);
 create table `tipoCliente`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion`varchar(30) not null,
    `estado` bool,
    primary key(`id`)
 );

create table `cliente`
(
    `id` int(6) NOT NULL AUTO_INCREMENT,
    `dni` int(8) not null,
    `nombre`varchar(30) not null,
    `apellido`varchar(30) not null,
    `fechaNto` date,
    `direccion`varchar(20) not null,
    `telefono`varchar(30) not null,
    `celular`varchar(30),
    `email`varchar(30) not null,
    `fechaActualizacionDeuda` date,
    `totalAdeudado` long,
    `id_localidad`int(5) not null,
	`id_tipo`int(5) not null,
    `cantTurnoSolicitado` int,
    `cantPresentes` int,
    `cantAusentes` int,
    `cantCanceladosCliente` int,
    `cantCanceladosEmpresa` int,
    `cantPuntos` int(6),
    `estado` bool,
    primary key(`id`),
    
    foreign key(`id_tipo`) references `tipoCliente`(`id`),
    
    foreign key(`id_localidad`) references `localidad`(`id`)
    
);

create table `envioMail`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion`varchar(30) not null,
    `fecha` date not null,
    `id_cliente` int(5) not null,
    `tipoMail` int(10) not null,
    `estado` bool,
     primary key(`id`),
    foreign key(`id_cliente`) references `cliente`(`id`)
    
);

/*
create table `estadoDetalleTurno`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
);
*/

 create table `medioPago`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
 );
 
create table `turno`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `fecha` date,
    `tiempoTotal` time,
    `precioTotal` decimal(12,2),
    `pagoRealizado` bool,
    `id_medioPago` int(5),
    `id_cliente` int(5),
    `id_sucursal` int(5),
    `id_legajo` int(5),
    `plataReal`  decimal(12,2),
    `plataCanjeada`  decimal(12,2),
    `puntosCanjeados` int(5),
    `puntosTotalGanados` int(5),
    `estado` bool,
    primary key(`id`),
    foreign key(`id_mediopago`) references `medioPago`(`id`),
    foreign key(`id_cliente`) references `cliente`(`id`),
    foreign key(`id_sucursal`) references `sucursal`(`id`),
    foreign key(`id_legajo`) references `empleado`(`legajo`)
    
);



create table `categoria`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30) not null,
    `estado` bool,
    primary key(`id`)
);
 create table `producto`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `nombre` varchar(30),
    `descripcion` varchar(30),
    `stock` int,
    `precioCosto` long,
    `precioVenta` long,
    `id_categoria` int(5),
    `estado` bool,
    primary key(`id`),
    foreign key (`id_categoria`) references `categoria`(`id`)
 );

create table `detalleTurno`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `horaInicio` time,
    `horaFin` time,
    `tiempo_estimado` time null,
    `tiempo_real` time null,
    `estado` int(5) not null,
    `id_asistencia` int(5) null,
    `id_turno` int(5) not null,
    `id_servicio` int(5) null,
    `id_promocion` int(5) null,
    `id_producto` int(5) null,
    `precio` long,
	primary key(`id`),
    foreign key(`id_asistencia`) references `asistencia`(`id`),
	foreign key( `id_turno`) references `turno`(`id`),
    foreign key( `id_servicio`) references `servicio`(`id`),
    foreign key( `id_promocion`) references `promocion`(`id`),
    foreign key(`id_producto`) references  `producto`(`id`)
  /*  foreign key(`id_estado`) references  `estadoDetalleTurno`(`id`)*/
    
    
    
);

 
 create table `historialPuntos`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `fecha` date,
    `cantPuntosActual` int(5),
    `cantPuntosTransacc` int(5),
    `cantPuntosTotal` int(5),
    `id_turno` int(5),
    `id_cliente` int(5),
    primary key(`id`),
    foreign key (`id_turno`) references `turno`(`id`),
    foreign key (`id_cliente`) references `cliente`(`id`)
    
 );
 
 
 create table `egresos`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `id_empleado` int(5),
    `fecha` date,
    `importeTotal` long,
    `id_sucursal` int(5), 
    `estado` bool,
    primary key(`id`),
     foreign key (`id_empleado`) references `empleado`(`legajo`),
    foreign key (`id_sucursal`) references `sucursal`(`id`)
    
 );
 
 
 
  create table `tipoEgresos`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30),
    `estado` bool,
    primary key(`id`)
 );
 
 create table `detalleEgresos`
 (
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `descripcion` varchar(30),
    `precio` long,
	`estado` bool,
    `id_egresos` int(5),
    `id_tipoEgreso` int(5),
    primary key(`id`),
    foreign key (`id_egresos`) references `egresos`(`id`),
    foreign key (`id_tipoEgreso`) references `tipoEgresos`(`id`)
    
 );
 
create table `sucursalPorDias`
(
	`id_sucursal` int(5),
    `id_dia` int(5),
    `horaInicio` time,
    `horaFin` time,
    `estado` bool,
    primary key(`id_sucursal`,`id_dia`),
    foreign key(`id_sucursal`) references `sucursal`(`id`),
    foreign key(`id_dia`) references `dia`(`id`)
    
);

create table `dias_laborales`
(
	`legajo` int (6),
    `id_sucursal` int (6),
    `id_dia` int (7),
    `horaDesde` time,
    `horaHasta` time,
    `estado` bool,
    primary key(`legajo`,`id_sucursal`,`id_dia`),
    foreign key(`legajo`) references `empleado`(`legajo`),
    foreign key(`id_sucursal`,`id_dia`) references `sucursalPorDias`(`id_sucursal`,`id_dia`)
);

create table `excepcionesCalendario`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
	`fecha` date,
    `id_sucursal` int(5),
    `estado` bool,
    primary key(`id`),
    
    foreign key(`id_sucursal`) references `sucursal`(`id`)
);


create table `configuracionGral`
(
	`id` int(5) NOT NULL AUTO_INCREMENT,
    `AcantPlata` long,
    `AcantPuntos` int(5),
    `BcantPlata` long,
    `BcantPuntos` int(5),
	`estado` bool,
    `limites_pesos_fiables` long,
    `limites_pesos_fiablesVIP` long ,
    primary key(`id`)
    
);
