package modelo;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.modelo.clases.Asistencia;
import com.modelo.clases.CategoriaProducto;
import com.modelo.clases.Cliente;
import com.modelo.clases.DetallePromocion;
import com.modelo.clases.DetalleTurno;
import com.modelo.clases.Empleado;
import com.modelo.clases.EstadoPromocion;
import com.modelo.clases.Localidad;
import com.modelo.clases.Pais;
import com.modelo.clases.Producto;
import com.modelo.clases.Promocion;
import com.modelo.clases.Provincia;
import com.modelo.clases.Servicio;
import com.modelo.clases.Sucursal;
import com.modelo.clases.TipoCliente;
import com.modelo.clases.TipoPromocion;
import com.modelo.clases.Turno;

import com.modelo.clases.EstadoCliente;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DetalleTurnoDAO;
import persistencia.dao.interfaz.EmpleadoDAO;
import persistencia.dao.interfaz.EstadoPromocionDAO;
import persistencia.dao.interfaz.AsistenciaDAO;
import persistencia.dao.interfaz.CategoriaProductoDAO;
import persistencia.dao.interfaz.ClienteDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.ProductoDAO;
import persistencia.dao.interfaz.PromocionDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.ServicioDAO;
import persistencia.dao.interfaz.SucursalDAO;
import persistencia.dao.interfaz.TipoClienteDAO;
import persistencia.dao.interfaz.TipoPromocionDAO;
import persistencia.dao.interfaz.TurnoDAO;
import persistencia.dao.interfaz.DetallePromocionDAO;

public class HyHs {

	private ClienteDAO cliente_DAO;
	private LocalidadDAO localidad_DAO;
	private PaisDAO pais_DAO;
	private ProvinciaDAO provincia_DAO;
	private TipoClienteDAO tipoCliente_DAO;
	private PromocionDAO promocion_DAO;
	private ProductoDAO producto_DAO;
	private SucursalDAO sucursal_DAO;
	private ServicioDAO servicio_DAO;
	private EstadoPromocionDAO estadopromocion_DAO;
	private TipoPromocionDAO tipopromocion_DAO;
	private CategoriaProductoDAO categoriaproducto_DAO;
	private EmpleadoDAO empleado_DAO;
	private TurnoDAO turno_DAO;
	private AsistenciaDAO asistencia_DAO;
	private DetallePromocionDAO detallePromo_DAO;
	private DetalleTurnoDAO detalleTurno_DAO;

	public HyHs(DAOAbstractFactory metodo_persistencia) {
		this.detalleTurno_DAO = metodo_persistencia.createDetalleTurnoDAO();
		this.asistencia_DAO = metodo_persistencia.createAsistenciaDAO();
		this.cliente_DAO = metodo_persistencia.createClienteDAO();
		this.localidad_DAO = metodo_persistencia.createLocalidadDAO();
		this.pais_DAO = metodo_persistencia.createPaisDAO();
		this.provincia_DAO = metodo_persistencia.createProvinciaDAO();
		this.tipoCliente_DAO = metodo_persistencia.createTipoClienteDAO();
		this.promocion_DAO = metodo_persistencia.createPromocionDAO();
		this.producto_DAO = metodo_persistencia.createProductoDAO();
		this.sucursal_DAO = metodo_persistencia.createSucursalDAO();
		this.servicio_DAO = metodo_persistencia.createServicioDAO();
		this.estadopromocion_DAO = metodo_persistencia.createEstadoPromocionDAO();
		this.tipopromocion_DAO = metodo_persistencia.createTipoPromocionDAO();
		this.categoriaproducto_DAO = metodo_persistencia.createCategoriaProductoDAO();
		this.empleado_DAO = metodo_persistencia.createEmpleadoDAO();
		this.turno_DAO = metodo_persistencia.createTurnoDAO();
		this.detallePromo_DAO = metodo_persistencia.createDetallePromoDAO();
	}

	public Cliente obtenerUnCliente(int id) {
		return this.cliente_DAO.readOne(id);
	}

	public Asistencia obtenerAsistenciaEmpleado(Date fecha, int legajo, int idSucursal) {
		return this.asistencia_DAO.readAsistenciaEmpleado(fecha, legajo, idSucursal);
	}

	public Asistencia obtenerAsistencia_Empleado_Dia_Sucursal(Date fecha, int legajo, int idSucursal) {
		return this.asistencia_DAO.readAsistencia_Empleado_Dia_Sucursal(fecha, legajo, idSucursal);
	}

	public List<DetalleTurno> obtenerDetalleTurno_Asistencia(Asistencia a) {
		return this.detalleTurno_DAO.readAllDetalleTurno_Asistencia(a);
	}

	public boolean existeCliente(int id) {
		return this.cliente_DAO.existe(id);
	}

	public List<Pais> readAllPaises() {
		return this.pais_DAO.readAll();
	}

	public List<Sucursal> readAllSucursales() {
		return this.sucursal_DAO.readAll();
	}

	public List<TipoCliente> readAllTipoCliente() {
		return tipoCliente_DAO.readAll();
	}

	public int readUltimoIdCliente() {
		return cliente_DAO.readUltimoId();
	}

	public List<Localidad> readAllLocalidadProvincia(Provincia p) {
		return localidad_DAO.readAllLocalidadProvincia(p);
	}

	public List<Provincia> readAllProvinciasDeUnPais(Pais p) {
		return provincia_DAO.readAllProvinciasDeUnPais(p);
	}

	public boolean agregarCliente(Cliente nuevoCliente) {
		return this.cliente_DAO.insert(nuevoCliente);
	}

	public boolean modificarCliente(Cliente cliente_a_modificar) {
		return this.cliente_DAO.update(cliente_a_modificar);
	}

	public List<Cliente> obtenerClientes() {
		return this.cliente_DAO.readAll();
	}

	public List<Cliente> obtenerClientesBuscadosMenosParam(String texto) {
		return this.cliente_DAO.readBusquedaCorta(texto);
	}

	public List<Cliente> obtenerClientesBuscador(String texto) {
		return this.cliente_DAO.read(texto);
	}

	public List<Empleado> obtenerEmpleados() {
		return this.empleado_DAO.readAll();
	}

	public List<Empleado> obtenerEmpleadosSucursalDiaServicios(Date fecha, int id_sucursal, int id_servicio) {
		return this.empleado_DAO.readByEmpleadosSucursalDiaServicios(fecha, id_sucursal, id_servicio);
	}

	public List<Producto> obtenerProductos() {
		return this.producto_DAO.readAll();
	}

	public List<Sucursal> obtenerSucursales() {
		return this.sucursal_DAO.readAll();
	}

	public List<Servicio> obtenerServicios() {
		return this.servicio_DAO.readAll();
	}

	public List<EstadoPromocion> obtenerEstadosdePromociones() {
		return this.estadopromocion_DAO.readAll();
	}

	public List<Promocion> obtenerPromociones() {
		return this.promocion_DAO.readAll();
	}

	public List<TipoPromocion> obtenerTiposdePromociones() {
		return this.tipopromocion_DAO.readAll();
	}

	public List<CategoriaProducto> obtenerCategoriaProducto() {
		return this.categoriaproducto_DAO.readAll();
	}

	public List<Turno> obtenerTurnosPorFecha(LocalDate fechaABuscar) {
		return this.turno_DAO.readByFecha(fechaABuscar);
	}

	public List<EstadoCliente> obtenerEstadosdeClientes() {
		return this.obtenerEstadosdeClientes();
	}

	// SECCION PROMOCIONES
	public List<Promocion> obtenerTodasLasPromos() {
		return this.promocion_DAO.readAll();
	}

	public List<Promocion> obtenerPromosVigentes(int estado) {
		return this.promocion_DAO.readByVigencia(estado);
	}

	public List<Promocion> obtenerPromosInactivas(int estado) {
		return this.promocion_DAO.readByVigencia(estado);
	}

	public List<DetallePromocion> obtenerDetallesDePromo(int idPromo) {
		return this.detallePromo_DAO.readByID(idPromo);
	}
	
		public boolean modificarEmpleado(Empleado empleado) {
		return this.empleado_DAO.update(empleado);
	}

	
	
	public boolean existeEmpleado(int id) {
		return this.empleado_DAO.existe(id);
	}

	public Empleado obtenerUnEmpleado(int id) {
		return this.empleado_DAO.readOne(id);
	}

	public List<Empleado> obtenerEmpleadoBuscador(String texto) {
		
		return this.empleado_DAO.read(texto);
	}
	
	
	public int readUltimoIdEmpleado() {
		return this.empleado_DAO.readUltimoId();
	}

	public boolean agregarEmpleado(Empleado empleado) {
		return this.empleado_DAO.insert(empleado);
	}
}