package main;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import com.presentacion.vistas.Vista;

import modelo.HyHs;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorEmpleado;
import presentacion.controlador.ControladorPromocion;
import presentacion.controlador.ControladorTurno;

public class Main {

	public static void main(String[] args) {

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

					Vista vista = new Vista();
					HyHs modelo = new HyHs(new DAOSQLFactory());
					ControladorCliente controlador = new ControladorCliente(vista, modelo);
					ControladorTurno controladorT = new ControladorTurno(vista, modelo);
					ControladorPromocion controladorProm = new ControladorPromocion(vista, modelo);
					ControladorEmpleado controladorE= new ControladorEmpleado(vista, modelo);
					
					controlador.inicializar();
					controladorProm.inicializar();

				} catch (Exception ex) {
					Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		});

	}
}
