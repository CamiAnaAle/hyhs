package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;

import com.modelo.clases.DetallePromocion;
import com.modelo.clases.Promocion;
import com.presentacion.vistas.Vista;

import modelo.HyHs;

public class ControladorPromocion {

	private Vista vista;
	private List<Promocion> promociones;
	private List<DetallePromocion> detalles;
	private HyHs sistema;

	public ControladorPromocion(Vista vista, HyHs sist) {

		this.vista = vista;
		this.sistema = sist;

		Vista.pnlPromociones.getBtnObtener().addActionListener(obt -> obtenerPromocionesPorRadioButton(obt));
		Vista.pnlPromociones.getTablaPromociones().getSelectionModel()
				.addListSelectionListener(det -> obtenerDetallesDePromocion(det));
	}

	private void obtenerDetallesDePromocion(ListSelectionEvent det) {

		try {

			detalles = null;

			int idPromo = Integer.valueOf(Vista.pnlPromociones.getTablaPromociones()
					.getValueAt(Vista.pnlPromociones.getTablaPromociones().getSelectedRow(), 0).toString());

			System.out.println(idPromo);
			detalles = new ArrayList<DetallePromocion>();
			detalles.addAll(sistema.obtenerDetallesDePromo(idPromo));
			llenarTablaDetalles(detalles);

		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna promocion", "Atención",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private void llenarTablaDetalles(List<DetallePromocion> detallesDePromos) {

		Vista.pnlPromociones.getModeloTablaDetalles().setRowCount(0);
		Vista.pnlPromociones.getModeloTablaDetalles().setColumnCount(0);
		Vista.pnlPromociones.getModeloTablaDetalles()
				.setColumnIdentifiers(Vista.pnlPromociones.getNombreColumnasDetalle());

		for (DetallePromocion dp : detallesDePromos) {
			String numDePromo = Long.toString(dp.getId());
			String servicio = dp.getServicio().getDescripcion();
			String precio = Double.toString(dp.getServicio().getPrecio());

			Object[] fila = { numDePromo, servicio, precio };
			Vista.pnlPromociones.getModeloTablaDetalles().addRow(fila);
		}
	}

	//
	private void obtenerPromocionesPorRadioButton(ActionEvent obt) {

		try {

			
			promociones = null;
			detalles = null;
			Vista.pnlPromociones.getTablaPromociones().clearSelection();

			if (Vista.pnlPromociones.getRdbtnTodas().isSelected()) {
				promociones = new ArrayList<Promocion>();
				promociones.addAll(sistema.obtenerTodasLasPromos());
				llenarTablaPromociones(promociones);

				if (promociones.size() == 0)
					JOptionPane.showMessageDialog(null, "No hay promociones", "Atención", JOptionPane.WARNING_MESSAGE);

			} else if (Vista.pnlPromociones.getRdbtnVigentes().isSelected()) {

				promociones = new ArrayList<Promocion>();
				promociones.addAll(sistema.obtenerPromosVigentes(2));
				llenarTablaPromociones(promociones);

				if (promociones.size() == 0)
					JOptionPane.showMessageDialog(null, "No hay promociones vigentes", "Atención",
							JOptionPane.WARNING_MESSAGE);

			} else if (Vista.pnlPromociones.getRdbtnInactivas().isSelected()) {

				promociones = new ArrayList<Promocion>();
				promociones.addAll(sistema.obtenerPromosInactivas(1));
				llenarTablaPromociones(promociones);

				if (promociones.size() == 0)
					JOptionPane.showMessageDialog(null, "No hay promociones cerradas", "Atención",
							JOptionPane.WARNING_MESSAGE);

			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No ha seleccionado formato a buscar", "Atención",
					JOptionPane.WARNING_MESSAGE);
		}
	}

	private void llenarTablaPromociones(List<Promocion> promociones) {

		Vista.pnlPromociones.getModeloTablaPromos().setRowCount(0);
		Vista.pnlPromociones.getModeloTablaPromos().setColumnCount(0);
		Vista.pnlPromociones.getModeloTablaPromos()
				.setColumnIdentifiers(Vista.pnlPromociones.getNombreColumnasPromos());

		for (Promocion p : promociones) {
			String numDePromo = Long.toString(p.getId());
			String descripcion = p.getDescripcion();
			String precio = Double.toString(p.getPrecio());
			String puntos = Integer.toString(p.getPuntos());
			String desde = p.getFechaDesde().toString();
			String hasta = p.getFechaHasta().toString();
			String estado = p.getEstado().getDescripcion();
			String tipo = p.getTipo().getDescripcion();

			Object[] fila = { numDePromo, descripcion, precio, puntos, desde, hasta, estado, tipo };
			Vista.pnlPromociones.getModeloTablaPromos().addRow(fila);
		}
	}

	public void inicializar() {
		this.vista.setVisible(true);
	}

	public Vista getVista() {
		return vista;
	}

	public void setVista(Vista vista) {
		this.vista = vista;
	}

	public List<Promocion> getPromociones() {
		return promociones;
	}

	public void setPromociones(List<Promocion> promociones) {
		this.promociones = promociones;
	}

	public List<DetallePromocion> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetallePromocion> detalles) {
		this.detalles = detalles;
	}

	public HyHs getSistema() {
		return sistema;
	}

	public void setSistema(HyHs sistema) {
		this.sistema = sistema;
	}

}