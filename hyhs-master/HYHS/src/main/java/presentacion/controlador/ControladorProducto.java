
package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import com.modelo.clases.Producto;
import com.presentacion.vistas.Vista;

import modelo.HyHs;
import persistencia.dao.mysql.ProductoDAOSQL;

public class ControladorProducto implements ActionListener
{
		
		private Vista vista;
		private List<Producto> productos;
		private HyHs agenda;
		
		public ControladorProducto(Vista vista, HyHs agenda)
		{
			
			this.vista = vista;
		}
		private List<Producto> ListarTodosLosProductos()
		{
			ProductoDAOSQL proddaosql = new ProductoDAOSQL();
			return proddaosql.readAll();

		}
		
		public void inicializar()
		{
			//this.refrescarTabla();
			this.vista.show();
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
			

}
