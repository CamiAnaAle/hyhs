package presentacion.controlador;

import java.awt.color.CMMException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import com.modelo.clases.Cliente;
import com.modelo.clases.Empleado;
import com.modelo.clases.EstadoCliente;
import com.modelo.clases.Localidad;
import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;
import com.modelo.clases.TipoCliente;
import com.presentacion.vistas.Vista;
import com.presentacion.vistas.pnlClientes;
import com.presentacion.vistas.panelesCliente.PanelAgregarCliente;
import com.presentacion.vistas.panelesCliente.PanelBuscarCliente;
import com.presentacion.vistas.panelesCliente.PanelClientesMain;
import com.presentacion.vistas.panelesCliente.PanelModificarCliente;
import com.presentacion.vistas.panelesEmpleado.PanelAgregarEmpleado;
import com.presentacion.vistas.panelesEmpleado.PanelBuscarEmpleado;
import com.presentacion.vistas.panelesEmpleado.PanelEmpleadosMain;
import com.presentacion.vistas.panelesEmpleado.PanelModificarEmpleado;

import modelo.HyHs;
import validaciones.validaEmail;
import validaciones.validaLetras;
import validaciones.validaNumeros;

public class ControladorEmpleado implements ActionListener {

	
	private Vista vista;
	private List<Empleado> empleadosTabla;
	private HyHs agenda;
	private PanelAgregarEmpleado pnlAgregarEmpleado;
	private PanelBuscarEmpleado pnlBuscarEmpleado;

	public ControladorEmpleado(Vista vista, HyHs agenda) {
		this.vista = vista;
		this.agenda = agenda;
		this.vista.pnlEmpleado.getPnlAgregar().getBtnRegistrar().addActionListener(a -> agregarPersona(a));
		this.vista.pnlEmpleado.getPnlAgregar().getCmbPais().addItemListener(a -> eventoSeleccionarPais(a));
		this.vista.pnlEmpleado.getPnlAgregar().getCmbProvincia().addItemListener(a -> eventoSeleccionarProvincia(a));
		this.vista.pnlEmpleado.getPnlAgregar().getCmbLocalidad().addItemListener(a -> eventoSeleccionarLocalidad(a));
		this.vista.pnlEmpleado.getPnlBuscar().getBtnBuscar().addActionListener(a -> FiltrarEmpleado(a));
		this.vista.pnlEmpleado.getPnlModificar().getBtn_modificar().addActionListener(a->modificarPersona(a));

		this.vista.pnlEmpleado.getPnlModificar().getCmb_pais().addItemListener(a -> eventoSeleccionarPais(a));
		this.vista.pnlEmpleado.getPnlModificar().getCmb_provincia().addItemListener(a -> eventoSeleccionarProvincia(a));
		this.vista.pnlEmpleado.getPnlModificar().getCmb_localidad().addItemListener(a -> eventoSeleccionarLocalidad(a));
		this.vista.pnlEmpleado.getPnlModificar().getBtn_buscar().addActionListener(a -> eventoBotonAceptarEmpleado(a));


		
		this.pnlBuscarEmpleado = PanelBuscarEmpleado.getInstance();
		int legajo=this.agenda.readUltimoIdEmpleado()+1;
		this.vista.pnlEmpleado.getPnlAgregar().getlblLegajo().setText(""+legajo);

		cargarEstadoCliente();
		validacionesDeTecla();
		cargarDllPais();
		actualizarTablaDeClientes();
	}

	private void cargarEstadoCliente() {

		

		this.vista.pnlEmpleado.getPnlModificar().getCmb_estadocliente().addItem("ACTIVO");
		this.vista.pnlEmpleado.getPnlModificar().getCmb_estadocliente().addItem("INACTIVO");
		
	}

	private void actualizarTablaDeClientes() {

		this.empleadosTabla = agenda.obtenerEmpleados();

		llenarTabla();
	}

	public void validacionesDeTecla() {
		this.vista.pnlEmpleado.getPnlAgregar().getTxtDNI().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlAgregar().getTxtNombre().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlAgregar().getTxtApellido().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlAgregar().getTxtTelefono().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlAgregar().getTxtCelular().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlAgregar().getTxtDireccion().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlAgregar().getTxt_sueldoInicial().addKeyListener(new validaNumeros());


		this.vista.pnlEmpleado.getPnlModificar().getTxt_dni().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_nombre().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_apellido().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_telefono().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_celular().addKeyListener(new validaNumeros());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_direccion().addKeyListener(new validaLetras());
		this.vista.pnlEmpleado.getPnlModificar().getTxt_cp().enable(false);	
		this.vista.pnlEmpleado.getPnlModificar().getTxt_sueldoInicial().addKeyListener(new validaNumeros());

	}

	private void eventoSeleccionarPais(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Pais item = (Pais) event.getItem();
			this.vista.pnlEmpleado.getPnlAgregar().getCmbProvincia().removeAllItems();
			this.vista.pnlEmpleado.getPnlAgregar().getCmbLocalidad().removeAllItems();

			this.vista.pnlEmpleado.getPnlModificar().getCmb_provincia().removeAllItems();
			this.vista.pnlEmpleado.getPnlModificar().getCmb_localidad().removeAllItems();
			cargarDllProvincia(item);
		}
	}

	private void eventoSeleccionarProvincia(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Provincia item = (Provincia) event.getItem();
			this.vista.pnlEmpleado.getPnlAgregar().getCmbLocalidad().removeAllItems();
			this.vista.pnlEmpleado.getPnlModificar().getCmb_localidad().removeAllItems();
			cargarDllLocalidad(item);
		}
	}

	private void eventoSeleccionarLocalidad(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Localidad loc = (Localidad) event.getItem();
			this.vista.pnlEmpleado.getPnlAgregar().getTxtCP().setText(loc.getCp() + "");

			this.vista.pnlEmpleado.getPnlModificar().getTxt_cp().setText(loc.getCp() + "");
		}
	}

	private void cargarDllProvincia(Pais pais) {

		List<Provincia> listaprovincia = agenda.readAllProvinciasDeUnPais(pais);
		for (Provincia prov : listaprovincia) {
			this.vista.pnlEmpleado.getPnlAgregar().getCmbProvincia().addItem(prov);

			this.vista.pnlEmpleado.getPnlModificar().getCmb_provincia().addItem(prov);}
	}

	private void cargarDllLocalidad(Provincia provincia) {
		List<Localidad> listalocalidad = agenda.readAllLocalidadProvincia(provincia);
		for (Localidad loc : listalocalidad) {
			this.vista.pnlEmpleado.getPnlAgregar().getCmbLocalidad().addItem(loc);
			this.vista.pnlEmpleado.getPnlAgregar().getTxtCP().setText(loc.getCp() + "");

			this.vista.pnlEmpleado.getPnlModificar().getCmb_localidad().addItem(loc);
			
			this.vista.pnlEmpleado.getPnlModificar().getTxt_cp().setText(loc.getCp() + "");
		}
	}


	private void cargarDllPais() {
		List<Pais> listaPais = agenda.readAllPaises();
		System.out.print("jajajaja"+listaPais.size());
		for (Pais pais : listaPais) {
			this.vista.pnlEmpleado.getPnlAgregar().getCmbPais().addItem(pais);
			this.vista.pnlEmpleado.getPnlModificar().getCmb_pais().addItem(pais);
		}
	}

	private void agregarPersona(ActionEvent a) {

		PanelAgregarEmpleado pc = this.vista.pnlEmpleado.getPnlAgregar();
		if(!pc.getTxtEmail().getText().trim().equals(""))
		{
			if(!validaEmail.validar(pc.getTxtEmail().getText()))
			{
				JOptionPane.showMessageDialog(null, "Email invalido");
				return;
			}
		}
		String cadenaVacia=pc.getTxtTelefono().getText()+pc.getTxtCelular().getText()+pc.getTxtEmail().getText();
		
		if ((pc.getTxt_sueldoInicial().getText().trim().equals("")|| pc.getTxtDNI().getText().trim().equals("") || pc.getTxtNombre().getText().trim().equals("")
				|| pc.getTxtApellido().getText().trim().equals("") || pc.getCmbPais().getSelectedIndex() == 0
				|| pc.getCmbProvincia().getSelectedIndex() == 0 || pc.getTxtDireccion().getText().trim().equals(""))
				&& cadenaVacia.equals("")) {
			JOptionPane.showMessageDialog(null, "Complete todos los campos");
		} else {
			Long legajo=Long.parseLong(pc.getlblLegajo().getText());
			String dni = pc.getTxtDNI().getText();
			String nombre = pc.getTxtNombre().getText();
			String apellido = pc.getTxtApellido().getText();
			Localidad lc = (Localidad) pc.getCmbLocalidad().getSelectedItem();
			String direccion = pc.getTxtDireccion().getText();
			String telefono = pc.getTxtTelefono().getText();
			String email = pc.getTxtEmail().getText();

			Double sueldo= Double.parseDouble(pc.getTxt_sueldoInicial().getText());
			Date f_nac = pc.getDateNacimiento().getDate();
			LocalDate f = f_nac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			System.out.println(f_nac);

			String celular = pc.getTxtCelular().getText();
			System.out.println(celular);

			Empleado empleado = new Empleado(legajo, Integer.parseInt(dni), nombre, apellido, f, direccion, telefono, celular,
					email, LocalDate.now(), sueldo, true, lc);
			boolean estado = agenda.agregarEmpleado(empleado);
			if (estado) {
				JOptionPane.showMessageDialog(null, "Registro agregado con exito");
				pc.getlblLegajo().setText("");
				pc.getTxtDNI().setText("");
				pc.getTxtNombre().setText("");
				pc.getTxtApellido().setText("");
				pc.getTxtDireccion().setText("");
				pc.getTxtTelefono().setText("");
				pc.getDateNacimiento().cleanup();
				pc.getTxtEmail().setText("");
				pc.getTxtCelular().setText("");
				pc.getTxt_sueldoInicial().setText("");
				pc.getTxtCP().setText("");
				actualizarTablaDeClientes();
			} else {
				JOptionPane.showMessageDialog(null, "El registro no pudo ser agregado con exito");
			}
		}

	}

	

	private void modificarPersona(ActionEvent a) {

		PanelModificarEmpleado pc = this.vista.pnlEmpleado.getPnlModificar();
		
		if(!pc.getTxt_email().getText().trim().equals(""))
		{
			if(!validaEmail.validar(pc.getTxt_email().getText()))
			{
				JOptionPane.showMessageDialog(null, "Email invalido");
				return;
			}
		}
		String cadenaVacia=pc.getTxt_telefono().getText()+pc.getTxt_celular().getText()+ pc.getTxt_email().getText();

		if ((pc.getTxt_dni().getText().trim().equals("") || pc.getTxt_nombre().getText().trim().equals("")
				|| pc.getTxt_apellido().getText().trim().equals("") || pc.getCmb_pais().getSelectedIndex() == 0
				|| pc.getCmb_provincia().getSelectedIndex() == 0 || pc.getTxt_direccion().getText().trim().equals(""))
				&& cadenaVacia.trim().equals("")) {
			JOptionPane.showMessageDialog(null, "Complete todos los campos");
		} else {
			String dni = pc.getTxt_dni().getText();
			String nombre = pc.getTxt_nombre().getText();
			String apellido = pc.getTxt_apellido().getText();
			Localidad lc = (Localidad) pc.getCmb_localidad().getSelectedItem();
			String direccion = pc.getTxt_direccion().getText();
			String telefono = pc.getTxt_telefono().getText();
			String email = pc.getTxt_email().getText();

			Double sueldo=Double.parseDouble(pc.getTxt_sueldoInicial().getText());
			Date f_nac = pc.getDateNacimiento().getDate();
			LocalDate f = f_nac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			String celular = pc.getTxt_celular().getText();
			long legajo=Integer.parseInt(pc.getTxt_Legajo().getText());

			boolean estado_empleado=(pc.getCmb_estadocliente().getSelectedIndex()==0) ? true: false;
			Empleado empleado = new Empleado(legajo, Long.parseLong(dni), nombre, apellido, f, direccion, telefono, celular,
					email, LocalDate.now(), sueldo,estado_empleado,lc);
			boolean estado = agenda.modificarEmpleado(empleado);
			if (estado) {
				JOptionPane.showMessageDialog(null, "Registro modificado con exito");

				pc.getTxt_legajo().setText("");
				pc.getTxt_dni().setText("");
				pc.getTxt_nombre().setText("");
				pc.getTxt_apellido().setText("");
				pc.getTxt_direccion().setText("");
				pc.getTxt_telefono().setText("");
				pc.getDateNacimiento().cleanup();
				pc.getTxt_email().setText("");
				pc.getTxt_celular().setText("");
				pc.getTxt_sueldoInicial().setText("");
				pc.getTxt_cp().setText("");
				actualizarTablaDeClientes();
			} else {
				JOptionPane.showMessageDialog(null, "El registro no pudo ser modificado con exito");
			}
		}

	
}


	
	
	private void eventoBotonAceptarEmpleado(ActionEvent a) {
		int id;
		PanelModificarEmpleado pm=this.vista.pnlEmpleado.getPnlModificar();
		String text = this.vista.pnlEmpleado.getPnlModificar().getTxt_Legajo().getText();
		if(text.trim().equals("")==true)id=0;
		else id =Integer.parseInt(text);  
		

		if(this.vista.pnlEmpleado.getPnlModificar().getTxt_Legajo().isEnabled()==false)
		{
			this.vista.pnlEmpleado.getPnlModificar().getTxt_Legajo().enable(true);
			pm.getTxt_dni().setText("");
			pm.getTxt_legajo().setText("");
			pm.getTxt_nombre().setText("");
			pm.getTxt_apellido().setText("");
			pm.getTxt_direccion().setText("");
			pm.getTxt_telefono().setText("");
			pm.getDateNacimiento().cleanup();
			pm.getTxt_email().setText("");
			pm.getTxt_celular().setText("");
			pm.getTxt_sueldoInicial().setText("");
			pm.getTxt_cp().setText("");
			pm.getFechaIngreso().cleanup();
			actualizarTablaDeClientes();
			return;
		}
		
		if(agenda.existeEmpleado(id))
		{

			this.vista.pnlEmpleado.getPnlModificar().getTxt_Legajo().enable(false);
			
			Empleado e = agenda.obtenerUnEmpleado(id);
			this.vista.pnlEmpleado.getPnlModificar().getTxt_dni().setText(e.getDni()+"");
			this.vista.pnlEmpleado.getPnlModificar().getTxt_nombre().setText(e.getNombre()+"");
			this.vista.pnlEmpleado.getPnlModificar().getTxt_apellido().setText(e.getApellido()+"");
			this.vista.pnlEmpleado.getPnlModificar().getTxt_Legajo().setEnabled(false);
			this.vista.pnlEmpleado.getPnlModificar().getTxt_email().setText(e.getEmail()+"");
		    this.vista.pnlEmpleado.getPnlModificar().getDateNacimiento().setDate(
					java.sql.Date.valueOf(e.getNacimiento()));
		    this.vista.pnlEmpleado.getPnlModificar().getFechaIngreso().setDate(
					java.sql.Date.valueOf(e.getFechaIngreso()));
		    this.vista.pnlEmpleado.getPnlModificar().getCmb_estadocliente().setSelectedIndex((e.getEstado()==true) ? 0:1);
			this.vista.pnlEmpleado.getPnlModificar().getTxt_celular().setText(e.getCelular()+"");

			this.vista.pnlEmpleado.getPnlModificar().getTxt_telefono().setText(e.getTelefono()+"");

			this.vista.pnlEmpleado.getPnlModificar().getTxt_direccion().setText(e.getDireccion()+"");

			
			this.vista.pnlEmpleado.getPnlModificar().getTxt_sueldoInicial().setText(e.getSueldo()+"");


			this.vista.pnlEmpleado.getPnlModificar().getCmb_localidad().setSelectedItem(e.getLocalidad());

			this.vista.pnlEmpleado.getPnlModificar().getCmb_provincia().setSelectedItem(e.getLocalidad().getProvincia());

			this.vista.pnlEmpleado.getPnlModificar().getCmb_pais().setSelectedItem(e.getLocalidad().getProvincia().getPais());
		}
		else
		{
			JOptionPane.showMessageDialog(null, "No existe empleado con ese ID");
		}
	}
	
	
	
	
	 private void FiltrarEmpleado(ActionEvent l)
	 {
		 PanelBuscarEmpleado pc = this.vista.pnlEmpleado.getPnlBuscar();
			
		String texto= this.vista.pnlEmpleado.getPnlBuscar().getTxtIngreseTexto().getText().toString();
		 
		this.empleadosTabla = this.agenda.obtenerEmpleadoBuscador(texto);
		
		llenarTabla();
	}
	 
	 

		public void llenarTabla() {
			

		
			this.vista.pnlEmpleado.getPnlBuscar().getModelEmpleado().setRowCount(0); // Para vaciar la tabla
			this.vista.pnlEmpleado.getPnlBuscar().getModelEmpleado().setColumnCount(0);
			this.vista.pnlEmpleado.getPnlBuscar().getModelEmpleado().setColumnIdentifiers(this.vista.pnlEmpleado.getPnlBuscar().getNombreColumnas());
			
			for (Empleado c : empleadosTabla) {
				
				String nombre = c.getNombre();
				String apellido = c.getApellido();
				String dni = Long.toString(c.getDni());
				String tel = c.getTelefono();
				String cel = c.getCelular();
				String mail = c.getEmail();
				String sueldo = Double.toString(c.getSueldo());
				
				String direccion =c.getDireccion();
				String localidad = c.getLocalidad().getDescripcion();
				String nacimiento = c.getNacimiento().toString();
				String fechaIngreso = c.getFechaIngreso().toString();
				String estado=Boolean.toString(c.getEstado());
				String legajo= Long.toString(c.getLegajo());
				
				Object[] fila = { legajo, nombre, apellido, dni,tel,cel,mail,nacimiento,direccion,localidad, fechaIngreso, sueldo, estado};
				this.vista.pnlEmpleado.getPnlBuscar().getModelEmpleado().addRow(fila);
			}
		}

		public void inicializar() {
			this.vista.show();
		}

	public void actionPerformed(ActionEvent arg0) {

	}


}
