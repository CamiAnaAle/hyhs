package presentacion.controlador;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import com.modelo.clases.Servicio;
import com.presentacion.vistas.Vista;

import modelo.HyHs;
import persistencia.dao.mysql.ServicioDAOSQL;

public class ControladorServicio implements ActionListener
{
		
		private Vista vista;
		private List<Servicio> servicios;
		private HyHs agenda;
		
		public ControladorServicio(Vista vista, HyHs agenda)
		{
			
			this.vista = vista;
		}
		private List<Servicio> ListarTodosLosServicios()
		{
			ServicioDAOSQL servdaosql = new ServicioDAOSQL();
			return servdaosql.readAll();

		}
		
		private List<Servicio> FiltrarServicio(String texto)
		{
			ServicioDAOSQL servdaosql = new ServicioDAOSQL();
			return servdaosql.read(texto);

		}
		
		public void inicializar()
		{
			//this.refrescarTabla();
			this.vista.show();
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
			

}