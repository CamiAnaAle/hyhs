package presentacion.controlador;

import java.awt.color.CMMException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import com.modelo.clases.Cliente;
import com.modelo.clases.EstadoCliente;
import com.modelo.clases.Localidad;
import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;
import com.modelo.clases.TipoCliente;
import com.presentacion.vistas.Vista;
import com.presentacion.vistas.pnlClientes;
import com.presentacion.vistas.panelesCliente.PanelAgregarCliente;
import com.presentacion.vistas.panelesCliente.PanelBuscarCliente;
import com.presentacion.vistas.panelesCliente.PanelClientesMain;
import com.presentacion.vistas.panelesCliente.PanelModificarCliente;

import modelo.HyHs;
import validaciones.validaEmail;
import validaciones.validaLetras;
import validaciones.validaNumeros;

public class ControladorCliente implements ActionListener {

	
	private Vista vista;
	private List<Cliente> clientesTabla;
	private HyHs agenda;
	private PanelAgregarCliente pnlAgregarClientes;
	private PanelBuscarCliente pnlBuscaCliente;

	public ControladorCliente(Vista vista, HyHs agenda) {
		this.vista = vista;
		this.agenda = agenda;
		this.vista.pnlClientes.getPnlAgregar().getBtnRegistrar().addActionListener(a -> agregarPersona(a));
		this.vista.pnlClientes.getPnlAgregar().getCmbPais().addItemListener(a -> eventoSeleccionarPais(a));
		this.vista.pnlClientes.getPnlAgregar().getCmbProvincia().addItemListener(a -> eventoSeleccionarProvincia(a));
		this.vista.pnlClientes.getPnlAgregar().getCmbLocalidad().addItemListener(a -> eventoSeleccionarLocalidad(a));
		this.vista.pnlClientes.getPnlBuscar().getBtnBuscar().addActionListener(a -> FiltrarCliente(a));
		this.vista.pnlClientes.getPnlModificar().getBtn_modificar().addActionListener(a->modificarPersona(a));

		this.vista.pnlClientes.getPnlModificar().getCmb_pais().addItemListener(a -> eventoSeleccionarPais(a));
		this.vista.pnlClientes.getPnlModificar().getCmb_provincia().addItemListener(a -> eventoSeleccionarProvincia(a));
		this.vista.pnlClientes.getPnlModificar().getCmb_localidad().addItemListener(a -> eventoSeleccionarLocalidad(a));
		this.vista.pnlClientes.getPnlModificar().getBtn_buscar().addActionListener(a -> eventoBotonAceptarCliente(a));


		
		this.pnlBuscaCliente = PanelBuscarCliente.getInstance();
		int idCliente=this.agenda.readUltimoIdCliente()+1;
		this.vista.pnlClientes.getPnlAgregar().getlblIdCliente().setText(""+idCliente);

		cargarEstadoCliente();
		validacionesDeTecla();
		cargarDllTipoCliente();
		cargarDllPais();
		actualizarTablaDeClientes();
	}

	private void cargarEstadoCliente() {

		

		this.vista.pnlClientes.getPnlModificar().getCmb_estadocliente().addItem("ACTIVO");
		this.vista.pnlClientes.getPnlModificar().getCmb_estadocliente().addItem("INACTIVO");
		
	}

	private void actualizarTablaDeClientes() {

		this.clientesTabla = agenda.obtenerClientes();

		PanelClientesMain.getPnlBuscar().llenarTabla(clientesTabla);
	}

	public void validacionesDeTecla() {
		this.vista.pnlClientes.getPnlAgregar().getTxtDNI().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlAgregar().getTxtNombre().addKeyListener(new validaLetras());
		this.vista.pnlClientes.getPnlAgregar().getTxtApellido().addKeyListener(new validaLetras());
		this.vista.pnlClientes.getPnlAgregar().getTxtTelefono().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlAgregar().getTxtCelular().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlAgregar().getTxtDireccion().addKeyListener(new validaLetras());
		
		

		this.vista.pnlClientes.getPnlModificar().getTxt_dni().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlModificar().getTxt_nombre().addKeyListener(new validaLetras());
		this.vista.pnlClientes.getPnlModificar().getTxt_apellido().addKeyListener(new validaLetras());
		this.vista.pnlClientes.getPnlModificar().getTxt_telefono().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlModificar().getTxt_celular().addKeyListener(new validaNumeros());
		this.vista.pnlClientes.getPnlModificar().getTxt_direccion().addKeyListener(new validaLetras());
		this.vista.pnlClientes.getPnlModificar().getTxt_totaladeudado().enable(false);
		this.vista.pnlClientes.getPnlModificar().getTxt_cp().enable(false);
		this.vista.pnlClientes.getPnlModificar().getTxt_puntos().enable(false);
	}

	private void eventoSeleccionarPais(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Pais item = (Pais) event.getItem();
			this.vista.pnlClientes.getPnlAgregar().getCmbProvincia().removeAllItems();
			this.vista.pnlClientes.getPnlAgregar().getCmbLocalidad().removeAllItems();

			this.vista.pnlClientes.getPnlModificar().getCmb_provincia().removeAllItems();
			this.vista.pnlClientes.getPnlModificar().getCmb_localidad().removeAllItems();
			cargarDllProvincia(item);
		}
	}

	private void eventoSeleccionarProvincia(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Provincia item = (Provincia) event.getItem();
			this.vista.pnlClientes.getPnlAgregar().getCmbLocalidad().removeAllItems();
			this.vista.pnlClientes.getPnlModificar().getCmb_localidad().removeAllItems();
			cargarDllLocalidad(item);
		}
	}

	private void eventoSeleccionarLocalidad(ItemEvent event) {
		if (event.getStateChange() == ItemEvent.SELECTED) {
			Localidad loc = (Localidad) event.getItem();
			this.vista.pnlClientes.getPnlAgregar().getTxtCP().setText(loc.getCp() + "");

			this.vista.pnlClientes.getPnlModificar().getTxt_cp().setText(loc.getCp() + "");
		}
	}

	private void cargarDllProvincia(Pais pais) {

		List<Provincia> listaprovincia = agenda.readAllProvinciasDeUnPais(pais);
		for (Provincia prov : listaprovincia) {
			this.vista.pnlClientes.getPnlAgregar().getCmbProvincia().addItem(prov);

			this.vista.pnlClientes.getPnlModificar().getCmb_provincia().addItem(prov);}
	}

	private void cargarDllLocalidad(Provincia provincia) {
		List<Localidad> listalocalidad = agenda.readAllLocalidadProvincia(provincia);
		for (Localidad loc : listalocalidad) {
			this.vista.pnlClientes.getPnlAgregar().getCmbLocalidad().addItem(loc);
			this.vista.pnlClientes.getPnlAgregar().getTxtCP().setText(loc.getCp() + "");

			this.vista.pnlClientes.getPnlModificar().getCmb_localidad().addItem(loc);
			
			this.vista.pnlClientes.getPnlModificar().getTxt_cp().setText(loc.getCp() + "");
		}
	}

	private void cargarDllTipoCliente() {
		List<TipoCliente> listaTipoCliente = agenda.readAllTipoCliente();
		for (TipoCliente tipoCliente : listaTipoCliente) {
			this.vista.pnlClientes.getPnlAgregar().getCmbTipoCliente().addItem(tipoCliente);
			this.vista.pnlClientes.getPnlModificar().getCmb_tipocliente().addItem(tipoCliente);
		}
	}

	private void cargarDllPais() {
		List<Pais> listaPais = agenda.readAllPaises();
		for (Pais pais : listaPais) {
			this.vista.pnlClientes.getPnlAgregar().getCmbPais().addItem(pais);
			this.vista.pnlClientes.getPnlModificar().getCmb_pais().addItem(pais);
		}
	}

	private void agregarPersona(ActionEvent a) {

		PanelAgregarCliente pc = this.vista.pnlClientes.getPnlAgregar();
		if(!pc.getTxtEmail().getText().trim().equals(""))
		{
			if(!validaEmail.validar(pc.getTxtEmail().getText()))
			{
				JOptionPane.showMessageDialog(null, "Email invalido");
				return;
			}
		}
		String cadenaVacia=pc.getTxtTelefono().getText()+pc.getTxtCelular().getText()+pc.getTxtEmail().getText();
		
		if ((pc.getTxtDNI().getText().trim().equals("") || pc.getTxtNombre().getText().trim().equals("")
				|| pc.getTxtApellido().getText().trim().equals("") || pc.getCmbPais().getSelectedIndex() == 0
				|| pc.getCmbProvincia().getSelectedIndex() == 0 || pc.getTxtDireccion().getText().trim().equals(""))
				&& cadenaVacia.equals("")) {
			JOptionPane.showMessageDialog(null, "Complete todos los campos");
		} else {
			String dni = pc.getTxtDNI().getText();
			String nombre = pc.getTxtNombre().getText();
			String apellido = pc.getTxtApellido().getText();
			TipoCliente tp = (TipoCliente) pc.getCmbTipoCliente().getSelectedItem();
			Localidad lc = (Localidad) pc.getCmbLocalidad().getSelectedItem();
			String direccion = pc.getTxtDireccion().getText();
			String telefono = pc.getTxtTelefono().getText();
			String email = pc.getTxtEmail().getText();

			Date f_nac = pc.getDateNacimiento().getDate();
			LocalDate f = f_nac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			System.out.println(f_nac);

			String celular = pc.getTxtCelular().getText();
			System.out.println(celular);

			Cliente cliente = new Cliente(1L, Integer.parseInt(dni), nombre, apellido, f, direccion, telefono, celular,
					email, LocalDate.now(), 0, lc, tp, 0, 0, 0, 0, 0, 0, true);
			boolean estado = agenda.agregarCliente(cliente);
			if (estado) {
				JOptionPane.showMessageDialog(null, "Registro agregado con exito");

				int idCliente=this.agenda.readUltimoIdCliente()+1;
				pc.getlblIdCliente().setText(""+idCliente);
				pc.getTxtDNI().setText("");
				pc.getTxtNombre().setText("");
				pc.getTxtApellido().setText("");
				pc.getTxtDireccion().setText("");
				pc.getTxtTelefono().setText("");
				pc.getDateNacimiento().cleanup();
				pc.getTxtEmail().setText("");
				pc.getTxtCelular().setText("");
				actualizarTablaDeClientes();
			} else {
				JOptionPane.showMessageDialog(null, "El registro no pudo ser agregado con exito");
			}
		}

	}

	

	private void modificarPersona(ActionEvent a) {

		PanelModificarCliente pc = this.vista.pnlClientes.getPnlModificar();
		
		if(!pc.getTxt_email().getText().trim().equals(""))
		{
			if(!validaEmail.validar(pc.getTxt_email().getText()))
			{
				JOptionPane.showMessageDialog(null, "Email invalido");
				return;
			}
		}
		String cadenaVacia=pc.getTxt_telefono().getText()+pc.getTxt_celular().getText()+ pc.getTxt_email().getText();

		if ((pc.getTxt_dni().getText().trim().equals("") || pc.getTxt_nombre().getText().trim().equals("")
				|| pc.getTxt_apellido().getText().trim().equals("") || pc.getCmb_pais().getSelectedIndex() == 0
				|| pc.getCmb_provincia().getSelectedIndex() == 0 || pc.getTxt_direccion().getText().trim().equals(""))
				&& cadenaVacia.trim().equals("")) {
			JOptionPane.showMessageDialog(null, "Complete todos los campos");
		} else {
			String dni = pc.getTxt_dni().getText();
			String nombre = pc.getTxt_nombre().getText();
			String apellido = pc.getTxt_apellido().getText();
			TipoCliente tp = (TipoCliente) pc.getCmb_tipocliente().getSelectedItem();
			Localidad lc = (Localidad) pc.getCmb_localidad().getSelectedItem();
			String direccion = pc.getTxt_direccion().getText();
			String telefono = pc.getTxt_telefono().getText();
			String email = pc.getTxt_email().getText();

			Date f_nac = pc.getDateNacimiento().getDate();
			LocalDate f = f_nac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			String celular = pc.getTxt_celular().getText();
			long id_cliente=Integer.parseInt(pc.getTxt_idcliente().getText());

			boolean estado_cliente=(pc.getCmb_estadocliente().getSelectedIndex()==0) ? true: false;
			Cliente cliente = new Cliente(id_cliente, Integer.parseInt(dni), nombre, apellido, f, direccion, telefono, celular,
					email, LocalDate.now(), 0, lc, tp, 0, 0, 0, 0, 0, 0, estado_cliente);
			boolean estado = agenda.modificarCliente(cliente);
			if (estado) {
				JOptionPane.showMessageDialog(null, "Registro modificado con exito");

				pc.getTxt_dni().setText("");
				pc.getTxt_nombre().setText("");
				pc.getTxt_apellido().setText("");
				pc.getTxt_direccion().setText("");
				pc.getTxt_telefono().setText("");
				pc.getDateNacimiento().cleanup();
				pc.getTxt_email().setText("");
				pc.getTxt_celular().setText("");
				actualizarTablaDeClientes();
			} else {
				JOptionPane.showMessageDialog(null, "El registro no pudo ser modificado con exito");
			}
		}

	
}


	
	
	private void eventoBotonAceptarCliente(ActionEvent a) {
		int id;
		PanelModificarCliente pm=this.vista.pnlClientes.getPnlModificar();
		String text = this.vista.pnlClientes.getPnlModificar().getTxt_idcliente().getText();
		if(text.trim().equals("")==true)id=0;
		else id =Integer.parseInt(text);  
		

		if(this.vista.pnlClientes.getPnlModificar().getTxt_idcliente().isEnabled()==false)
		{
			this.vista.pnlClientes.getPnlModificar().getTxt_idcliente().enable(true);
			pm.getTxt_dni().setText("");
			pm.getTxt_nombre().setText("");
			pm.getTxt_apellido().setText("");
			pm.getTxt_direccion().setText("");
			pm.getTxt_telefono().setText("");
			pm.getDateNacimiento().cleanup();
			pm.getTxt_email().setText("");
			pm.getTxt_celular().setText("");
			actualizarTablaDeClientes();
			return;
		}
		
		if(agenda.existeCliente(id))
		{

			this.vista.pnlClientes.getPnlModificar().getTxt_idcliente().enable(false);
			
			Cliente c = agenda.obtenerUnCliente(id);
			this.vista.pnlClientes.getPnlModificar().getTxt_dni().setText(c.getDni()+"");
			this.vista.pnlClientes.getPnlModificar().getTxt_nombre().setText(c.getNombre()+"");
			this.vista.pnlClientes.getPnlModificar().getTxt_apellido().setText(c.getApellido()+"");
			this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setEnabled(false);
			this.vista.pnlClientes.getPnlModificar().getTxt_email().setText(c.getEmail()+"");
		    this.vista.pnlClientes.getPnlModificar().getDateNacimiento().setDate(
					java.sql.Date.valueOf(c.getNacimiento()));
		    this.vista.pnlClientes.getPnlModificar().getCmb_estadocliente().setSelectedIndex((c.getEstado()) ? 0:1);
			this.vista.pnlClientes.getPnlModificar().getTxt_celular().setText(c.getCelular()+"");

			this.vista.pnlClientes.getPnlModificar().getTxt_telefono().setText(c.getTelefono()+"");

			this.vista.pnlClientes.getPnlModificar().getTxt_direccion().setText(c.getDireccion()+"");

			
			this.vista.pnlClientes.getPnlModificar().getTxt_totaladeudado().setText(c.getTotalAdeudado()+"");

			this.vista.pnlClientes.getPnlModificar().getTxt_puntos().setText(c.getPuntos()+"");
			
			this.vista.pnlClientes.getPnlModificar().getCmb_tipocliente().setSelectedItem(c.getTipoDeCliente());

			this.vista.pnlClientes.getPnlModificar().getCmb_localidad().setSelectedItem(c.getLocalidad());

			this.vista.pnlClientes.getPnlModificar().getCmb_provincia().setSelectedItem(c.getLocalidad().getProvincia());

			this.vista.pnlClientes.getPnlModificar().getCmb_pais().setSelectedItem(c.getLocalidad().getProvincia().getPais());
		}
		else
		{
			JOptionPane.showMessageDialog(null, "No existe cliente con ese ID");
		}
	}
	
	
	
	
	
	 private void FiltrarCliente(ActionEvent l)
	 {
		 PanelBuscarCliente pc = this.vista.pnlClientes.getPnlBuscar();
			
		String texto= this.vista.pnlClientes.getPnlBuscar().getTxtIngreseTexto().getText().toString();
		 
		this.clientesTabla = this.agenda.obtenerClientesBuscador(texto);
		PanelClientesMain.getPnlBuscar().llenarTabla(clientesTabla);
	}
	public void inicializar() {
		this.vista.show();
	}

	public void actionPerformed(ActionEvent arg0) {

	}


}
