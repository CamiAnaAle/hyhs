package presentacion.controlador;
//
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.modelo.clases.Asistencia;
import com.modelo.clases.Cliente;
import com.modelo.clases.DetalleTurno;
import com.modelo.clases.Empleado;
import com.modelo.clases.Franja;
import com.modelo.clases.Pais;
import com.modelo.clases.Producto;
import com.modelo.clases.Provincia;
import com.modelo.clases.Servicio;
import com.modelo.clases.Sucursal;
import com.modelo.clases.Turno;
import com.presentacion.vistas.Vista;
import com.presentacion.vistas.panelesBusqueda.BusquedaClientes;
import com.presentacion.vistas.panelesCliente.PanelClientesMain;
import com.presentacion.vistas.panelesTurno.FrameProductos;
import com.presentacion.vistas.panelesTurno.PanelTurnosMain;

import modelo.HyHs;
import persistencia.dao.mysql.ProvinciaDAOSQL;
import persistencia.dao.mysql.SucursalDAOSQL;
import validaciones.validaLetras;
import validaciones.validaNumeros;

public class ControladorTurno implements ActionListener {

	
	private Vista vista;
	private HyHs agenda;
	private BusquedaClientes busquedaClientes;
	private List<Cliente> clientesTabla;
	private List<Turno> turnosEnPanelConsultar;
	private FrameProductos pantallaProductos;

	public ControladorTurno(Vista vista, HyHs agenda) {
		this.agenda = agenda;
		this.vista = vista;
		
		//Eventos sobre pnlAgregar
		this.vista.pnlTurnos.getPnlAgregar().getBtnAceptarCliente().addActionListener(a -> pnlAgregar_AceptarCliente(a));
		this.vista.pnlTurnos.getPnlAgregar().getBtnBuscarCliente().addActionListener(a -> pnlAgregar_BuscarCliente(a)); // Abre el frame buscar
		this.vista.pnlTurnos.getPnlAgregar().getDateFecha().getDateEditor().addPropertyChangeListener(b -> pnlAgregar_SeleccionarFecha(b));
		this.vista.pnlTurnos.getPnlAgregar().getBtnLimpiarCliente().addActionListener(a -> pnlAgregar_LimpiarCliente(a)); 
		this.vista.pnlTurnos.getPnlAgregar().getCmbSucursal().addItemListener(a -> pnlAgregar_SeleccionarSucursal(a));
		
		//Panel de búsquedas
		busquedaClientes = new BusquedaClientes();
		busquedaClientes.getBtnBusqueda().addActionListener(a -> frameBusquedaBuscarCliente(a));
		busquedaClientes.getBtnAceptar().addActionListener(a -> frameBusquedaAceptaCliente(a));

		this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getBtnAgregarServicio().addActionListener(a -> pnlAgregarServicio_AgregarServicio(a));
		this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio().addItemListener(a -> pnlAgregarServicio_SeleccionarComboTipoServicio(a));
		this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().addItemListener(a -> pnlAgregarServicio_SeleccionarComboEmpleado(a));
		
		// EVENTOS SECCION BUSCAR PRODUCTOS
				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getBtnBuscarProducto()
						.addActionListener(bsc -> buscarProductosParaAgregarEnTurno(bsc));
				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtCantidad()
						.addActionListener(num -> calcularMontoTotal(num));

		
		// EVENTOS DE PANEL CONSULTAR
		Vista.pnlTurnos.getPnlConsultar().getBtnActualizar()
				.addActionListener(act -> actualizarTablaConFechaSeleccionada(act));
		Vista.pnlTurnos.getPnlConsultar().getBtnModificar().addActionListener(mod -> modificarTurnoSeleccionado(mod));
				
		validacionesDeTecla();
		llenarSucursales();
		llenarClientes();
		llenarServicios();
		//llenarEmpleados();
	}



	
	private void modificarTurnoSeleccionado(ActionEvent mod) {
		// TODO Auto-generated method stub
		Vista.pnlTurnos.getTabbedPane().setSelectedIndex(3);
		Turno turnoSeleccionado = obtenerTurnoDeTabla();
	}

	private Turno obtenerTurnoDeTabla() {
		return null;
	}
	
	//EVENTOS SOBRE PNL_AGREGAR:

	private void pnlAgregar_LimpiarCliente(ActionEvent a) {
		this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setEnabled(true);
		this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setText("");
		this.vista.pnlTurnos.getPnlAgregar().getTxtDni().setText("");
		this.vista.pnlTurnos.getPnlAgregar().getTxtNombre().setText("");
	}

	private void pnlAgregar_AceptarCliente(ActionEvent a) {
		int id;
		String text = this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().getText();

		if (text.trim().equals("") == true)
			id = 0;
		else
			id = Integer.parseInt(text);

		if (agenda.existeCliente(id)) {
			Cliente c = agenda.obtenerUnCliente(id);
			this.vista.pnlTurnos.getPnlAgregar().getTxtDni().setText(c.getDni() + "");
			this.vista.pnlTurnos.getPnlAgregar().getTxtNombre().setText(c.getNombre() + "," + c.getApellido());
			this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setEnabled(false);
		} else {
			JOptionPane.showMessageDialog(null, "No existe cliente con ese ID");
		}
	}

	private void pnlAgregar_BuscarCliente(ActionEvent a) {
		busquedaClientes.setVisible(true);
	}

	private void pnlAgregar_SeleccionarFecha(PropertyChangeEvent e)
	{
		 if ("date".equals(e.getPropertyName())) {
			 this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio().setSelectedIndex(0);
			 /*System.out.println(e.getPropertyName()
                 + ": " + (Date) e.getNewValue());
                 */
         }
	}

	private void pnlAgregar_SeleccionarSucursal(ItemEvent item) {
		
		if(item.getStateChange()==ItemEvent.SELECTED)
		{
			//Al seleccionar el combo sucursal se limpia la información
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio().setSelectedIndex(-1);
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().removeAllItems();
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbRango().removeAllItems();
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getTxtPrecio().setText("0");
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getTxtTiempo().setText("0");

		}
	}


	//EVENTOS SOBRE EL FRAME BUSQUEDA CLIENTES
	public void frameBusquedaAceptaCliente(ActionEvent a) {

		int i = busquedaClientes.getTablaClientes().getSelectedRow();
		Cliente c = clientesTabla.get(i);
		this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setText(c.getIdCliente() + "");
		this.vista.pnlTurnos.getPnlAgregar().getTxtNombre().setText(c.getNombre() + "," + c.getApellido());
		this.vista.pnlTurnos.getPnlAgregar().getTxtDni().setText(c.getDni() + "");
		busquedaClientes.setVisible(false);
	}

	public void frameBusquedaBuscarCliente(ActionEvent a) {

		clientesTabla = agenda.obtenerClientesBuscadosMenosParam(busquedaClientes.getTxtBusquedaCliente().getText() + "");
		busquedaClientes.llenarTabla(clientesTabla);
	}

	public void frameBusquedaCliente_Aceptar(ActionEvent a) {
		int i = busquedaClientes.getTablaClientes().getSelectedRow();

		Cliente c = clientesTabla.get(i);
		this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().setText(c.getIdCliente() + "");
	}
	
	
	//EVENTOS SOBRE PANEL AGREGAR SERVICIO

	private void pnlAgregarServicio_SeleccionarComboEmpleado(ItemEvent item) {
			
		//	Traer los rangos libre	
		if(item.getStateChange()==ItemEvent.SELECTED)
			{
				Empleado e = (Empleado) this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().getSelectedItem();
				Sucursal sucursal= (Sucursal)this.vista.pnlTurnos.getPnlAgregar().getCmbSucursal().getSelectedItem();
				Date fecha = this.vista.pnlTurnos.getPnlAgregar().getDateFecha().getDate();
				
				if(e.getNombre().equals("")==false && fecha!=null && sucursal.getNombre().equals("")==false)
				{
					java.sql.Date fechaCasteada = new java.sql.Date(fecha.getTime());
					//Obtener la hora de asistencia de ese empleado:
					Asistencia a =this.agenda.obtenerAsistencia_Empleado_Dia_Sucursal(fechaCasteada, (int)e.getLegajo(), (int)sucursal.getId());
					//JOptionPane.showMessageDialog(null, a.toString());

					System.out.println("ASISTENCIA "+a.toString());
					
					//Obtener los turnos que tiene ese empleado ese día
					List<DetalleTurno> listaDetalle = this.agenda.obtenerDetalleTurno_Asistencia(a);
					
					for (DetalleTurno d : listaDetalle) {
					//	JOptionPane.showMessageDialog(null, d.toString());	
						System.out.println("DETALLES"+d.toString());
						
					}
					
					List<Franja> listaFranja = Franja.calculoFranjaHoraria(a, listaDetalle);
					for (Franja franja : listaFranja) {
						System.out.println("FRANJA"+franja.toString());
					}
					
					llenarFranjaHoraria(listaFranja);
				}
			}
	}
	
	private void pnlAgregarServicio_SeleccionarComboTipoServicio(ItemEvent item) {
		
		if(item.getStateChange()==ItemEvent.SELECTED)
		{
			Date date = this.vista.pnlTurnos.getPnlAgregar().getDateFecha().getDate();
			
			Servicio servicio = (Servicio) this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio()
				.getSelectedItem();
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getTxtPrecio().setText(servicio.getPrecio() + "");
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getTxtTiempo().setText(servicio.getTiempoEstimado() + "");

			Sucursal sucursal= (Sucursal)this.vista.pnlTurnos.getPnlAgregar().getCmbSucursal().getSelectedItem();
			int sucId= (int)sucursal.getId();
			int servId= (int)servicio.getId();
			
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().removeAllItems();
				
			if(date!= null && servicio.getDescripcion().equals("")==false && sucursal.getNombre().equals("")==false )
			{
				llenarEmpleados(date,sucId,servId);
			}
		
	}
	}

	private void pnlAgregarServicio_AgregarServicio(ActionEvent a) {
		JOptionPane.showMessageDialog(null, "Se agregar un servicio nuevo");
	}

	public void validacionesDeTecla() {
		this.vista.pnlTurnos.getPnlAgregar().getTxtNroCliente().addKeyListener(new validaNumeros());
	}

	//EVENTOS LLENAR
	
	public void llenarFranjaHoraria(List<Franja> listaHoraria) {
		for (Franja f : listaHoraria) {
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbRango().addItem(f);
		}
	}
	
	public void llenarSucursales() {
		List<Sucursal> lSucursales = agenda.readAllSucursales();
		for (Sucursal suc : lSucursales) {
			this.vista.pnlTurnos.getPnlAgregar().getCmbSucursal().addItem(suc);
		}
	}

	public void llenarServicios() {
		List<Servicio> lServicios = agenda.obtenerServicios();
		this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio().addItem(new Servicio(0, ""));
		for (Servicio serv : lServicios) {
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbTipoServicio().addItem(serv);
		}
	}

	public void llenarClientes() {
		clientesTabla = agenda.obtenerClientes();
		busquedaClientes.llenarTabla(clientesTabla);
	}

	public void llenarEmpleados(Date fecha, int id_sucursal, int id_servicio) {
		java.sql.Date fechaCasteada = new java.sql.Date(fecha.getTime());
		List<Empleado> lEmpleados = agenda.obtenerEmpleadosSucursalDiaServicios(fechaCasteada,id_sucursal,id_servicio);
		int cant=0;
		
		this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().addItem(new Empleado(0, "",""));
		
		for (Empleado emp : lEmpleados) {
			cant++;
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().addItem(emp);
		}
		if(cant==0)
		{
			this.vista.pnlTurnos.getPnlAgregar().getPnlTurnoServicio().getCbEmpleado().addItem(new Empleado(0, "Sin empleado",""));
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

	}

	// METODOS DE PANEL CONSULTAR
	private void actualizarTablaConFechaSeleccionada(ActionEvent act) {

		try {

			turnosEnPanelConsultar = null;
			
			LocalDate fechaSeleccionada = Vista.pnlTurnos.getPnlConsultar().getCalendar().getDatoFecha().toInstant()
					.atZone(ZoneId.systemDefault()).toLocalDate();
			turnosEnPanelConsultar = new ArrayList<Turno>();
			turnosEnPanelConsultar.addAll(agenda.obtenerTurnosPorFecha(fechaSeleccionada));

			System.out.println(turnosEnPanelConsultar.size());

			llenarTablaTurnos(turnosEnPanelConsultar);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna fecha para buscar", "Atención",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	private void llenarTablaTurnos(List<Turno> turnosEnFecha) {

		Vista.pnlTurnos.getPnlConsultar().getModeloTurnos().setRowCount(0);
		Vista.pnlTurnos.getPnlConsultar().getModeloTurnos().setColumnCount(0);
		Vista.pnlTurnos.getPnlConsultar().getModeloTurnos()
				.setColumnIdentifiers(Vista.pnlTurnos.getPnlConsultar().getNombreColumnas());

		for (Turno t : turnosEnFecha) {
			String numeroDeTurno = Long.toString(t.getId());
			String nombre = t.getCliente().getNombre();
			String apellido = t.getCliente().getApellido();
			String tiempo = t.getTiempoTotal().toString();
			String servicio = t.getSucursal().getNombre();
			String estado = t.isEstado() ? "Pendiente" : "Cancelado";

			Object[] fila = { numeroDeTurno, nombre, apellido, tiempo, servicio, estado };
			Vista.pnlTurnos.getPnlConsultar().getModeloTurnos().addRow(fila);
		}
	}
	
	// METODOS DE PARTE AGREGAR PRODUCTO EN SECCION AGREGAR TURNO
		private void buscarProductosParaAgregarEnTurno(ActionEvent bsc) {

			pantallaProductos = new FrameProductos();
			pantallaProductos.setVisible(true);

			llenarTablaProductos(pantallaProductos);
			pantallaProductos.getBtnAgregar().addActionListener(agr -> agregarProductoATurno(agr));
		}

		private void agregarProductoATurno(ActionEvent agr) {

			try {

				int fila = pantallaProductos.getTablaProductos().getSelectedRow();
				Producto unProducto = new Producto();

				unProducto.setId(Long.parseLong(pantallaProductos.getTablaProductos().getValueAt(fila, 0).toString()));
				unProducto.setNombre(pantallaProductos.getTablaProductos().getValueAt(fila, 1).toString());
				unProducto.setPrecioDeVenta(
						Double.parseDouble(pantallaProductos.getTablaProductos().getValueAt(fila, 5).toString()));

				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtIdProducto()
						.setText(Long.toString(unProducto.getId()));
				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtNombre().setText(unProducto.getNombre());
				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtPrecio()
						.setText(Double.toString(unProducto.getPrecioDeVenta()));
				
				pantallaProductos.dispose();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No ha seleccionado ningun producto", "Atención",
						JOptionPane.WARNING_MESSAGE);
			}

		}

		private void llenarTablaProductos(FrameProductos pantallaProductos) {

			pantallaProductos.getModelProductos().setRowCount(0);
			pantallaProductos.getModelProductos().setColumnCount(0);
			pantallaProductos.getModelProductos().setColumnIdentifiers(pantallaProductos.getNombreColumnas());

			ArrayList<Producto> productos = new ArrayList<Producto>();
			productos.addAll(agenda.obtenerProductos());

			for (Producto p : productos) {
				String numDeProducto = Long.toString(p.getId());
				String nombre = p.getNombre();
				String descripcion = p.getDescripcion();
				String stock = Integer.toString(p.getStock());
				String precioCompra = Double.toString(p.getPrecioDeCompra());
				String precioVenta = Double.toString(p.getPrecioDeVenta());
				String categoria = p.getCategoria().getDescripcion();

				Object[] fila = { numDeProducto, nombre, descripcion, stock, precioCompra, precioVenta, categoria };
				pantallaProductos.getModelProductos().addRow(fila);
			}

		}

		// TODO: Verificar que no admita caracteres que no sean numeros
		private void calcularMontoTotal(ActionEvent num) {

			try {

				int cant = Integer
						.parseInt(PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtCantidad().getText());
				double valorUnidad = Double
						.parseDouble(PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtPrecio().getText());

				PanelTurnosMain.getPnlAgregar().getPnlTurnoProducto().getTxtImporteTotal()
						.setText(Double.toString((cant * valorUnidad)));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No hay ningun producto", "Atención", JOptionPane.WARNING_MESSAGE);
			}
		}

}
