package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.DetallePromocion;

public interface DetallePromocionDAO {

	public List<DetallePromocion> readByID(int idPromo);

}
