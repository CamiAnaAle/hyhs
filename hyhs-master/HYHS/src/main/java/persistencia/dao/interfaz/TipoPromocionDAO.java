package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.TipoPromocion;


public interface TipoPromocionDAO {
	
	public boolean insert(TipoPromocion tipopromocion_a_agregar);//Agregar un tipo de promocion
	public  TipoPromocion readForId(int idtipoPromocion);
		
    public List<TipoPromocion> readAll();//Listado de tipo promocion
		
}
