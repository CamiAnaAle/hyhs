package persistencia.dao.interfaz;

import java.sql.Date;
import java.util.List;

import com.modelo.clases.Cliente;
import com.modelo.clases.Empleado;


public interface ClienteDAO 
{
	
	public boolean insert(Cliente cliente_a_agregar);//Agregar un cliente
	
	public boolean update(Cliente cliente_a_modificar);//Modificacion de cliente
	
	public List<Cliente> readAll();//Listado de clientes
	
	public List<Cliente> readByFecha(Date fecha_a_buscar);
	
	public List<Cliente> read(String texto_a_buscar);
	
	public Cliente readOne(int id);

	public int readUltimoId();
	
	public boolean existe(int id);
	
	public List<Cliente> readBusquedaCorta(String texto_a_buscar);
		
}

