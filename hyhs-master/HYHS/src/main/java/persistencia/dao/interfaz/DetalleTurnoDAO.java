package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Asistencia;
import com.modelo.clases.DetalleTurno;

public interface DetalleTurnoDAO {

	public List<DetalleTurno> readAllDetalleTurno_Asistencia(Asistencia a);
}
