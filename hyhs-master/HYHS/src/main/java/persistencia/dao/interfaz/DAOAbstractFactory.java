package persistencia.dao.interfaz;

public interface DAOAbstractFactory 
{
	public AsistenciaDAO createAsistenciaDAO();
	
	public DetalleTurnoDAO createDetalleTurnoDAO();
	
	public ClienteDAO createClienteDAO();
	
	public EmpleadoDAO createEmpleadoDAO();
	
	public EstadoPromocionDAO createEstadoPromocionDAO();
	
	public LocalidadDAO createLocalidadDAO();
	
	public PaisDAO createPaisDAO();
	
	public PromocionDAO createPromocionDAO();
	
	public ServicioDAO createServicioDAO();
	
	public TipoClienteDAO createTipoClienteDAO();
	
	public TipoPromocionDAO createTipoPromocionDAO();
	
	public ProvinciaDAO createProvinciaDAO();

	public CategoriaProductoDAO createCategoriaProductoDAO();
	
	public ProductoDAO createProductoDAO();

	public SucursalDAO createSucursalDAO();

	public TurnoDAO createTurnoDAO();

	public DetallePromocionDAO createDetallePromoDAO();
}

