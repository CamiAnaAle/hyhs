package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Producto;

public interface ProductoDAO {

	
	public boolean insert(Producto producto);
	public List<Producto> readAll();
	
	
}
