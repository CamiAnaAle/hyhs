package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;

public interface ProvinciaDAO {
	
	public boolean insert(Provincia provincia);
	
	public List<Provincia> readAll();

	public int traerporNombre(String nombre);
	
	public Provincia readForId(int idprovincia);
	
	public List<Provincia> readAllProvinciasDeUnPais(Pais p);
}
