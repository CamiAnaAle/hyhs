package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Servicio;

public interface ServicioDAO {

	
public boolean insert(Servicio servicio_a_agregar);//Agregar un servicio
	
	public boolean update(Servicio servicio_a_modificar);//Modificacion de servicio
	
	public List<Servicio> readAll();//Listado de servicio
	
	public List<Servicio> read(String texto_a_buscar);

	public Servicio readForId(int unServicio);
	
}
