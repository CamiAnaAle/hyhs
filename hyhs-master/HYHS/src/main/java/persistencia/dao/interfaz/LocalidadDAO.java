package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Localidad;
import com.modelo.clases.Provincia;


//CLASE INTERFAZ DE OBJETO LOCALIDAD DAO
public interface LocalidadDAO 
{

	public boolean insert(Localidad localidad);

	public boolean update(Localidad localidad);

	public List<Localidad> readAll();

	public int traerporNombre(String nombre);
	
	public Localidad readForId(int idLocalidad);
	
	public List<Localidad> readAllLocalidadProvincia(Provincia p);

}
