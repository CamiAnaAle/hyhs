package persistencia.dao.interfaz;

import java.sql.Date;

import com.modelo.clases.Asistencia;

public interface AsistenciaDAO {

	
	public Asistencia readAsistencia_Empleado_Dia_Sucursal(Date fecha, int legajo, int idSucursal);

	public Asistencia readAsistenciaEmpleado(Date fecha, int legajo, int idSucursal);
	
}
