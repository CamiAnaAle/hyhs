package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Promocion;

public interface PromocionDAO {

	public boolean insert(Promocion promocion_a_agregar);// Agregar una promocion

	public boolean update(Promocion promocion_a_modificar);// Modificacion de promocion

	public List<Promocion> readAll();// Listado de promocion

	public List<Promocion> readByVigencia(int estado);
}
