package persistencia.dao.interfaz;

import java.time.LocalDate;
import java.util.List;

import com.modelo.clases.Turno;

public interface TurnoDAO {

	public boolean insert(Turno turno_a_agregar);		// Agregar un turno

	public boolean update(Turno turno_a_modificar);		// Modificacion de turno

	public List<Turno> readAll();						// Listado de turnos

	public List<Turno> readByFecha(LocalDate fecha_a_buscar);

	public List<Turno> read(String texto_a_buscar);

}
