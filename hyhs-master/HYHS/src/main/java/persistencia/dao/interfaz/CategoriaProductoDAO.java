package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.CategoriaProducto;

public interface CategoriaProductoDAO {

	
	public boolean insert(CategoriaProducto categoria);
	
	public List<CategoriaProducto> readAll();
	
	public CategoriaProducto readForId(int idCategoria);
	
	
	public int traerporNombre(String nombre) ;
	
}
