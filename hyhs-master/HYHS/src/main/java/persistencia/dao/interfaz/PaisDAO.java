package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Pais;


public interface PaisDAO {
	
	public boolean insert(Pais pais);
	
	public List<Pais> readAll();

	public int traerporNombre(String nombre);
	
	public Pais readForId(int idpais);

}
