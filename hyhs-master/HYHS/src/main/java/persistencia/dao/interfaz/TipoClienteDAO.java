package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.TipoCliente;

public interface TipoClienteDAO {

	public boolean insert(TipoCliente tipo_a_agregar);// Agregar un tipo de cliente

	public boolean edit(TipoCliente tipo_a_modificar, String nuevo_nombre);// Modificacion de un tipo de cliente

	public List<TipoCliente> readAll();// Listado de tipos de clientes
}
