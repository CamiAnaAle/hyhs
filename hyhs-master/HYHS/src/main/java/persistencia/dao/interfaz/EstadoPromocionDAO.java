package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.EstadoPromocion;

public interface EstadoPromocionDAO {

	
	public boolean insert(EstadoPromocion estadopromocion_a_agregar);//Agregar una promocion
	public  EstadoPromocion readForId(int idEstadoPromocion);
  		
    public List<EstadoPromocion> readAll();//Listado de promocion
		
	
}

