package persistencia.dao.interfaz;

import java.util.List;

import com.modelo.clases.Sucursal;

public interface SucursalDAO {

	

	public boolean insert(Sucursal sucursal);
	public boolean update(Sucursal sucursal_modificar);
	public List<Sucursal> readAll();
	public List<Sucursal> read(String texto_a_buscar);
	public Sucursal readOne(int idSucursal);

	
	
}
