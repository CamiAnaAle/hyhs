package persistencia.dao.interfaz;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import com.modelo.clases.Empleado;

public interface EmpleadoDAO {


	public boolean insert(Empleado empleado_a_agregar);//Agregar un cliente
	
	public boolean update(Empleado empleado_a_modificar);//Modificacion de cliente
	
	public List<Empleado> readAll();//Listado de clientes
	
	public List<Empleado> readByFecha(Date fecha_a_buscar);
	
	public List<Empleado> read(String texto_a_buscar);
	
	public List<Empleado> readByEmpleadosSucursalDiaServicios(Date fecha_a_buscar, int id_sucursal, int id_servicio);
		
	public Empleado readOne(int id);

	public int readUltimoId();
	
	public boolean existe(int id);
	
		
}