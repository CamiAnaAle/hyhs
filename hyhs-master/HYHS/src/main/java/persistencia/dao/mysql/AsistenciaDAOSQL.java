package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Asistencia;
import com.modelo.clases.DiaLaboral;
import com.modelo.clases.Empleado;
import com.modelo.clases.Producto;
import com.modelo.clases.Turno;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.AsistenciaDAO;

public class AsistenciaDAOSQL implements AsistenciaDAO {

	
	private static final String readAsistenciaEmpleadoEnUnDiaParticular = "SELECT a.id as id, a.fecha as fecha, a.horaDesde as horaDesde, a.horaHasta as horaHasta,  \r\n" + 
			"a.idEstado as idEstado, a.legajo As legajo, a.id_sucursal as id_sucursal, a.id_dia as id_dia, a.estado as estado,\r\n" + 
			"e.legajo As e_legajo, e.dni As e_dni, e.nombre As e_nombre, e.apellido As e_apellido\r\n" + 
			"FROM asistencia a inner join empleado e on a.legajo=e.legajo  where a.legajo=? and a.id_sucursal=? and a.fecha=? and a.estado=? and a.idEstado=?";
	
	private static final String readAllEmpleado = "SELECT a.id as id, a.fecha as fecha, a.horaDesde as horaDesde, a.horaHasta as horaHasta,  \r\n" + 
			"a.idEstado as idEstado, a.legajo As legajo, a.id_sucursal as id_sucursal, a.id_dia as id_dia, a.estado as estado,\r\n" + 
			"e.legajo As e_legajo, e.dni As e_dni, e.nombre As e_nombre, e.apellido As e_apellido\r\n" + 
			"FROM asistencia a inner join empleado e on a.legajo=e.legajo where a.id_sucursal=? and a.fecha=? and a.estado=? and a.idEstado=? order by a.legajo asc";
	

	private static final String readOneAsistencia = "SELECT a.id as id, a.fecha as fecha, a.horaDesde as horaDesde, a.horaHasta as horaHasta,  \r\n" + 
			"a.idEstado as idEstado, a.legajo As legajo, a.id_sucursal as id_sucursal, a.id_dia as id_dia, a.estado as estado,\r\n" + 
			"e.legajo As e_legajo, e.dni As e_dni, e.nombre As e_nombre, e.apellido As e_apellido\r\n" + 
			"FROM asistencia a inner join empleado e on a.legajo=e.legajo where id=?";
	
	private static final String  update = "update asistencia set legajo=?, id_sucursal=?, id_dia=?, idEstado=?, horaDesde=?,  horaHasta=? where id=?";
	
	@Override
	public Asistencia readAsistencia_Empleado_Dia_Sucursal(Date fecha, long legajo, long idSucursal) {
		PreparedStatement statement;
		ResultSet resultSet; 
		Conexion conexion = Conexion.getConexion();
		Asistencia asistencia = new Asistencia();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAsistenciaEmpleadoEnUnDiaParticular);
			statement.setLong(1, legajo);
			statement.setLong(2, idSucursal);
			statement.setDate(3,fecha);
			statement.setInt(4,1);
			statement.setInt(5,1);
			
			resultSet = statement.executeQuery();
			resultSet.next();
			asistencia = getAsistencia(resultSet);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return asistencia;
	}
	
	public boolean update(Asistencia asistencia) {
				
					PreparedStatement statement;
					Connection conexion = Conexion.getConexion().getSQLConexion();
					boolean isUpdateExitoso = false;
					try {
	
						statement = conexion.prepareStatement(update);
								
						statement.setLong(1, asistencia.getDiaLaboral().getLegajo().getLegajo()); // HAY SETLONG
						statement.setLong(2, asistencia.getDiaLaboral().getSucPorDia().getSucursal().getId());
						statement.setLong(3, asistencia.getDiaLaboral().getSucPorDia().getDia().getId());
						statement.setLong(4, asistencia.getEstadoAsist().getId());
						statement.setTime(5,Time.valueOf(asistencia.getHoraDesde()));
						statement.setTime(6, Time.valueOf(asistencia.getHoraHasta()));
						statement.setLong(7, asistencia.getId());
	
						if (statement.executeUpdate() > 0) {
							conexion.commit();
							isUpdateExitoso = true;
						}
					} catch (SQLException e) {
						e.printStackTrace();
						try {
							conexion.rollback();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
	
					return isUpdateExitoso;
			}
	
	
	private Asistencia getAsistencia(ResultSet resultSet) throws SQLException{
		
		long id = resultSet.getLong("id");
		LocalDate fecha = resultSet.getDate("fecha").toLocalDate();
		Time horaDesde = resultSet.getTime("horaDesde");
		Time horaHasta = resultSet.getTime("horaHasta");
	
		long legajo = resultSet.getLong("e_legajo");
		long dni = resultSet.getLong("e_dni");
		String nombre = resultSet.getString("e_nombre"); 
		String apellido = resultSet.getString("e_apellido"); 
		
		Empleado empleado = new Empleado(legajo,dni, nombre, apellido);
	
		boolean estado = resultSet.getBoolean("estado");
		Asistencia asistencia = new Asistencia(id, fecha, horaDesde.toLocalTime(), horaHasta.toLocalTime(), estado, new DiaLaboral(empleado));
		return asistencia;
	}

	@Override
	public List<Asistencia> readAllAsistenciaEmpleado(Date fecha, long idSucursal) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Asistencia> lAsistencia = new ArrayList<Asistencia>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readAllEmpleado);
			statement.setLong(1, idSucursal);
			statement.setDate(2,fecha);
			statement.setInt(3,1);
			statement.setInt(4,1);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				lAsistencia.add(getAsistencia(resultSet));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return lAsistencia;
	}

	@Override
	public Asistencia readOne(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Asistencia asistencia = new Asistencia();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readOneAsistencia);
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				asistencia = getAsistencia(resultSet);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return asistencia;

	}

}
