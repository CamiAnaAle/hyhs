package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Localidad;
import com.modelo.clases.Provincia;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE LOCALIDAD DAO PARA LA PERSISTENCIA EN BDD.
public class LocalidadDAOSQL implements LocalidadDAO
{
	private static final String insert = "INSERT INTO localidad ( `cp`, `descripcion`, `id_provincia`)   VALUES(?, ?,?)";
	
	
	private static final String readall = "SELECT * FROM localidad";
	private static final String traerPorNombre = "SELECT id FROM localidad Where descripcion = ?";
	private static final String readForId = "SELECT * FROM localidad WHERE id = ? ";
	private static final String readForLocalidad = "SELECT * FROM localidad WHERE id_provincia = ? ";

	
	public boolean insert(Localidad localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, localidad.getCp());
			statement.setString(2, localidad.getDescripcion());
			statement.setLong(3, localidad.getProvincia().getId());
			if(statement.executeUpdate() > 0) //Si se ejecutï¿½ devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	public List<Localidad> readAllLocalidadProvincia(Provincia p)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Localidad> localidades = new ArrayList<Localidad>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForLocalidad);
			statement.setInt(1, (int) p.getId());
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{				
			localidades.add(new Localidad(resultSet.getLong("id"), resultSet.getInt("cp"),resultSet.getString("descripcion"),p));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidades;
	}

	
	public List<Localidad> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Localidad> localidades = new ArrayList<Localidad>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				

				Provincia provincia = new DAOSQLFactory().createProvinciaDAO().readForId(resultSet.getInt("id_provincia"));
		 			
				
			localidades.add(new Localidad(resultSet.getLong("id"), resultSet.getInt("cp"),resultSet.getString("descripcion"),provincia));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidades;
	}
	
	public Localidad readForId(int idLocalidad) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idLocalidad);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				Provincia provincia = new DAOSQLFactory().createProvinciaDAO().readForId(resultSet.getInt("id_provincia"));
	 			
				
				return new Localidad(resultSet.getLong("id"), resultSet.getInt("cp"),resultSet.getString("descripcion"),provincia);
			
			}	
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("id");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;
	}


	@Override
	public boolean update(Localidad localidad) {
		// TODO Auto-generated method stub
		return false;
	}


}
