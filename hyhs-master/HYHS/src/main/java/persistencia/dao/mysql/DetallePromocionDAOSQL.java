package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.DetallePromocion;
import com.modelo.clases.Promocion;
import com.modelo.clases.Servicio;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DetallePromocionDAO;

public class DetallePromocionDAOSQL implements DetallePromocionDAO {

	private static final String readForIdPromocion = "SELECT * FROM detallepromocion WHERE id_promocion = ? ";

	@Override
	public List<DetallePromocion> readByID(int idPromo) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<DetallePromocion> detalles = new ArrayList<DetallePromocion>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readForIdPromocion);
			statement.setInt(1, idPromo);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				detalles.add(getDetalles(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return detalles;
	}

	private DetallePromocion getDetalles(ResultSet resultSet) throws SQLException {
		
		long id = resultSet.getLong("id");
		Servicio servicio = new DAOSQLFactory().createServicioDAO().readForId(resultSet.getInt("id_servicio"));
		Promocion promocion = null; //TODO
		boolean estado = resultSet.getBoolean("estado");
		
		DetallePromocion unDetalle = new DetallePromocion(id, servicio, promocion, estado);
		
		return unDetalle;
	}

}
