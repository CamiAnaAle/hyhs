package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DetallePromocionDAO;
import persistencia.dao.interfaz.DetalleTurnoDAO;
import persistencia.dao.interfaz.EmpleadoDAO;
import persistencia.dao.interfaz.EstadoPromocionDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.ProductoDAO;
import persistencia.dao.interfaz.PromocionDAO;
import persistencia.dao.interfaz.ProvinciaDAO;
import persistencia.dao.interfaz.ServicioDAO;
import persistencia.dao.interfaz.SucursalDAO;
import persistencia.dao.interfaz.TipoClienteDAO;
import persistencia.dao.interfaz.TipoPromocionDAO;
import persistencia.dao.interfaz.TurnoDAO;
import persistencia.dao.interfaz.AsistenciaDAO;
import persistencia.dao.interfaz.CategoriaProductoDAO;
import persistencia.dao.interfaz.ClienteDAO;

public class DAOSQLFactory implements DAOAbstractFactory {

	public ClienteDAO createClienteDAO() {
		return new ClienteDAOSQL();
	}

	public EmpleadoDAO createEmpleadoDAO() {
		return new EmpleadoDAOSQL();
	}

	public EstadoPromocionDAO createEstadoPromocionDAO() {
		return new EstadoPromocionDAOSQL();
	}

	public LocalidadDAO createLocalidadDAO() {
		return new LocalidadDAOSQL();
	}

	public PaisDAO createPaisDAO() {
		return new PaisDAOSQL();
	}

	public PromocionDAO createPromocionDAO() {
		return new PromocionDAOSQL();
	}

	public ServicioDAO createServicioDAO() {
		return new ServicioDAOSQL();
	}

	public TipoClienteDAO createTipoClienteDAO() {
		return new TipoClienteDAOSQL();
	}

	public TipoPromocionDAO createTipoPromocionDAO() {
		return new TipoPromocionDAOSQL();
	}

	public ProvinciaDAO createProvinciaDAO() {
		return new ProvinciaDAOSQL();
	}

	public CategoriaProductoDAO createCategoriaProductoDAO() {
		return new CategoriaProductoDAOSQL();
	}

	public ProductoDAO createProductoDAO() {
		return new ProductoDAOSQL();
	}

	@Override
	public SucursalDAO createSucursalDAO() {
		return new SucursalDAOSQL();
	}

	@Override
	public TurnoDAO createTurnoDAO() {
		return new TurnoDAOSQL();
	}

	@Override
	public AsistenciaDAO createAsistenciaDAO() {
		return new AsistenciaDAOSQL();
	}

	@Override
	public DetallePromocionDAO createDetallePromoDAO() {
		return new DetallePromocionDAOSQL();
	}

	public DetalleTurnoDAO createDetalleTurnoDAO() {
		return new DetalleTurnoDAOSQL();
	}
}