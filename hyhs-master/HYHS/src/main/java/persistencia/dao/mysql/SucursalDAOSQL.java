
package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Cliente;
import com.modelo.clases.Localidad;
import com.modelo.clases.Sucursal;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.SucursalDAO;

public class SucursalDAOSQL implements SucursalDAO {

	private static final String insert = "INSERT `sucursal` (`nombre`,`direccion`,`telefono`, `email`,`id_localidad`, `estado`)"
			+ " VALUES(?,?,?, ?, ?, ?)";

	private static final String readall = "SELECT * FROM sucursal";

	private static final String read = "SELECT * FROM sucursal where  like '%?%' or direccion  like '%?%'  "
			+ "or telefono  like '%?%' or email  like '%?%' ";
	
	private static final String readForId = "SELECT * FROM sucursal WHERE id = ? ";

	public boolean insert(Sucursal sucursal) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {

			statement = conexion.prepareStatement(insert);
			statement.setString(1, sucursal.getNombre());
			statement.setString(2, sucursal.getDireccion());
			statement.setString(3, sucursal.getTelefono());
			statement.setString(4, sucursal.getEmail());
			statement.setLong(5, sucursal.getLocalidad().getId());
			statement.setBoolean(6, sucursal.isEstado());
			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}

//
	public boolean update(Sucursal sucursal_modificar) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Sucursal> read(String texto_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setString(1, texto_a_buscar);
			statement.setString(2, texto_a_buscar);
			statement.setString(3, texto_a_buscar);
			statement.setString(4, texto_a_buscar);
			statement.setString(5, texto_a_buscar);
			statement.setString(6, texto_a_buscar);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				sucursales.add(getSucursal(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sucursales;

	}

	public List<Sucursal> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				sucursales.add(getSucursal(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sucursales;

	}

	public Sucursal getSucursal(ResultSet resultSet) throws SQLException {

		long id = resultSet.getLong("id");
		String nombre = resultSet.getString("nombre");
		String direccion = resultSet.getString("direccion");
		String telefono = resultSet.getString("telefono");
		String email = resultSet.getString("email");
		//Time horaInicio = resultSet.getTime("horaInicio");
		//Time horaFin = resultSet.getTime("horaFin");
		boolean estado = resultSet.getBoolean("estado");

		Localidad localidad = new DAOSQLFactory().createLocalidadDAO().readForId(resultSet.getInt("id_localidad"));

		return new Sucursal(id, nombre, direccion, telefono, email, localidad, estado);
	}

	@Override
	public Sucursal readOne(int idSucursal) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Sucursal sucursal = new Sucursal();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idSucursal);
			resultSet = statement.executeQuery();
			resultSet.next();
			sucursal = getSucursal(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sucursal;
	}

}
