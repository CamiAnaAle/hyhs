package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.modelo.clases.EstadoPromocion;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EstadoPromocionDAO;

public class EstadoPromocionDAOSQL implements EstadoPromocionDAO {

	private static final String insert = "INSERT INTO estadoPromocion(descripcion, estado) VALUES(?, ?)";

	private static final String readId = "SELECT * FROM estadoPromocion where id=?";

	private static final String readall = "SELECT * FROM estadoPromocion WHERE estado = 'true'";

	
	public  EstadoPromocion readForId(int idEstadoPromocion) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		EstadoPromocion estadopromocion = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readId);
			statement.setString(1, Integer.toString(idEstadoPromocion));
			resultSet = statement.executeQuery();
			resultSet.next();
			estadopromocion = new EstadoPromocion(idEstadoPromocion, resultSet.getString("descripcion"), resultSet.getBoolean("estado"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return estadopromocion;
	}

	public boolean insert(EstadoPromocion estadopromocion) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setLong(1, estadopromocion.getId());
			statement.setString(2, estadopromocion.getDescripcion());
			statement.setBoolean(3, estadopromocion.isEstado());
			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}


	public List<EstadoPromocion> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<EstadoPromocion> estadospromociones = new ArrayList<EstadoPromocion>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				estadospromociones.add(getEstadosdePromociones(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return estadospromociones;
	}

	private EstadoPromocion getEstadosdePromociones(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("descripcion");
		boolean estado = resultSet.getBoolean("estado");
		return new EstadoPromocion(id, nombre, estado);
	}



}
