

package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.CategoriaProducto;
import com.modelo.clases.EstadoPromocion;
import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.CategoriaProductoDAO;
import persistencia.dao.interfaz.EstadoPromocionDAO;

public class CategoriaProductoDAOSQL implements CategoriaProductoDAO {

	private static final String insert = "INSERT INTO categoria ( `descripcion`,`estado`)   VALUES(?,?)";
	
	
	private static final String readall = "SELECT * FROM provincia";
	private static final String traerPorNombre = "SELECT id FROM categoria where descripcion = ?";
	private static final String readForId = "SELECT * FROM categoria WHERE id = ? ";

	public boolean insert(CategoriaProducto categoria) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, categoria.getDescripcion());
			statement.setBoolean(2, categoria.isEstado());
			if(statement.executeUpdate() > 0) //Si se ejecutï¿½ devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	
	public List<CategoriaProducto> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<CategoriaProducto> categorias = new ArrayList<CategoriaProducto>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				categorias.add( new CategoriaProducto(resultSet.getLong("id"),resultSet.getString("descripcion"),resultSet.getBoolean("estado")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return categorias;
	}
	
	public CategoriaProducto readForId(int idCategoria) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idCategoria);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				
				return new CategoriaProducto(resultSet.getLong("id"),resultSet.getString("descripcion"),resultSet.getBoolean("estado"));
			}	
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("id");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;
	}


}
