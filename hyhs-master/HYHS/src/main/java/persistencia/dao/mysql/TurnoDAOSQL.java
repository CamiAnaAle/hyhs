package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Cliente;
import com.modelo.clases.Empleado;
import com.modelo.clases.MedioDePago;
import com.modelo.clases.Sucursal;
import com.modelo.clases.Turno;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TurnoDAO;

public class TurnoDAOSQL implements TurnoDAO {
	
	private static final String insert = "INSERT INTO turno(fecha, tiempoTotal, precioTotal, pagoRealizado, id_medioPago, id_cliente, id_sucursal,"
			+ "id_legajo, plataReal, plataCanjeada, puntosCanjeados, puntosTotalGanados, estado)"
			+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String readByFecha = "SELECT * FROM turno where fecha like ?";

	
	@Override
	public boolean insert(Turno turno_a_agregar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {

			java.sql.Date sqlFechaDeTurno = java.sql.Date.valueOf(turno_a_agregar.getFecha());
			java.sql.Time sqlTiempoDeTurno = turno_a_agregar.getTiempoTotal(); //VERIFICAR

			statement = conexion.prepareStatement(insert);
			statement.setDate(1, sqlFechaDeTurno);
			statement.setTime(2, sqlTiempoDeTurno);								//VERIFICAR
			statement.setDouble(3, turno_a_agregar.getPrecioTotal());
			statement.setBoolean(4, turno_a_agregar.isPagado());
			statement.setLong(5, turno_a_agregar.getMedio().getId());
			statement.setLong(6, turno_a_agregar.getCliente().getIdCliente());
			statement.setLong(7, turno_a_agregar.getSucursal().getId());
			statement.setLong(8, turno_a_agregar.getEmpleado().getLegajo());
			statement.setDouble(9, turno_a_agregar.getPlataReal());
			statement.setDouble(10, turno_a_agregar.getPlataCanjeada());
			statement.setInt(11, turno_a_agregar.getPuntosCanjeados());
			statement.setInt(12, turno_a_agregar.getPuntosGanados());
			statement.setBoolean(13, turno_a_agregar.isEstado());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isInsertExitoso;
	}

	@Override
	public boolean update(Turno turno_a_modificar) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Turno> readAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Turno> readByFecha(LocalDate fecha_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Turno> turnos = new ArrayList<Turno>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByFecha);
			statement.setDate(1,Date.valueOf(fecha_a_buscar));
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				turnos.add(getTurno(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return turnos;
	}

	@Override
	public List<Turno> read(String texto_a_buscar) {
		// TODO Auto-generated method stub
		return null;
	}

	private Turno getTurno(ResultSet resultSet) throws SQLException{
		
		long id = resultSet.getLong("id");
		LocalDate fecha = resultSet.getDate("fecha").toLocalDate();
		Time tiempoTotal = resultSet.getTime("tiempoTotal");
		double precioTotal = resultSet.getDouble("precioTotal");
		boolean pagado = resultSet.getBoolean("pagoRealizado");
		MedioDePago medio = MedioDePagoDAOSQL.readForId(resultSet.getInt("id_medioPago"));
		Cliente cliente = new DAOSQLFactory().createClienteDAO().readOne(resultSet.getInt("id_cliente"));
		Sucursal sucursal = new DAOSQLFactory().createSucursalDAO().readOne(resultSet.getInt("id_sucursal"));
		Empleado empleado = null;// TODO Auto-generated method stub
		boolean estado = resultSet.getBoolean("estado");
		double plataReal = resultSet.getDouble("plataReal");
		double plataCanjeada = resultSet.getDouble("plataCanjeada");
		int puntosCanjeados = resultSet.getInt("puntosCanjeados");
		int puntosGanados = resultSet.getInt("puntosTotalGanados");
		
		Turno unTurno = new Turno(id, fecha, tiempoTotal, precioTotal, pagado, medio, cliente, sucursal, empleado, estado, plataReal, plataCanjeada, puntosCanjeados, puntosGanados);
		
		return unTurno;
	}
}
