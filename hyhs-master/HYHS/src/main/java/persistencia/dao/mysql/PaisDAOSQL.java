package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.modelo.clases.Pais;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE LOCALIDAD DAO PARA LA PERSISTENCIA EN BDD.
public class PaisDAOSQL implements PaisDAO
{
	private static final String insert = "INSERT INTO pais ( `descripcion`)   VALUES(?)";
	
	
	private static final String readall = "SELECT * FROM pais";
	private static final String traerPorNombre = "SELECT id FROM pais Where descripcion = ?";
	private static final String readForId = "SELECT * FROM pais WHERE id = ? ";

	public boolean insert(Pais pais) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, pais.getDescripcion());
			if(statement.executeUpdate() > 0) //Si se ejecutï¿½ devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	
	public List<Pais> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Pais> paises = new ArrayList<Pais>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				
		paises.add(new Pais(resultSet.getInt("id"), resultSet.getString("descripcion")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return paises;
	}
	
	public Pais readForId(int idpais) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idpais);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
		return new Pais(resultSet.getInt("id"), resultSet.getString("descripcion"));
			}	
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("id");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;
	}




}
