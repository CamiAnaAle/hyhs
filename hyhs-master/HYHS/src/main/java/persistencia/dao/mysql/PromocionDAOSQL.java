package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.modelo.clases.EstadoPromocion;
import com.modelo.clases.Promocion;
import com.modelo.clases.TipoPromocion;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PromocionDAO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE SERVICIO DAO PARA LA PERSISTENCIA EN BDD.
public class PromocionDAOSQL implements PromocionDAO {
	private static final String insert = "INSERT INTO `promocion` (`descripcion` , `precio`, `puntos`, `fechaDesde`, `fechaHasta`,    `id_Estado`,    `id_tipo` ) values(?,?,?,?,?,?,?)";

	private static final String readall = "SELECT * FROM promocion";
	private static final String readByVigentes = "SELECT * FROM PROMOCION WHERE id_Estado = ?";
	private static final String traerPorNombre = "SELECT id FROM promocion Where descripcion = ?";
	private static final String readForId = "SELECT * FROM promocion WHERE id = ? ";

	public boolean insert(Promocion promocion) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {

			java.sql.Date sqlFechaDesde = java.sql.Date.valueOf(promocion.getFechaDesde());
			java.sql.Date sqlFechaHasta = java.sql.Date.valueOf(promocion.getFechaHasta());

			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, promocion.getDescripcion());
			statement.setDouble(2, promocion.getPrecio());
			statement.setInt(3, promocion.getPuntos());
			statement.setDate(4, sqlFechaDesde);
			statement.setDate(5, sqlFechaHasta);
			statement.setLong(6, promocion.getEstado().getId());
			statement.setLong(7, promocion.getTipo().getId());
			if (statement.executeUpdate() > 0) // Si se ejecutï¿½ devuelvo true
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<Promocion> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Promocion> promociones = new ArrayList<Promocion>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				EstadoPromocion estadopromocion = new DAOSQLFactory().createEstadoPromocionDAO()
						.readForId(resultSet.getInt("id_estado"));
				TipoPromocion tipopromocion = new DAOSQLFactory().createTipoPromocionDAO()
						.readForId(resultSet.getInt("id_tipo"));

				promociones.add(new Promocion(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getInt("puntos"),
						resultSet.getDate("fechaDesde").toLocalDate(), resultSet.getDate("fechaHasta").toLocalDate(),
						estadopromocion, tipopromocion));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return promociones;
	}

	public Promocion readForId(int promocion) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, promocion);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				EstadoPromocion estadopromocion = new DAOSQLFactory().createEstadoPromocionDAO()
						.readForId(resultSet.getInt("id_estado"));
				TipoPromocion tipopromocion = new DAOSQLFactory().createTipoPromocionDAO()
						.readForId(resultSet.getInt("id_tipo"));

				return new Promocion(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getInt("puntos"),
						resultSet.getDate("fechaDesde").toLocalDate(), resultSet.getDate("fechaHasta").toLocalDate(),
						estadopromocion, tipopromocion);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				return resultSet.getInt("id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean update(Promocion promocion) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Promocion> readByVigencia(int estado) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Promocion> promociones = new ArrayList<Promocion>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByVigentes);
			statement.setInt(1, estado);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				EstadoPromocion estadopromocion = new DAOSQLFactory().createEstadoPromocionDAO()
						.readForId(resultSet.getInt("id_estado"));
				TipoPromocion tipopromocion = new DAOSQLFactory().createTipoPromocionDAO()
						.readForId(resultSet.getInt("id_tipo"));

				promociones.add(new Promocion(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getInt("puntos"),
						resultSet.getDate("fechaDesde").toLocalDate(), resultSet.getDate("fechaHasta").toLocalDate(),
						estadopromocion, tipopromocion));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return promociones;
	}
}
