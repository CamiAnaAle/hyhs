package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Asistencia;
import com.modelo.clases.CategoriaProducto;
import com.modelo.clases.DetalleTurno;
import com.modelo.clases.EstadoDetalleTurno;
import com.modelo.clases.Producto;
import com.modelo.clases.Promocion;
import com.modelo.clases.Servicio;
import com.modelo.clases.Turno;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DetalleTurnoDAO;

public class DetalleTurnoDAOSQL implements DetalleTurnoDAO{

	
	private static final String readall = "select * from detalleturno where id_asistencia=? and estado=? order by horaInicio asc ";
	
	@Override
	public List<DetalleTurno> readAllDetalleTurno_Asistencia(Asistencia a) {
		PreparedStatement statement;
		ResultSet resultSet;
		
		ArrayList<DetalleTurno> listaDetalles = new ArrayList<DetalleTurno>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1, (int)a.getId());
			statement.setInt(2, 1);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				listaDetalles.add(getDetalle(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaDetalles;
	}
	
		private DetalleTurno getDetalle(ResultSet resultSet) throws SQLException {
			
			long id = resultSet.getLong("id");
			LocalTime horaInicio = resultSet.getTime("horaInicio").toLocalTime();
			LocalTime horaFin = resultSet.getTime("horaFin").toLocalTime();
			LocalTime tiempoEstimado = resultSet.getTime("tiempo_estimado").toLocalTime();
			LocalTime tiempoReal = resultSet.getTime("tiempo_real").toLocalTime();
			boolean  estado = resultSet.getBoolean("estado");
			
			long id_Asistencia = resultSet.getLong("id_asistencia");
			long id_turno = resultSet.getLong("id_turno");		
			long id_servicio= resultSet.getLong("id_servicio");		
			long id_producto= resultSet.getLong("id_producto");		
			long id_promocion= resultSet.getLong("id_promocion");		
			
			Asistencia asistencia = new Asistencia();
			Turno turno = new Turno();
			Servicio servicio = new Servicio();
			Producto producto = new Producto();
			Promocion promocion = new Promocion();
				
			asistencia = new Asistencia(id_Asistencia);
			turno = new Turno(id_turno);
			servicio = new Servicio(id_servicio,"");
			producto = new Producto(id_producto,"");
			promocion = new Promocion(id_promocion);

			DetalleTurno dt = new DetalleTurno(id, horaInicio, horaFin, tiempoEstimado, tiempoReal, estado, asistencia, turno, servicio, promocion, producto);
			return dt;
		}


}
