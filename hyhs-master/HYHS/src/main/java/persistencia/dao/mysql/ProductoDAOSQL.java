package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.CategoriaProducto;
import com.modelo.clases.Producto;
import com.modelo.clases.Sucursal;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProductoDAO;

public class ProductoDAOSQL implements ProductoDAO{
	private static final String insert = "INSERT `producto` (`nombre`, `descripcion`, `stock`, `precioCosto`, `precioVenta`,`id_categoria`, `estado` )" + 
			" VALUES(?,?,?, ?, ?, ?,?)";
	
	private static final String readall = "SELECT * FROM producto";

	
	
	public boolean insert(Producto producto) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {

			statement = conexion.prepareStatement(insert);
			statement.setString(1, producto.getNombre()); 			
			statement.setString(2, producto.getDescripcion()); 				
			statement.setInt(3, producto.getStock());
			statement.setDouble(4, producto.getPrecioDeCompra());
			statement.setDouble(5, producto.getPrecioDeVenta());
			statement.setLong(6, producto.getCategoria().getId());
			statement.setBoolean(7, producto.isEstado());	
			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}

	public List<Producto> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Producto> productos = new ArrayList<Producto>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				productos.add(getProducto(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return productos;
	
	}
	private Producto getProducto(ResultSet resultSet) throws SQLException {
		
		
		long id = resultSet.getLong("id");
		String nombre = resultSet.getString("nombre");
		String descripcion = resultSet.getString("descripcion");
		int stock = resultSet.getInt("stock");
		double precioCosto = resultSet.getDouble("precioCosto");
		double  precioVenta= resultSet.getDouble("precioVenta");
		boolean  estado = resultSet.getBoolean("estado");
		
		CategoriaProducto  categoria = new DAOSQLFactory().createCategoriaProductoDAO().readForId(resultSet.getInt("id_categoria"));
 	
		return new Producto (id,nombre, descripcion, stock, precioCosto, precioVenta,categoria,estado);
		
		
	}

	
	
	
}
