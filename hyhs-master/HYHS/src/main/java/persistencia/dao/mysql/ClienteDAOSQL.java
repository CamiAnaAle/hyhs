package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Cliente;
import com.modelo.clases.Localidad;
import com.modelo.clases.TipoCliente;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ClienteDAO;

public class ClienteDAOSQL implements ClienteDAO {

	private static final String insert = "INSERT INTO cliente(dni, nombre, apellido, fechaNto, direccion, telefono, celular,"
			+ "email, fechaActualizacionDeuda, totalAdeudado, id_localidad, id_tipo, cantTurnoSolicitado, cantPresentes, cantAusentes, cantCanceladosCliente, cantCanceladosEmpresa, cantPuntos, estado)"
			+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String readall = "SELECT * FROM cliente";
	
	private static final String readultimoid="SELECT count(1) from cliente";
	
	private static final String readOne = "SELECT * FROM cliente where id=?";
	
	private static final String update = "UPDATE cliente set dni=? , nombre= ?, apellido=?, fechaNto=?, direccion=?, telefono=?, celular=?,"
			+ " email=?, fechaActualizacionDeuda=?, totalAdeudado=?, id_localidad=?, id_tipo=?, cantTurnoSolicitado=?, cantPresentes=?, cantAusentes=?, cantCanceladosCliente=?, cantCanceladosEmpresa=?, cantPuntos=?, estado=? "
			+ " where id=?";

	private static final String read = "SELECT * FROM cliente where id like ? or dni like ? or nombre  like ? " + 
			"or apellido  like ?  or direccion  like ?  or telefono  like  ? or " + 
			"celular like ? or email  like ?  or totalAdeudado  like  ? or  cantTurnoSolicitado  like  ?  or cantPresentes  like  ?  or cantAusentes like  ?  or cantCanceladosCliente like  ?  or cantCanceladosEmpresa like  ?  or cantPuntos like  ?  ";
	 
	
	private static final String readBusquedaCorta = "SELECT * FROM cliente where id like ? or dni like ? or nombre like ? " + 
			"or apellido  like ? or telefono like ? or email like ? or direccion like ?";
		
	private static final String readByFecha = "SELECT * FROM cliente where fechaNto like ? or fechaActualizacionDeuda  like ?  ";
	
	public boolean insert(Cliente cliente) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {

			java.sql.Date sqlDateNacimiento = java.sql.Date.valueOf(cliente.getNacimiento());
			java.sql.Date sqlDateFechaActualizaDeuda = java.sql.Date.valueOf(cliente.getActualizacionDeuda());

			statement = conexion.prepareStatement(insert);
			statement.setLong(1, cliente.getDni()); 				// HAY SETLONG
			statement.setString(2,cliente.getNombre());
			statement.setString(3,cliente.getApellido());
			statement.setDate(4, sqlDateNacimiento);
			statement.setString(5, cliente.getDireccion());
			statement.setString(6, cliente.getTelefono());
			statement.setString(7, cliente.getCelular());
			statement.setString(8, cliente.getEmail());
			statement.setDate(9, sqlDateFechaActualizaDeuda);
			statement.setInt(10, cliente.getTotalAdeudado());
			statement.setLong(11, cliente.getLocalidad().getId());	// HAY SETLONG
			statement.setInt(12, cliente.getTipoDeCliente().getId());
			statement.setInt(13, cliente.getCantTurnosSolicitados());
			statement.setInt(14, cliente.getCantTurnosPresente());
			statement.setInt(15, cliente.getCantTurnosAusente());
			statement.setInt(16, cliente.getCantTurnosCanceladoCliente());
			statement.setInt(17, cliente.getCantTurnosCanceladoEmpresa());
			statement.setInt(18, cliente.getPuntos());
			statement.setBoolean(19, cliente.getEstado());			// HAY SETBOOLEAN

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isInsertExitoso;
	}

	public boolean update(Cliente cliente) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {

			java.sql.Date sqlDateNacimiento = java.sql.Date.valueOf(cliente.getNacimiento());
			java.sql.Date sqlDateFechaActualizaDeuda = java.sql.Date.valueOf(cliente.getActualizacionDeuda());

			statement = conexion.prepareStatement(update);
			statement.setLong(1, cliente.getDni()); 				// HAY SETLONG
			statement.setString(2,cliente.getNombre());
			statement.setString(3,cliente.getApellido());
			statement.setDate(4, sqlDateNacimiento);
			statement.setString(5, cliente.getDireccion());
			statement.setString(6, cliente.getTelefono());
			statement.setString(7, cliente.getCelular());
			statement.setString(8, cliente.getEmail());
			statement.setDate(9, sqlDateFechaActualizaDeuda);
			statement.setInt(10, cliente.getTotalAdeudado());
			statement.setLong(11, cliente.getLocalidad().getId());	// HAY SETLONG
			statement.setInt(12, cliente.getTipoDeCliente().getId());
			statement.setInt(13, cliente.getCantTurnosSolicitados());
			statement.setInt(14, cliente.getCantTurnosPresente());
			statement.setInt(15, cliente.getCantTurnosAusente());
			statement.setInt(16, cliente.getCantTurnosCanceladoCliente());
			statement.setInt(17, cliente.getCantTurnosCanceladoEmpresa());
			statement.setInt(18, cliente.getPuntos());
			statement.setBoolean(19, cliente.getEstado());			// HAY SETBOOLEAN
			statement.setLong(20, cliente.getIdCliente());		

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isUpdateExitoso;
	}
	
	public List<Cliente> readBusquedaCorta(String texto_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		System.out.print(texto_a_buscar);
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readBusquedaCorta);
			statement.setString(1,"%"+texto_a_buscar+"%");
			statement.setString(2,"%"+texto_a_buscar+"%");
			statement.setString(3,"%"+texto_a_buscar+"%");
			statement.setString(4,"%"+texto_a_buscar+"%");
			statement.setString(5,"%"+texto_a_buscar+"%");
			statement.setString(6,"%"+texto_a_buscar+"%");
			statement.setString(7,"%"+texto_a_buscar+"%");
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				clientes.add(getCliente(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientes;
	
	}
	
	public List<Cliente> read(String texto_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(read);
			statement.setString(1,"%"+texto_a_buscar+"%");
			statement.setString(2,"%"+texto_a_buscar+"%");
			statement.setString(3,"%"+texto_a_buscar+"%");
			statement.setString(4,"%"+texto_a_buscar+"%");
			statement.setString(5,"%"+texto_a_buscar+"%");
			statement.setString(6,"%"+texto_a_buscar+"%");
			statement.setString(7,"%"+texto_a_buscar+"%");
			statement.setString(8,"%"+texto_a_buscar+"%");
			statement.setString(9,"%"+texto_a_buscar+"%");
			statement.setString(10,"%"+texto_a_buscar+"%");
			statement.setString(11,"%"+texto_a_buscar+"%");
			statement.setString(12,"%"+texto_a_buscar+"%");
			statement.setString(13,"%"+texto_a_buscar+"%");
			statement.setString(14,"%"+texto_a_buscar+"%");
			statement.setString(15,"%"+texto_a_buscar+"%");
			
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				clientes.add(getCliente(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientes;
	
	}
	
	
	public List<Cliente> readByFecha(Date fecha_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByFecha);
			statement.setDate(1,fecha_a_buscar);
			statement.setDate(2,fecha_a_buscar);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				clientes.add(getCliente(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientes;
	}
	
	

	public List<Cliente> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				clientes.add(getCliente(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientes;
	}
	

	public int readUltimoId() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(readultimoid);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	public boolean existe(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Cliente cliente = new Cliente();
		Conexion conexion = Conexion.getConexion();
		boolean estado=false;
		try {
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				estado=true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return estado;
	}
	

	public Cliente readOne(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Cliente cliente = new Cliente();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			resultSet.next();
			cliente= getCliente(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cliente;
	}
	
	
	

	private Cliente getCliente(ResultSet resultSet) throws SQLException { //FALTA DEFINIR LOCALIDAD
		long id = resultSet.getLong("id");
		long dni = resultSet.getLong("dni");
		String nombre = resultSet.getString("nombre");
		String apellido = resultSet.getString("apellido");
		LocalDate nacimiento = resultSet.getDate("fechaNto").toLocalDate();
		String direccion = resultSet.getString("direccion");
		String telefono = resultSet.getString("telefono");
		String celular = resultSet.getString("celular");
		String email = resultSet.getString("email");
		LocalDate actualizacionDeuda = resultSet.getDate("fechaActualizacionDeuda").toLocalDate();
		int totalAdeudado = resultSet.getInt("totalAdeudado");
		Localidad localidad = new DAOSQLFactory().createLocalidadDAO().readForId(resultSet.getInt("id_localidad"));
		TipoCliente tipo = TipoClienteDAOSQL.getById(resultSet.getInt("id_tipo"));
		int cantTurnoSolicitado = resultSet.getInt("cantTurnoSolicitado");
		int cantPresentes = resultSet.getInt("cantPresentes");
		int cantAusentes = resultSet.getInt("cantAusentes");
		int cantCanceladosCliente = resultSet.getInt("cantCanceladosCliente");
		int cantCanceladosEmpresa = resultSet.getInt("cantCanceladosEmpresa");
		int cantPuntos = resultSet.getInt("cantPuntos");
		boolean estado = resultSet.getBoolean("estado");
		
		return new Cliente(id, dni, nombre, apellido, nacimiento, direccion, telefono, celular, email, 
				actualizacionDeuda, totalAdeudado, localidad, tipo, cantTurnoSolicitado, cantPresentes, 
				cantAusentes, cantCanceladosCliente, cantCanceladosEmpresa, cantPuntos, estado);
	}

	/*
	 * public boolean insert(Cliente persona) { PreparedStatement statement;
	 * Connection conexion = Conexion.getConexion().getSQLConexion(); boolean
	 * isInsertExitoso = false; try { statement = conexion.prepareStatement(insert);
	 * statement.setInt(1, persona.getIdPersona()); statement.setString(2,
	 * persona.getNombre()); statement.setString(3, persona.getTelefono());
	 * if(statement.executeUpdate() > 0) { conexion.commit(); isInsertExitoso =
	 * true; } } catch (SQLException e) { e.printStackTrace(); try {
	 * conexion.rollback(); } catch (SQLException e1) { e1.printStackTrace(); } }
	 * 
	 * return isInsertExitoso; }
	 * 
	 * public boolean delete(Cliente persona_a_eliminar) { PreparedStatement
	 * statement; Connection conexion = Conexion.getConexion().getSQLConexion();
	 * boolean isdeleteExitoso = false; try { statement =
	 * conexion.prepareStatement(delete); statement.setString(1,
	 * Integer.toString(persona_a_eliminar.getIdPersona()));
	 * if(statement.executeUpdate() > 0) { conexion.commit(); isdeleteExitoso =
	 * true; } } catch (SQLException e) { e.printStackTrace(); } return
	 * isdeleteExitoso; }
	 * 
	 * public List<Cliente> readAll() { PreparedStatement statement; ResultSet
	 * resultSet; //Guarda el resultado de la query ArrayList<Cliente> personas =
	 * new ArrayList<Cliente>(); Conexion conexion = Conexion.getConexion(); try {
	 * statement = conexion.getSQLConexion().prepareStatement(readall); resultSet =
	 * statement.executeQuery(); while(resultSet.next()) {
	 * personas.add(getPersonaDTO(resultSet)); } } catch (SQLException e) {
	 * e.printStackTrace(); } return personas; }
	 * 
	 * private Cliente getPersonaDTO(ResultSet resultSet) throws SQLException { int
	 * id = resultSet.getInt("idPersona"); String nombre =
	 * resultSet.getString("Nombre"); String tel = resultSet.getString("Telefono");
	 * return new Cliente(id, nombre, tel); }
	 */
}
//}