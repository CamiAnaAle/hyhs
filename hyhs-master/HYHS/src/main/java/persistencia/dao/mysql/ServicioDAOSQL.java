package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.modelo.clases.Servicio;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ServicioDAO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE SERVICIO DAO PARA LA PERSISTENCIA EN BDD.
public class ServicioDAOSQL implements ServicioDAO {
	private static final String insert = "INSERT INTO `servicio` ( `descripcion`,    `precio`,    `tiempo_estimado`,    `estado`)   VALUES(?, ?,?,?)";

	private static final String readall = "SELECT * FROM servicio";
	private static final String traerPorNombre = "SELECT id FROM servicio Where descripcion = ?";
	private static final String readForId = "SELECT * FROM servicio WHERE id = ? ";
	private static final String read = "SELECT * FROM servicio where descripcion like '%?%' or precio  like '%?%' "
			+ "or tiempo_estimado  like '%?%'";

	public boolean insert(Servicio servicio) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, servicio.getDescripcion());
			statement.setDouble(2, servicio.getPrecio());
			statement.setTime(3, servicio.getTiempoEstimado());
			statement.setBoolean(4, servicio.isEstado());
			if (statement.executeUpdate() > 0) // Si se ejecutï¿½ devuelvo true
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public List<Servicio> read(String texto_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Servicio> servicios = new ArrayList<Servicio>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(read);
			statement.setString(1, texto_a_buscar);
			statement.setString(2, texto_a_buscar);
			statement.setString(3, texto_a_buscar);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				servicios.add(new Servicio(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getBoolean("estado"),
						resultSet.getTime("tiempo_estimado")));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return servicios;

	}

	public List<Servicio> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Servicio> servicios = new ArrayList<Servicio>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				servicios.add(new Servicio(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getBoolean("estado"),
						resultSet.getTime("tiempo_estimado")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return servicios;
	}

	@Override
	public Servicio readForId(int servicio) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, servicio);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				return new Servicio(resultSet.getLong("id"), resultSet.getString("descripcion"),
						resultSet.getDouble("precio"), resultSet.getBoolean("estado"),
						resultSet.getTime("tiempo_estimado"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				return resultSet.getInt("id");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean update(Servicio servicio) {
		// TODO Auto-generated method stub
		return false;
	}

}