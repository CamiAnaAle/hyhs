package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.modelo.clases.MedioDePago;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.MedioDePagoDAO;

public class MedioDePagoDAOSQL implements MedioDePagoDAO {

	private static final String readForId = "SELECT * FROM mediopago WHERE id = ? ";

	public static MedioDePago readForId(int idMedioDePago) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idMedioDePago);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				return new MedioDePago(resultSet.getInt("id"), resultSet.getString("descripcion"), resultSet.getBoolean("estado"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
