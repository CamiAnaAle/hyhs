package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.TipoCliente;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoClienteDAO;

public class TipoClienteDAOSQL implements TipoClienteDAO {

	private static final String insert = "INSERT INTO tipoCliente(id, descripcion, estado) VALUES(?, ?, ?)";

	private static final String edit = "UPDATE tipoCliente SET descripcion = ? WHERE id = ?";
	private static final String delete = "UPDATE tipoCliente SET estado = ? WHERE id = ?";
	
	private static final String readId = "SELECT * FROM tipoCliente WHERE id = ? and estado = '1'";
	private static final String readall = "SELECT * FROM tipoCliente where estado=?";

	public static TipoCliente getById(int idTipo) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		TipoCliente tipo = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readId);
			statement.setString(1, Integer.toString(idTipo));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipo = new TipoCliente(idTipo, resultSet.getString("descripcion"), resultSet.getBoolean("estado"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tipo;
	}

	public boolean insert(TipoCliente unTipo) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, unTipo.getId());
			statement.setString(2, unTipo.getDescripcion());
			statement.setBoolean(3, unTipo.isEstado());
			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}

	public boolean delete(TipoCliente tipo_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setBoolean(1, false);
			statement.setInt(2, tipo_a_eliminar.getId());
			if (statement.executeUpdate() > 0) {
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public List<TipoCliente> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<TipoCliente> tiposDeClientes = new ArrayList<TipoCliente>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setBoolean(1,true);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tiposDeClientes.add(getTiposDeClientes(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tiposDeClientes;
	}

	private TipoCliente getTiposDeClientes(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("descripcion");
		boolean estado = resultSet.getBoolean("estado");
		return new TipoCliente(id, nombre, estado);
	}

	public boolean edit(TipoCliente unTipo, String nombreNuevo) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditExitoso = false;

		try {
			statement = conexion.prepareStatement(edit);
			statement.setString(1, nombreNuevo);
			statement.setString(2, Integer.toString(unTipo.getId()));
			if (statement.executeUpdate() > 0) {
				isEditExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isEditExitoso;
	}

}
