package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Localidad;
import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ProvinciaDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO{

	private static final String insert = "INSERT INTO provincia ( `descripcion`,`id_pais`)   VALUES(?,?)";
	
	
	private static final String readall = "SELECT * FROM provincia";
	private static final String traerPorNombre = "SELECT id FROM provincia Where descripcion = ?";
	private static final String readForId = "SELECT * FROM provincia WHERE id = ? ";
	private static final String readForPais = "SELECT * FROM provincia WHERE id_pais = ? ";

	
	public boolean insert(Provincia provincia) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, provincia.getDescripcion());
			statement.setLong(2, provincia.getPais().getId());
			if(statement.executeUpdate() > 0) //Si se ejecutï¿½ devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	public List<Provincia> readAllProvinciasDeUnPais(Pais p)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Provincia> provincias = new ArrayList<Provincia>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForPais);
			statement.setInt(1,(int) p.getId());
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				provincias.add( new Provincia(resultSet.getLong("id"),resultSet.getString("descripcion"),p));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	
	public List<Provincia> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Provincia> provincias = new ArrayList<Provincia>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				Pais pais = new DAOSQLFactory().createPaisDAO().readForId(resultSet.getInt("id_pais"));
	 			
				provincias.add( new Provincia(resultSet.getLong("id"),resultSet.getString("descripcion"),pais));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	public Provincia readForId(int idprovincia) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idprovincia);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				Pais pais = new DAOSQLFactory().createPaisDAO().readForId(resultSet.getInt("id_pais"));
	 			
				
				return new Provincia(resultSet.getLong("id"),resultSet.getString("descripcion"),pais);
				}	
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("id");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;
	}

}
