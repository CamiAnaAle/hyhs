package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.modelo.clases.TipoPromocion;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoPromocionDAO;

public class TipoPromocionDAOSQL implements TipoPromocionDAO {

	private static final String insert = "INSERT INTO tipoPromocion(descripcion, estado) VALUES(?, ?)";

	private static final String readId = "SELECT * FROM tipoPromocion where id=?";

	private static final String readall = "SELECT * FROM tipoPromocion WHERE estado = 'true'";

	
	public  TipoPromocion readForId(int idtipoPromocion) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		TipoPromocion tipopromocion = null;
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readId);
			statement.setString(1, Integer.toString(idtipoPromocion));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipopromocion = new TipoPromocion(idtipoPromocion, resultSet.getString("descripcion"), resultSet.getBoolean("estado"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tipopromocion;
	}

	public boolean insert(TipoPromocion tipopromocion) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setLong(1, tipopromocion.getId());
			statement.setString(2, tipopromocion.getDescripcion());
			statement.setBoolean(3, tipopromocion.isEstado());
			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}


	public List<TipoPromocion> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<TipoPromocion> tipospromociones = new ArrayList<TipoPromocion>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				tipospromociones.add(getTiposdePromociones(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tipospromociones;
	}

	private TipoPromocion getTiposdePromociones(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("descripcion");
		boolean estado = resultSet.getBoolean("estado");
		return new TipoPromocion(id, nombre, estado);
	}



}
