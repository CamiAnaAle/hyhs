
package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.modelo.clases.Cliente;
import com.modelo.clases.Empleado;
import com.modelo.clases.Localidad;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EmpleadoDAO;
import persistencia.dao.interfaz.LocalidadDAO;

public class EmpleadoDAOSQL implements EmpleadoDAO {

	private static final String insert = "INSERT `empleado` (`legajo`,`dni`, `nombre`,  `apellido`,  `fecha_nacimiento`,  `direccion` ," + 
			"  `telefono`,   `celular`,   `email`,    `fecha_ingreso`,    `sueldo_inicial`,   `estado` ," + 
			"    `id_localidad`)" + 
			" VALUES(?,?,?, ?, ?, ?,?,?, ?, ?, ?, ?,?)";
	
	private static final String readall = "SELECT * FROM empleado";

	private static final String readByFecha = "SELECT * FROM empleado where fecha_nacimiento like '%?%' or fecha_ingreso  like '%?%'  ";
	
	private static final String readByEmpleadosSucursalDiaServicios=" select b.legajo As legajo,b.dni As dni, b.nombre As nombre, b.apellido As apellido,\r\n" + 
			"		b.fecha_nacimiento As fecha_nacimiento, b.direccion As direccion, b.telefono As telefono,\r\n" + 
			"        b.celular As celular, b.email As email, b.fecha_ingreso As fecha_ingreso,\r\n" + 
			"        b.sueldo_inicial As sueldo_inicial, b.id_localidad As id_localidad, b.estado As estado\r\n" + 
			" from asistencia a inner join empleado b on a.legajo=b.legajo where a.fecha=? and a.id_sucursal=? and a.estado=? and a.idEstado=? \r\n" + 
			" and(select count(servicio_empleado.legajo) As total from servicio_empleado where servicio_empleado.legajo=a.legajo and servicio_empleado.id_servicio=?)>0\r\n" + 
			" and(select count(rol_empleado.legajo) As total from rol_empleado where rol_empleado.legajo=a.legajo and rol_empleado.id_rol=?)>0";

	
	private static final String read = "SELECT * FROM empleado where legajo like ? or dni like ? or nombre like ? " + 
			"or apellido  like ? or telefono like ?  or celular like ? or email like ? or direccion like ? or sueldo_inicial like ? ";
		

	private static final String update = "UPDATE empleado  set dni= ?, nombre=?, apellido=?, fecha_nacimiento=?, direccion=?, telefono=?,"
			+ " celular=?, email=?, fecha_ingreso=?, sueldo_inicial=?, estado=?, id_localidad=? "
			+ " where legajo=?";

	private static final String readOne = "SELECT * FROM empleado where legajo=?";

	private static final String readultimoid="SELECT count(1) from empleado";

	
	
	public List<Empleado> readByEmpleadosSucursalDiaServicios(Date fecha_a_buscar, int id_sucursal, int id_servicio) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByEmpleadosSucursalDiaServicios);
			statement.setDate(1,fecha_a_buscar);
			statement.setInt(2,id_sucursal);
			statement.setInt(3,1);
			statement.setInt(4,1);
			statement.setInt(5,id_servicio);
			statement.setInt(6,3);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				empleados.add(getEmpleado(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empleados;
	
	}
	
	public boolean insert(Empleado empleado) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {

			java.sql.Date sqlDateNacimiento = java.sql.Date.valueOf(empleado.getNacimiento());
			java.sql.Date sqlDateFechaDeIngreso = java.sql.Date.valueOf(empleado.getFechaIngreso());

			statement = conexion.prepareStatement(insert);
			statement.setLong(1, empleado.getLegajo()); 			
			statement.setLong(2, empleado.getDni()); 				
			statement.setString(3, empleado.getNombre());
			statement.setString(4, empleado.getApellido());
			statement.setDate(5, sqlDateNacimiento);
			statement.setString(6, empleado.getDireccion());
			statement.setString(7, empleado.getTelefono());
			statement.setString(8, empleado.getCelular());
			statement.setString(9, empleado.getEmail());
			statement.setDate(10, sqlDateFechaDeIngreso);
			statement.setDouble(11, empleado.getSueldo());
			statement.setBoolean(12, empleado.getEstado());	
			statement.setLong(13, empleado.getLocalidad().getId());

			if (statement.executeUpdate() > 0) {
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInsertExitoso;
	}


	
	
	public boolean update(Empleado empleado) {

		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			java.sql.Date sqlDateNacimiento = java.sql.Date.valueOf(empleado.getNacimiento());
			java.sql.Date sqlDateFechaIngreso = java.sql.Date.valueOf(empleado.getFechaIngreso());

			statement = conexion.prepareStatement(update);
	
			
			statement.setLong(1, empleado.getDni()); 				// HAY SETLONG
			statement.setString(2,empleado.getNombre());
			statement.setString(3,empleado.getApellido());
			statement.setDate(4, sqlDateNacimiento);
			statement.setString(5, empleado.getDireccion());
			statement.setString(6, empleado.getTelefono());
			statement.setString(7, empleado.getCelular());
			statement.setString(8, empleado.getEmail());
			statement.setDate(9, sqlDateFechaIngreso);
			statement.setDouble(10, empleado.getSueldo());
			statement.setBoolean(11, empleado.getEstado());			// HAY SETBOOLEAN
			statement.setLong(12, empleado.getLocalidad().getId());	// HAY SETLONG
			statement.setLong(13, empleado.getLegajo());	
			
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isUpdateExitoso;
	}

	public List<Empleado> read(String texto_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(read);
			statement.setString(1,"%"+texto_a_buscar+"%");
			statement.setString(2,"%"+texto_a_buscar+"%");
			statement.setString(3,"%"+texto_a_buscar+"%");
			statement.setString(4,"%"+texto_a_buscar+"%");
			statement.setString(5,"%"+texto_a_buscar+"%");
			statement.setString(6,"%"+texto_a_buscar+"%");
			statement.setString(7,"%"+texto_a_buscar+"%");
			statement.setString(8,"%"+texto_a_buscar+"%");
			statement.setString(9,"%"+texto_a_buscar+"%");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				empleados.add(getEmpleado(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empleados;
	
	}
	
	
	public List<Empleado> readByFecha(Date fecha_a_buscar) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readByFecha);
			statement.setDate(1,fecha_a_buscar);
			statement.setDate(2,fecha_a_buscar);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				empleados.add(getEmpleado(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empleados;
	
	}
	
	
	
	public List<Empleado> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				empleados.add(getEmpleado(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empleados;
	
	}


	private Empleado getEmpleado(ResultSet resultSet) throws SQLException {
		
		
		long legajo = resultSet.getLong("legajo");
		long dni = resultSet.getLong("dni");
		String nombre = resultSet.getString("nombre");
		String apellido = resultSet.getString("apellido");
		LocalDate nacimiento = resultSet.getDate("fecha_nacimiento").toLocalDate();
		String direccion = resultSet.getString("direccion");
		String telefono = resultSet.getString("telefono");
		String celular = resultSet.getString("celular");
		String email = resultSet.getString("email");
		LocalDate fechaIngreso = resultSet.getDate("fecha_ingreso").toLocalDate();
		double  sueldo = resultSet.getInt("sueldo_inicial");
		
		
		
		Localidad localidad = new DAOSQLFactory().createLocalidadDAO().readForId(resultSet.getInt("id_localidad"));
 						
		boolean estado = resultSet.getBoolean("estado");
		
		return new Empleado(legajo, dni, nombre, apellido, nacimiento,direccion,
				telefono, celular, email,  fechaIngreso, sueldo, estado,
					localidad);
	}

	@Override
	public Empleado readOne(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Empleado empleado = new Empleado();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			resultSet.next();
			empleado= getEmpleado(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return empleado;
	}

	@Override
	public int readUltimoId() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try {

			statement = conexion.getSQLConexion().prepareStatement(readultimoid);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean existe(int id) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		Empleado empleado = new Empleado();
		Conexion conexion = Conexion.getConexion();
		boolean estado=false;
		try {
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				estado=true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return estado;
	}

}