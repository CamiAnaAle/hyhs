package validaciones;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class validaEmail {
	
	
	static public boolean validar(String email){
	Pattern pattern = Pattern
            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");


    Matcher mather = pattern.matcher(email);

    if (mather.find() == true) {
        System.out.println("El email ingresado es válido.");
        return true;
    } else {
        System.out.println("El email ingresado es inválido.");
        return false;
       
    }
	}
}
