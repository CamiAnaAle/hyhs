package com.modelo.clases;

import java.time.LocalDate;

public class Empleado {

	private long legajo;
	private long dni;
	private String nombre;
	private String apellido;
	private LocalDate nacimiento;
	private String direccion;
	private String telefono;
	private String celular;
	private String email;
	private LocalDate fechaIngreso;
	private double sueldo;
	private boolean estado;
	private Localidad localidad;
	
	public Empleado()
	{
		
	}
	public Empleado(long legajo, long dni, String nombre, String apellido, LocalDate nacimiento, String direccion,
			String telefono, String celular, String email, LocalDate fechaIngreso, double sueldo, boolean estado,
			Localidad localidad) {
		super();
		this.legajo = legajo;
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.nacimiento = nacimiento;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.email = email;
		this.fechaIngreso = fechaIngreso;
		this.sueldo = sueldo;
		this.estado = estado;
		this.localidad = localidad;
	}
	public long getLegajo() {
		return legajo;
	}
	public void setLegajo(long legajo) {
		this.legajo = legajo;
	}
	public long getDni() {
		return dni;
	}
	public void setDni(long dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public LocalDate getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(LocalDate nacimiento) {
		this.nacimiento = nacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public double getSueldo() {
		return sueldo;
	}
	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
	public boolean getEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public Localidad getLocalidad() {
		return localidad;
	}
	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}
	@Override
	public String toString() {
		String texto="";
		if(legajo==0)
			texto=(nombre + " " + apellido).toUpperCase();
		else
			texto= (legajo + " " + nombre + "," + apellido).toUpperCase();
		return texto;
	}
	public Empleado(long legajo, String nombre,String apellido) {
		super();
		this.legajo = legajo;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	
}