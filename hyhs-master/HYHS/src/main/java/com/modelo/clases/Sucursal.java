package com.modelo.clases;

import java.sql.Time;

public class Sucursal {

	private long id;
	private String nombre;
	private String direccion;
	private String telefono;
	private String email;
//	private Time horaDeApertura;
	//private Time horaDeCierre;
	private Localidad localidad;
	private boolean estado;

	public Sucursal() {
	}

	public Sucursal(long id, String nombre, String direccion, String telefono, String email, Localidad localidad, boolean estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.email = email;
		this.localidad = localidad;
		this.estado = estado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return nombre.toUpperCase();
	}

}
