package com.modelo.clases;

public class Producto {

	private long id;
	private String nombre;
	private String descripcion;
	private int stock;
	private double precioDeCompra;
	private double precioDeVenta;
	private CategoriaProducto categoria;
	private boolean estado;

	public Producto(long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Producto() {

	}

	public Producto(long id, String nombre, String descripcion, int stock, double precioDeCompra, double precioDeVenta,
			CategoriaProducto categoria, boolean estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precioDeCompra = precioDeCompra;
		this.precioDeVenta = precioDeVenta;
		this.categoria = categoria;
		this.estado = estado;
	}

	public Producto(long id) {
		super();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecioDeCompra() {
		return precioDeCompra;
	}

	public void setPrecioDeCompra(double precioDeCompra) {
		this.precioDeCompra = precioDeCompra;
	}

	public double getPrecioDeVenta() {
		return precioDeVenta;
	}

	public void setPrecioDeVenta(double precioDeVenta) {
		this.precioDeVenta = precioDeVenta;
	}

	public CategoriaProducto getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaProducto categoria) {
		this.categoria = categoria;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}
