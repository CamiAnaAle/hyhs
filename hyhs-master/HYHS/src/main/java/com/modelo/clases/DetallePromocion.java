package com.modelo.clases;

public class DetallePromocion {
	
	private long id;
	private Servicio servicio;
	private Promocion promocion;
	private boolean estado;
	
	public DetallePromocion(long id, Servicio servicio, Promocion promocion, boolean estado) {
		super();
		this.id = id;
		this.servicio = servicio;
		this.promocion = promocion;
		this.estado = estado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Promocion getPromocion() {
		return promocion;
	}

	public void setPromocion(Promocion promocion) {
		this.promocion = promocion;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
