package com.modelo.clases;

import java.time.LocalDate;

public class HistorialPuntos {

	private long id;
	private LocalDate fecha;
	private int cantPuntosActual;
	private int cantPuntosTransaccion;
	private int cantPuntosTotal;
	private Turno turno;
	private Cliente cliente;
	
	public HistorialPuntos(long id, LocalDate fecha, int cantPuntosActual, int cantPuntosTransaccion,
			int cantPuntosTotal, Turno turno, Cliente cliente) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.cantPuntosActual = cantPuntosActual;
		this.cantPuntosTransaccion = cantPuntosTransaccion;
		this.cantPuntosTotal = cantPuntosTotal;
		this.turno = turno;
		this.cliente = cliente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public int getCantPuntosActual() {
		return cantPuntosActual;
	}

	public void setCantPuntosActual(int cantPuntosActual) {
		this.cantPuntosActual = cantPuntosActual;
	}

	public int getCantPuntosTransaccion() {
		return cantPuntosTransaccion;
	}

	public void setCantPuntosTransaccion(int cantPuntosTransaccion) {
		this.cantPuntosTransaccion = cantPuntosTransaccion;
	}

	public int getCantPuntosTotal() {
		return cantPuntosTotal;
	}

	public void setCantPuntosTotal(int cantPuntosTotal) {
		this.cantPuntosTotal = cantPuntosTotal;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
