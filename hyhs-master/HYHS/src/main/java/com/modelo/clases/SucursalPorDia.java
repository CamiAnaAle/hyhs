package com.modelo.clases;

import java.time.LocalTime;

public class SucursalPorDia {

	private Sucursal sucursal;
	private Dia dia;
	private LocalTime horaInicio;
	private LocalTime horaFin;
	private boolean estado;
	
	public SucursalPorDia(Sucursal sucursal, Dia dia, LocalTime horaInicio, LocalTime horaFin, boolean estado) {
		super();
		this.sucursal = sucursal;
		this.dia = dia;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.estado = estado;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Dia getDia() {
		return dia;
	}

	public void setDia(Dia dia) {
		this.dia = dia;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
