package com.modelo.clases;

import java.time.LocalDate;

public class Egreso {
	
	private long id;
	private Empleado empleado;
	private LocalDate fecha;
	private double importe;
	private boolean estado;
	private Sucursal sucursal;
	
	public Egreso(long id, Empleado empleado, LocalDate fecha, double importe, boolean estado, Sucursal sucursal) {
		super();
		this.id = id;
		this.empleado = empleado;
		this.fecha = fecha;
		this.importe = importe;
		this.estado = estado;
		this.sucursal = sucursal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
}
