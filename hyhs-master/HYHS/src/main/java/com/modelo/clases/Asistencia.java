package com.modelo.clases;

import java.time.LocalDate;
import java.time.LocalTime;

public class Asistencia {

	private long id;
	private LocalDate fecha;
	private LocalTime horaDesde;
	private LocalTime horaHasta;
	private EstadoAsistencia estadoAsist;
	private DiaLaboral diaLaboral;
	private boolean estado;
	
	public Asistencia()
	{
		
	}
	
	
	
	public Asistencia(long id) {
		super();
		this.id = id;
	}



	public Asistencia(long id, LocalDate fecha, LocalTime horaDesde, LocalTime horaHasta, boolean estado) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.horaDesde = horaDesde;
		this.horaHasta = horaHasta;
		this.estado = estado;
	}

	public Asistencia(long id, LocalDate fecha, LocalTime horaDesde, LocalTime horaHasta, EstadoAsistencia estadoAsist,
			DiaLaboral diaLaboral, boolean estado) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.horaDesde = horaDesde;
		this.horaHasta = horaHasta;
		this.estadoAsist = estadoAsist;
		this.diaLaboral = diaLaboral;
		this.estado = estado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalTime getHoraDesde() {
		return horaDesde;
	}

	public void setHoraDesde(LocalTime horaDesde) {
		this.horaDesde = horaDesde;
	}

	public LocalTime getHoraHasta() {
		return horaHasta;
	}

	public void setHoraHasta(LocalTime horaHasta) {
		this.horaHasta = horaHasta;
	}

	public EstadoAsistencia getEstadoAsist() {
		return estadoAsist;
	}

	public void setEstadoAsist(EstadoAsistencia estadoAsist) {
		this.estadoAsist = estadoAsist;
	}

	public DiaLaboral getDiaLaboral() {
		return diaLaboral;
	}

	public void setDiaLaboral(DiaLaboral diaLaboral) {
		this.diaLaboral = diaLaboral;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Asistencia [id=" + id + ", fecha=" + fecha + ", horaDesde=" + horaDesde + ", horaHasta=" + horaHasta
				+ ", estadoAsist=" + estadoAsist + ", diaLaboral=" + diaLaboral + ", estado=" + estado + "]";
	}
	
	
}
