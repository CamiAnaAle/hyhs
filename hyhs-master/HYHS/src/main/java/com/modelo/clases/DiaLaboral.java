package com.modelo.clases;

import java.time.LocalTime;

public class DiaLaboral {

	private Empleado legajo;
	private SucursalPorDia sucPorDia;
	private LocalTime horaDesde;
	private LocalTime horaHasta;
	private boolean estado;
	
	public DiaLaboral(Empleado legajo, SucursalPorDia sucPorDia, LocalTime horaDesde, LocalTime horaHasta,
			boolean estado) {
		super();
		this.legajo = legajo;
		this.sucPorDia = sucPorDia;
		this.horaDesde = horaDesde;
		this.horaHasta = horaHasta;
		this.estado = estado;
	}

	public Empleado getLegajo() {
		return legajo;
	}

	public void setLegajo(Empleado legajo) {
		this.legajo = legajo;
	}

	public SucursalPorDia getSucPorDia() {
		return sucPorDia;
	}

	public void setSucPorDia(SucursalPorDia sucPorDia) {
		this.sucPorDia = sucPorDia;
	}

	public LocalTime getHoraDesde() {
		return horaDesde;
	}

	public void setHoraDesde(LocalTime horaDesde) {
		this.horaDesde = horaDesde;
	}

	public LocalTime getHoraHasta() {
		return horaHasta;
	}

	public void setHoraHasta(LocalTime horaHasta) {
		this.horaHasta = horaHasta;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
