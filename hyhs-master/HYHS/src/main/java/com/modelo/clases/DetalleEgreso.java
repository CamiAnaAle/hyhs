package com.modelo.clases;

public class DetalleEgreso {

	private long id;
	private String descripcion;
	private double precio;
	private boolean estado;
	private Egreso egreso;
	private TipoDeEgreso tipo;	
	
	public DetalleEgreso(long id, String descripcion, double precio, boolean estado, Egreso egreso, TipoDeEgreso tipo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.precio = precio;
		this.estado = estado;
		this.egreso = egreso;
		this.tipo = tipo;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public Egreso getEgreso() {
		return egreso;
	}
	public void setEgreso(Egreso egreso) {
		this.egreso = egreso;
	}
	public TipoDeEgreso getTipo() {
		return tipo;
	}
	public void setTipo(TipoDeEgreso tipo) {
		this.tipo = tipo;
	}
	
	
}
