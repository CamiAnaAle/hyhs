package com.modelo.clases;

import java.time.LocalDate;

public class RolPorEmpleado {

	private Rol idRol;
	private Empleado legajoEmpleado;
	private LocalDate fechaHasta;
	private boolean estado;
	
	public RolPorEmpleado(Rol idRol, Empleado idEmpleado, LocalDate fechaHasta, boolean estado) {
		super();
		this.idRol = idRol;
		this.legajoEmpleado = idEmpleado;
		this.fechaHasta = fechaHasta;
		this.estado = estado;
	}

	public Rol getIdRol() {
		return idRol;
	}

	public void setIdRol(Rol idRol) {
		this.idRol = idRol;
	}

	public Empleado getIdEmpleado() {
		return legajoEmpleado;
	}

	public void setIdEmpleado(Empleado idEmpleado) {
		this.legajoEmpleado = idEmpleado;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
