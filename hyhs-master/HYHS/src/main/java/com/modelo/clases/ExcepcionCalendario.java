package com.modelo.clases;

import java.time.LocalDate;

public class ExcepcionCalendario {

	private long id;
	private LocalDate fecha;
	private Sucursal sucursal;
	private boolean estado;
	
	public ExcepcionCalendario(long id, LocalDate fecha, Sucursal sucursal, boolean estado) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.sucursal = sucursal;
		this.estado = estado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
