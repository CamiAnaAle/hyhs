package com.modelo.clases;

import java.time.LocalDate;

public class Cliente {

	private long idCliente;
	private long dni;
	private String nombre;
	private String apellido;
	private LocalDate nacimiento;
	private String direccion;
	private String telefono;
	private String celular;
	private String email;
	private LocalDate actualizacionDeuda;
	private int totalAdeudado;
	private Localidad localidad;
	private TipoCliente tipoDeCliente;
	private int cantTurnosSolicitados;
	private int cantTurnosPresente;
	private int cantTurnosAusente;
	private int cantTurnosCanceladoCliente;
	private int cantTurnosCanceladoEmpresa;
	private int puntos;
	private boolean estado;
	
	
	
	public Cliente()
	{
		
	}
	
	public Cliente(String nombre, String apellido, String telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
	}

	
	

	public Cliente(long idCliente, long dNI, String nombre, String apellido, LocalDate nacimiento, String direccion,
			String telefono, String celular, String email, LocalDate actualizacionDeuda, int totalAdeudado,
			Localidad localidad, TipoCliente tipoDeCliente, int cantTurnosSolicitados, int cantTurnosPresente,
			int cantTurnosAusente, int cantTurnosCanceladoCliente,int cantTurnosCanceladoEmpresa, int puntos, boolean estado) {
		super();
		this.idCliente = idCliente;
		this.dni = dNI;
		this.nombre = nombre;
		this.apellido = apellido;
		this.nacimiento = nacimiento;
		this.direccion = direccion;
		this.telefono = telefono;
		this.celular = celular;
		this.email = email;
		this.actualizacionDeuda = actualizacionDeuda;
		this.totalAdeudado = totalAdeudado;
		this.localidad = localidad;
		this.tipoDeCliente = tipoDeCliente;
		this.cantTurnosSolicitados = cantTurnosSolicitados;
		this.cantTurnosPresente = cantTurnosPresente;
		this.cantTurnosAusente = cantTurnosAusente;
		this.cantTurnosCanceladoCliente = cantTurnosCanceladoCliente;
		this.cantTurnosCanceladoEmpresa = cantTurnosCanceladoEmpresa;
		this.puntos = puntos;
		this.estado = estado;
	}


	public long getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}


	public long getDni() {
		return dni;
	}


	public void setDni(long dni) {
		this.dni = dni;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public LocalDate getNacimiento() {
		return nacimiento;
	}


	public void setNacimiento(LocalDate nacimiento) {
		this.nacimiento = nacimiento;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public LocalDate getActualizacionDeuda() {
		return actualizacionDeuda;
	}


	public void setActualizacionDeuda(LocalDate actualizacionDeuda) {
		this.actualizacionDeuda = actualizacionDeuda;
	}


	public int getTotalAdeudado() {
		return totalAdeudado;
	}


	public void setTotalAdeudado(int totalAdeudado) {
		this.totalAdeudado = totalAdeudado;
	}


	public Localidad getLocalidad() {
		return localidad;
	}


	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}


	public TipoCliente getTipoDeCliente() {
		return tipoDeCliente;
	}


	public void setTipoDeCliente(TipoCliente tipoDeCliente) {
		this.tipoDeCliente = tipoDeCliente;
	}


	public int getCantTurnosSolicitados() {
		return cantTurnosSolicitados;
	}


	public void setCantTurnosSolicitados(int cantTurnosSolicitados) {
		this.cantTurnosSolicitados = cantTurnosSolicitados;
	}


	public int getCantTurnosPresente() {
		return cantTurnosPresente;
	}


	public void setCantTurnosPresente(int cantTurnosPresente) {
		this.cantTurnosPresente = cantTurnosPresente;
	}


	public int getCantTurnosAusente() {
		return cantTurnosAusente;
	}


	public void setCantTurnosAusente(int cantTurnosAusente) {
		this.cantTurnosAusente = cantTurnosAusente;
	}



	public int getPuntos() {
		return puntos;
	}


	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}


	public boolean getEstado() {
		return estado;
	}


	public void setEstado(boolean estado) {
		this.estado = estado;
	}


	public int getCantTurnosCanceladoCliente() {
		return cantTurnosCanceladoCliente;
	}


	public void setCantTurnosCanceladoCliente(int cantTurnosCanceladoCliente) {
		this.cantTurnosCanceladoCliente = cantTurnosCanceladoCliente;
	}


	public int getCantTurnosCanceladoEmpresa() {
		return cantTurnosCanceladoEmpresa;
	}


	public void setCantTurnosCanceladoEmpresa(int cantTurnosCanceladoEmpresa) {
		this.cantTurnosCanceladoEmpresa = cantTurnosCanceladoEmpresa;
	}

	@Override
	public String toString() {
		return "Cliente [idCliente=" + idCliente + ", dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", nacimiento=" + nacimiento + ", direccion=" + direccion + ", telefono=" + telefono + ", celular="
				+ celular + ", email=" + email + ", actualizacionDeuda=" + actualizacionDeuda + ", totalAdeudado="
				+ totalAdeudado + ", localidad=" + localidad + ", tipoDeCliente=" + tipoDeCliente
				+ ", cantTurnosSolicitados=" + cantTurnosSolicitados + ", cantTurnosPresente=" + cantTurnosPresente
				+ ", cantTurnosAusente=" + cantTurnosAusente + ", cantTurnosCanceladoCliente="
				+ cantTurnosCanceladoCliente + ", cantTurnosCanceladoEmpresa=" + cantTurnosCanceladoEmpresa
				+ ", puntos=" + puntos + ", estado=" + estado + "]";
	}	
}
