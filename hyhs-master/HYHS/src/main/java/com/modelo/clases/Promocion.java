package com.modelo.clases;

import java.time.LocalDate;

public class Promocion {
	
	private long id;
	private String descripcion;
	private double precio;
	private int puntos;
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;
	private EstadoPromocion estado;
	private TipoPromocion tipo;
	
	public Promocion()
	{
		
	}
	
	public Promocion(long id, String descripcion, double precio, int puntos, LocalDate fechaDesde, LocalDate fechaHasta,
			EstadoPromocion estado, TipoPromocion tipo) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.precio = precio;
		this.puntos = puntos;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.estado = estado;
		this.tipo = tipo;
	}
	
	

	public Promocion(long id) {
		super();
		this.id = id;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public EstadoPromocion getEstado() {
		return estado;
	}

	public void setEstado(EstadoPromocion estado) {
		this.estado = estado;
	}

	public TipoPromocion getTipo() {
		return tipo;
	}

	public void setTipo(TipoPromocion tipo) {
		this.tipo = tipo;
	}
}
