package com.modelo.clases;

public class User {

	private String username;
	private String password;
	private boolean estado;
	private long legajo;
	private Rol rol;
	private Idioma idioma;
	
	public User(String username, String password, boolean estado, long legajo, Rol rol, Idioma idioma) {
		super();
		this.username = username;
		this.password = password;
		this.estado = estado;
		this.legajo = legajo;
		this.rol = rol;
		this.idioma = idioma;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public long getLegajo() {
		return legajo;
	}

	public void setLegajo(long legajo) {
		this.legajo = legajo;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}	
}
