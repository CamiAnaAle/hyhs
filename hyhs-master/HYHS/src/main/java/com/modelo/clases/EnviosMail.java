package com.modelo.clases;

import java.time.LocalDate;

public class EnviosMail {

	private long id;
	private String descripcion;
	private LocalDate fecha;
	private Cliente cliente;
	private int tipoMail;
	private boolean estado;
	
	public EnviosMail(long id, String descripcion, LocalDate fecha, Cliente cliente, int tipoMail, boolean estado) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.cliente = cliente;
		this.tipoMail = tipoMail;
		this.estado = estado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getTipoMail() {
		return tipoMail;
	}

	public void setTipoMail(int tipoMail) {
		this.tipoMail = tipoMail;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
