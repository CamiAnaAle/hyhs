package com.modelo.clases;

import java.sql.Time;

public class Servicio {

	private long id;
	private String descripcion;
	private double precio;
	private boolean estado;
	private Time tiempoEstimado;
	
	public Servicio()
	{
		
	}
	
	public Servicio(long id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public Servicio(long id) {
		super();
		this.id = id;
	}

	public Servicio(long id, String descripcion, double precio, boolean estado, Time tiempoEstimado) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.precio = precio;
		this.estado = estado;
		this.tiempoEstimado = tiempoEstimado;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Time getTiempoEstimado() {
		return tiempoEstimado;
	}

	public void setTiempoEstimado(Time tiempoEstimado) {
		this.tiempoEstimado = tiempoEstimado;
	}

	@Override
	public String toString() {
		return descripcion.toUpperCase();
	}
	
	
}
