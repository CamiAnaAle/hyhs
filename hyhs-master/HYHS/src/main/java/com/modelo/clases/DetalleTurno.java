package com.modelo.clases;

import java.time.LocalTime;

public class DetalleTurno {

	private long id;
	private LocalTime horaInicio;
	private LocalTime horaFin;
	private LocalTime tiempoEstimado;
	private LocalTime tiempoReal;
	private boolean estado;
	private Asistencia asistencia;
	private Turno turno;
	private Servicio servicio;
	private Promocion promocion;
	private Producto producto;
	
	public DetalleTurno(long id, LocalTime horaInicio, LocalTime horaFin, LocalTime tiempoEstimado,
			LocalTime tiempoReal, boolean estado, Asistencia asistencia, Turno turno, Servicio servicio,
			Promocion promocion, Producto producto) {
		super();
		this.id = id;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.tiempoEstimado = tiempoEstimado;
		this.tiempoReal = tiempoReal;
		this.estado = estado;
		this.asistencia = asistencia;
		this.turno = turno;
		this.servicio = servicio;
		this.promocion = promocion;
		this.producto = producto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public LocalTime getTiempoEstimado() {
		return tiempoEstimado;
	}

	public void setTiempoEstimado(LocalTime tiempoEstimado) {
		this.tiempoEstimado = tiempoEstimado;
	}

	public LocalTime getTiempoReal() {
		return tiempoReal;
	}

	public void setTiempoReal(LocalTime tiempoReal) {
		this.tiempoReal = tiempoReal;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Asistencia getAsistencia() {
		return asistencia;
	}

	public void setAsistencia(Asistencia asistencia) {
		this.asistencia = asistencia;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Promocion getPromocion() {
		return promocion;
	}

	public void setPromocion(Promocion promocion) {
		this.promocion = promocion;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "DetalleTurno [id=" + id + ", horaInicio=" + horaInicio + ", horaFin=" + horaFin + ", tiempoEstimado="
				+ tiempoEstimado + ", tiempoReal=" + tiempoReal + ", estado=" + estado + ", asistencia=" + asistencia
				+ ", turno=" + turno + ", servicio=" + servicio + ", promocion=" + promocion + ", producto=" + producto
				+ "]";
	}
	
	
}
