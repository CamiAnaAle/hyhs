package com.modelo.clases;

import java.sql.Time;
import java.time.LocalDate;

public class Turno {

	private long id;
	private LocalDate fecha;
	private Time tiempoTotal;
	private double precioTotal;
	private boolean pagado;
	private MedioDePago medio;
	private Cliente cliente;
	private Sucursal sucursal;
	private Empleado empleado;
	private boolean estado;
	private double plataReal;
	private double plataCanjeada;
	private int puntosCanjeados;
	private int puntosGanados;
	
	
	public Turno()
	{
		
	}
	public Turno(long id, LocalDate fecha, Time tiempoTotal, double precioTotal, boolean pagado, MedioDePago medio,
			Cliente cliente, Sucursal sucursal, Empleado empleado, boolean estado, double plataReal,
			double plataCanjeada, int puntosCanjeados, int puntosGanados) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.tiempoTotal = tiempoTotal;
		this.precioTotal = precioTotal;
		this.pagado = pagado;
		this.medio = medio;
		this.cliente = cliente;
		this.sucursal = sucursal;
		this.empleado = empleado;
		this.estado = estado;
		this.plataReal = plataReal;
		this.plataCanjeada = plataCanjeada;
		this.puntosCanjeados = puntosCanjeados;
		this.puntosGanados = puntosGanados;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Time getTiempoTotal() {
		return tiempoTotal;
	}

	public void setTiempoTotal(Time tiempoTotal) {
		this.tiempoTotal = tiempoTotal;
	}

	public double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}

	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public MedioDePago getMedio() {
		return medio;
	}

	public void setMedio(MedioDePago medio) {
		this.medio = medio;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public double getPlataReal() {
		return plataReal;
	}

	public void setPlataReal(double plataReal) {
		this.plataReal = plataReal;
	}

	public double getPlataCanjeada() {
		return plataCanjeada;
	}

	public void setPlataCanjeada(double plataCanjeada) {
		this.plataCanjeada = plataCanjeada;
	}

	public int getPuntosCanjeados() {
		return puntosCanjeados;
	}

	public void setPuntosCanjeados(int puntosCanjeados) {
		this.puntosCanjeados = puntosCanjeados;
	}

	public int getPuntosGanados() {
		return puntosGanados;
	}

	public void setPuntosGanados(int puntosGanados) {
		this.puntosGanados = puntosGanados;
	}

	public Turno(long id) {
		super();
		this.id = id;
	}
	
	
}
