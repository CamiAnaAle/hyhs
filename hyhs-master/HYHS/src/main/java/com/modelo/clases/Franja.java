package com.modelo.clases;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Franja {
	
	static final int MINUTES_PER_HOUR = 60;
    static final int SECONDS_PER_MINUTE = 60;
    static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;
    
	private LocalTime horaDesde;
	private LocalTime horaHasta;
	private long[] tiempoTotal;
		
	 public Franja(LocalTime horaDesde, LocalTime horaHasta, long[] tiempoTotal) {
		super();
		this.horaDesde = horaDesde;
		this.horaHasta = horaHasta;
		this.tiempoTotal = tiempoTotal;
	}

	public long[] getTiempoTotal() {
		return tiempoTotal;
	}

	public void setTiempoTotal(long[] tiempoTotal) {
		this.tiempoTotal = tiempoTotal;
	}
	
	public LocalTime getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(LocalTime horaDesde) {
		this.horaDesde = horaDesde;
	}
	public LocalTime getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(LocalTime horaHasta) {
		this.horaHasta = horaHasta;
	}
	public static long[] getTime(LocalTime dob, LocalTime now) {
		
        Duration duration = Duration.between(dob, now);
        long seconds = duration.getSeconds();

        long hours = seconds / SECONDS_PER_HOUR;
        long minutes = ((seconds % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE);
        long secs = (seconds % SECONDS_PER_MINUTE);

        return new long[]{hours, minutes, secs};
	}

	@Override
	public String toString() {
		return horaDesde + " hasta " + horaHasta + " ["+tiempoTotal[0]+"hs"+tiempoTotal[1]+"min]";
	}
	
public static List<Franja> calculoFranjaHoraria(Asistencia a, List<DetalleTurno> listadt) {
		
		List<Franja> listaFranja = new ArrayList<Franja>();
		
		LocalTime inicio;
		LocalTime fin;
		long time[];
		
		if(listadt.size()==0)
		{ 
			//El empleado no tiene turnos
			time=Franja.getTime(a.getHoraDesde(), a.getHoraHasta());
			listaFranja.add(new Franja(a.getHoraDesde(),a.getHoraHasta(), time) );
		}
		else
		{
			//Hora en la que empieza a trabajar el empleado
			inicio = a.getHoraDesde();
			
			for (DetalleTurno d : listadt) {
				
				fin = d.getHoraInicio();
				time=Franja.getTime(inicio, fin);

				//Coincidencia de horarios
				if(time[0]==0 && time[1]==0)
				{
					inicio = d.getHoraFin();
				}
				else
				{
					listaFranja.add(new Franja(inicio,fin, time) );
					inicio = d.getHoraFin();
				}
			}
			fin = a.getHoraHasta();
			time=Franja.getTime(inicio, fin);
			listaFranja.add(new Franja(inicio,fin, time) );
			
		}
		return listaFranja;
	}
	
	
}
