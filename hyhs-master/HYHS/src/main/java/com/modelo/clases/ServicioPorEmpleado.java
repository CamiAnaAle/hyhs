package com.modelo.clases;

public class ServicioPorEmpleado {

	private Empleado empleado;
	private Servicio servicio;
	private boolean estado;
	
	public ServicioPorEmpleado(Empleado empleado, Servicio servicio, boolean estado) {
		super();
		this.empleado = empleado;
		this.servicio = servicio;
		this.estado = estado;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
}
