package com.presentacion.vistas.panelesBusqueda;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.modelo.clases.Cliente;
import com.presentacion.vistas.panelesCliente.PanelBuscarCliente;

import presentacion.controlador.ControladorTurno;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BusquedaClientes extends JFrame {

	private JPanel contentPane;
	private JTextField txtBusquedaCliente;
	private String[] nombreColumnas = { "ID", "DNI", "Nombre", "Apellido", "Telefono", "Mail", "Direccion" };
	private DefaultTableModel modelClientes;
	private JTable tablaClientes;

	public JTable getTablaClientes() {
		return tablaClientes;
	}

	public void setTablaClientes(JTable tablaClientes) {
		this.tablaClientes = tablaClientes;
	}

	public JTextField getTxtBusquedaCliente() {
		return txtBusquedaCliente;
	}

	public void setTxtBusquedaCliente(JTextField txtBusquedaCliente) {
		this.txtBusquedaCliente = txtBusquedaCliente;
	}

	public DefaultTableModel getModelClientes() {
		return modelClientes;
	}

	public void setModelClientes(DefaultTableModel modelClientes) {
		this.modelClientes = modelClientes;
	}

	public BusquedaClientes() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 901, 531);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel();
		label.setText("LISTADO");
		label.setForeground(SystemColor.windowBorder);
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setBounds(15, 29, 101, 22);
		contentPane.add(label);

		JLabel label_1 = new JLabel("INGRESE TEXTO:");
		label_1.setBounds(114, 31, 162, 20);
		contentPane.add(label_1);

		txtBusquedaCliente = new JTextField();
		txtBusquedaCliente.setColumns(10);
		txtBusquedaCliente.setBounds(251, 28, 214, 26);
		contentPane.add(txtBusquedaCliente);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 80, 832, 395);
		contentPane.add(scrollPane);

		modelClientes = new DefaultTableModel(null, nombreColumnas);
		tablaClientes = new JTable(modelClientes);

		tablaClientes.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaClientes.getColumnModel().getColumn(0).setResizable(false);
		tablaClientes.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaClientes.getColumnModel().getColumn(1).setResizable(false);

		scrollPane.setViewportView(tablaClientes);

		btnBusqueda = new JButton("Busqueda");
		btnBusqueda.setBounds(469, 27, 115, 29);
		contentPane.add(btnBusqueda);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(599, 27, 115, 29);
		contentPane.add(btnAceptar);
	}

	public JButton getBtnBusqueda() {
		return btnBusqueda;
	}

	public void setBtnBusqueda(JButton btnBusqueda) {
		this.btnBusqueda = btnBusqueda;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	private JButton btnBusqueda;
	private JButton btnAceptar;

	public void llenarTabla(List<Cliente> clientesRegistrados) {

		this.getModelClientes().setRowCount(0); // Para vaciar la tabla
		this.getModelClientes().setColumnCount(0);
		this.getModelClientes().setColumnIdentifiers(this.getNombreColumnas());

		for (Cliente c : clientesRegistrados) {
			String id = c.getIdCliente() + "";
			String nombre = c.getNombre();
			String apellido = c.getApellido();
			String dni = Long.toString(c.getDni());
			String tel = c.getTelefono();
			String mail = c.getEmail();
			// String deuda = Integer.toString(c.getTotalAdeudado());
			// String localidad = c.getLocalidad().getDescripcion();
			// String tipo = c.getTipoDeCliente().getDescripcion();
			String direccion = c.getDireccion();

			Object[] fila = { id, dni, nombre, apellido, tel, mail, direccion };
			this.getModelClientes().addRow(fila);
		}
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
}
