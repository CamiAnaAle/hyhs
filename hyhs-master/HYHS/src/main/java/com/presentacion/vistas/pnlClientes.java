/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.presentacion.vistas;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import com.modelo.clases.Cliente;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import javax.swing.JComboBox;


/**
 *
 * @author RojeruSan
 */
public class pnlClientes extends javax.swing.JPanel {

	
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"Nombre y apellido","Telefono"};
	private JTable table;
	
    public pnlClientes() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));
        JPanel panel = new JPanel();
       // panel.setForeground();
        panel.setBackground(Color.WHITE);
        panel.setBorder(new LineBorder(SystemColor.activeCaptionBorder));
        
        JPanel panel_1 = new JPanel();
        panel_1.setBorder(new LineBorder(SystemColor.activeCaptionBorder));
        panel_1.setBackground(Color.WHITE);
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.TRAILING, false)
        				.addComponent(panel_1, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
        				.addComponent(panel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE))
        			.addContainerGap(75, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addComponent(panel, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(53, Short.MAX_VALUE))
        );
        panel_1.setLayout(null);
        
        JLabel lblListado = new JLabel();
        lblListado.setBounds(15, 16, 101, 22);
        panel_1.add(lblListado);
        lblListado.setText("LISTADO");
        lblListado.setForeground(SystemColor.textHighlight);
        lblListado.setFont(new Font("Tahoma", Font.PLAIN, 18));
        
        JLabel lblDni_1 = new JLabel("INGRESE TEXTO:");
        lblDni_1.setBounds(204, 18, 162, 20);
        panel_1.add(lblDni_1);
        
        textField_10 = new JTextField();
        textField_10.setBounds(368, 15, 214, 26);
        panel_1.add(textField_10);
        textField_10.setColumns(10);
        
        JScrollPane spPersonas = new JScrollPane();
        spPersonas.setBounds(15, 67, 832, 136);
        panel_1.add(spPersonas);
        panel.setLayout(null);
		
        modelPersonas = new DefaultTableModel(null,nombreColumnas);
		table = new JTable(modelPersonas);
		
		table.getColumnModel().getColumn(0).setPreferredWidth(103);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(table);
		
		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.setBounds(618, 14, 115, 29);
		panel_1.add(btnBuscar);
		
        
		
        JLabel lblFNacimiento = new JLabel("F. NACIMIENTO");
        lblFNacimiento.setBounds(22, 160, 122, 20);
        panel.add(lblFNacimiento);
        
        textField_4 = new JTextField();
        textField_4.setBounds(148, 157, 146, 26);
        panel.add(textField_4);
        textField_4.setColumns(10);
        
        JLabel lblApellido = new JLabel("APELLIDO");
        lblApellido.setBounds(22, 124, 111, 20);
        panel.add(lblApellido);
        
        JLabel lblNombre = new JLabel("NOMBRE");
        lblNombre.setBounds(22, 88, 73, 20);
        panel.add(lblNombre);
        
        JLabel lblDni = new JLabel("DNI");
        lblDni.setBounds(22, 52, 73, 20);
        panel.add(lblDni);
        
        textField = new JTextField();
        textField.setBounds(148, 16, 146, 26);
        panel.add(textField);
        textField.setColumns(10);
        
        textField_1 = new JTextField();
        textField_1.setBounds(148, 49, 146, 26);
        panel.add(textField_1);
        textField_1.setColumns(10);
        
        textField_2 = new JTextField();
        textField_2.setBounds(148, 85, 146, 26);
        panel.add(textField_2);
        textField_2.setColumns(10);
        
        textField_3 = new JTextField();
        textField_3.setBounds(148, 121, 146, 26);
        panel.add(textField_3);
        textField_3.setColumns(10);
        
        JLabel lblCp = new JLabel("CP:");
        lblCp.setBounds(345, 19, 111, 20);
        panel.add(lblCp);
        
        JLabel lblDireccin = new JLabel("DIRECCION");
        lblDireccin.setBounds(345, 55, 90, 20);
        panel.add(lblDireccin);
        
        JLabel lblTelefono = new JLabel("TELEFONO");
        lblTelefono.setBounds(345, 91, 90, 20);
        panel.add(lblTelefono);
        
        JLabel lblCelular = new JLabel("CELULAR");
        lblCelular.setBounds(345, 124, 98, 20);
        panel.add(lblCelular);
        
        JLabel lblEmail = new JLabel("EMAIL");
        lblEmail.setBounds(345, 160, 65, 20);
        panel.add(lblEmail);
        
        textField_5 = new JTextField();
        textField_5.setBounds(450, 16, 146, 26);
        panel.add(textField_5);
        textField_5.setColumns(10);
        
        textField_6 = new JTextField();
        textField_6.setBounds(450, 49, 146, 26);
        panel.add(textField_6);
        textField_6.setColumns(10);
        
        textField_7 = new JTextField();
        textField_7.setBounds(450, 85, 146, 26);
        panel.add(textField_7);
        textField_7.setColumns(10);
        
        textField_8 = new JTextField();
        textField_8.setBounds(450, 124, 146, 26);
        panel.add(textField_8);
        textField_8.setColumns(10);
        
        textField_9 = new JTextField();
        textField_9.setBounds(450, 157, 146, 26);
        panel.add(textField_9);
        textField_9.setColumns(10);
        
        JLabel lblHola = new JLabel("ID CLIENTE:");
        lblHola.setBounds(22, 19, 111, 20);
        panel.add(lblHola);
        
        JButton btnNewButton = new JButton("NUEVO");
        btnNewButton.setBounds(630, 15, 133, 29);
        panel.add(btnNewButton);
        
        JButton button = new JButton("AGREGAR");
        button.setBounds(630, 55, 133, 29);
        panel.add(button);
        
        JButton btnModificar = new JButton("ACEPTAR");
        btnModificar.setBounds(630, 91, 133, 29);
        panel.add(btnModificar);
        
        JComboBox comboBox = new JComboBox();
        comboBox.setBounds(151, 196, 136, 26);
        panel.add(comboBox);
        
        JLabel label = new JLabel("TIPO CLIENTE");
        label.setBounds(15, 199, 122, 20);
        panel.add(label);
        this.setLayout(layout);
        
        
        List<Cliente> clientes = new ArrayList<Cliente>();
        clientes.add(new Cliente("PEPE","GONZALEZ", "6526753"));
        clientes.add(new Cliente("LUISA","CASAS", "74463226"));
        llenarTabla(clientes);
    }
    
    
    public void llenarTabla(List<Cliente> personasEnTabla) {
		this.modelPersonas.setRowCount(0); //Para vaciar la tabla
		this.modelPersonas.setColumnCount(0);
		this.modelPersonas.setColumnIdentifiers(nombreColumnas);

		for (Cliente p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			Object[] fila = {nombre, tel};
			this.modelPersonas.addRow(fila);
		}
		
	}
    
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;
    private JTextField textField_8;
    private JTextField textField_9;

    private JTextField textField_10;
}
