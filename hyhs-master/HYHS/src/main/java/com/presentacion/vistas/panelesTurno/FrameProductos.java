package com.presentacion.vistas.panelesTurno;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class FrameProductos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel pnlPrincipal;
	private String[] nombreColumnas = { "ID", "Nombre","Descripcion", "Stock", "Precio Compra", "Precio Venta", "Categoria"};
	private DefaultTableModel modelProductos;
	private JTable tablaProductos;
	private JButton btnAgregar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameProductos frame = new FrameProductos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameProductos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 400);
		setLocationRelativeTo(null);
		setTitle("Productos");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		pnlPrincipal = new JPanel();
		pnlPrincipal.setBounds(0, 0, 724, 361);
		contentPane.add(pnlPrincipal);
		pnlPrincipal.setLayout(null);
		
		JLabel lblListadoDeProductos = new JLabel("Listado de Productos");
		lblListadoDeProductos.setHorizontalAlignment(SwingConstants.CENTER);
		lblListadoDeProductos.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblListadoDeProductos.setBounds(10, 11, 219, 37);
		pnlPrincipal.add(lblListadoDeProductos);
		
		modelProductos = new DefaultTableModel(null, nombreColumnas);
		tablaProductos = new JTable(modelProductos);
		tablaProductos.setBounds(10, 131, 704, 170);
		pnlPrincipal.add(tablaProductos);

		JScrollPane scrollPromociones = new JScrollPane(tablaProductos,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPromociones.setBounds(10, 131, 704, 170);
		pnlPrincipal.add(scrollPromociones);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAgregar.setBounds(10, 327, 89, 23);
		pnlPrincipal.add(btnAgregar);
	}

	public JPanel getPnlPrincipal() {
		return pnlPrincipal;
	}

	public void setPnlPrincipal(JPanel pnlPrincipal) {
		this.pnlPrincipal = pnlPrincipal;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}

	public DefaultTableModel getModelProductos() {
		return modelProductos;
	}

	public void setModelProductos(DefaultTableModel modelProductos) {
		this.modelProductos = modelProductos;
	}

	public JTable getTablaProductos() {
		return tablaProductos;
	}

	public void setTablaProductos(JTable tablaProductos) {
		this.tablaProductos = tablaProductos;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}
}
