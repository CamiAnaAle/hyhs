package com.presentacion.vistas.panelesServicios;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.sql.Time;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.modelo.clases.Cliente;
import com.modelo.clases.Servicio;

public class PanelBuscarServicio extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtIngreseTexto;
	private DefaultTableModel modelServicios;
	private String[] nombreColumnas = { "Descripcion", "Precio", "Tiempo Estimado"};
	private JTable tablaServicios;	
	
	public static PanelBuscarServicio INSTANCE;
	
	public static PanelBuscarServicio getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PanelBuscarServicio();
			return new PanelBuscarServicio();
		} else
			return INSTANCE;
	}
	
	public PanelBuscarServicio() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel pnlBuscar = new JPanel();
		pnlBuscar.setLayout(null);
		pnlBuscar.setBackground(Color.WHITE);
		pnlBuscar.setBounds(0, 0, 968, 504);
		add(pnlBuscar);
		
		JLabel lblListado = new JLabel();
		lblListado.setText("LISTADO");
		lblListado.setForeground(SystemColor.windowBorder);
		lblListado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblListado.setBounds(15, 16, 101, 22);
		pnlBuscar.add(lblListado);
		
		JLabel lblIngreseTexto = new JLabel("INGRESE TEXTO:");
		lblIngreseTexto.setBounds(204, 18, 162, 20);
		pnlBuscar.add(lblIngreseTexto);
		
		txtIngreseTexto = new JTextField();
		txtIngreseTexto.setColumns(10);
		txtIngreseTexto.setBounds(368, 15, 214, 26);
		pnlBuscar.add(txtIngreseTexto);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 67, 832, 395);
		pnlBuscar.add(scrollPane);
		
		modelServicios = new DefaultTableModel(null, nombreColumnas);
		tablaServicios = new JTable(modelServicios);

		tablaServicios.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaServicios.getColumnModel().getColumn(0).setResizable(false);
		tablaServicios.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaServicios.getColumnModel().getColumn(1).setResizable(false);
		
		scrollPane.setViewportView(tablaServicios);
		
		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.setBounds(618, 14, 115, 29);
		pnlBuscar.add(btnBuscar);
	}
	
	

	public JTextField getTxtIngreseTexto() {
		return txtIngreseTexto;
	}



	public void setTxtIngreseTexto(JTextField txtIngreseTexto) {
		this.txtIngreseTexto = txtIngreseTexto;
	}



	public DefaultTableModel getModelServicios() {
		return modelServicios;
	}



	public void setModelClientes(DefaultTableModel modelServicios) {
		this.modelServicios = modelServicios;
	}



	public String[] getNombreColumnas() {
		return nombreColumnas;
	}



	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}



	public JTable getTablaClientes() {
		return tablaServicios;
	}



	public void setTablaClientes(JTable tablaServicios) {
		this.tablaServicios = tablaServicios;
	}



	public void llenarTabla(List<Servicio> serviciosRegistrados) {
		
		this.getModelServicios().setRowCount(0); // Para vaciar la tabla
		this.getModelServicios().setColumnCount(0);
		this.getModelServicios().setColumnIdentifiers(this.getNombreColumnas());
		
		for (Servicio s : serviciosRegistrados) {
			String descripcion = s.getDescripcion();
			Double precio = s.getPrecio();
			Time tiempoestimado=s.getTiempoEstimado();
			
			Object[] fila = { descripcion, precio, tiempoestimado};
			this.getModelServicios().addRow(fila);
		}
	}
}