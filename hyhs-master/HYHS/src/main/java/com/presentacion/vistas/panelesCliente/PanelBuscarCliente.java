package com.presentacion.vistas.panelesCliente;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.modelo.clases.Cliente;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelBuscarCliente extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtIngreseTexto;
	private DefaultTableModel modelClientes;
	private String[] nombreColumnas = { "Nombre", "Apellido", "DNI", "Telefono", "Celular","Mail", "Deuda","Direccion", "Localidad", "Tipo" ,"Fecha Nacimiento","Fecha Actualizacion deuda",
			"Total Adeudado","Turnos solicitados","Turnos presentes","Ausente","Cancelados cliente","Cancelados emrpesa","Puntos","Estado"};
	private JTable tablaClientes;	
	private JButton btnBuscar;
	
	public static PanelBuscarCliente INSTANCE;
	
	public static PanelBuscarCliente getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PanelBuscarCliente();
			return new PanelBuscarCliente();
		} else
			return INSTANCE;
	}
	
	public PanelBuscarCliente() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel pnlBuscar = new JPanel();
		pnlBuscar.setLayout(null);
		pnlBuscar.setBackground(Color.WHITE);
		pnlBuscar.setBounds(0, 0, 968, 504);
		add(pnlBuscar);
		
		JLabel lblListado = new JLabel();
		lblListado.setText("LISTADO");
		lblListado.setForeground(SystemColor.windowBorder);
		lblListado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblListado.setBounds(15, 16, 101, 22);
		pnlBuscar.add(lblListado);
		
		JLabel lblIngreseTexto = new JLabel("INGRESE TEXTO:");
		lblIngreseTexto.setBounds(204, 18, 162, 20);
		pnlBuscar.add(lblIngreseTexto);
		
		txtIngreseTexto = new JTextField();
		txtIngreseTexto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		txtIngreseTexto.setColumns(10);
		txtIngreseTexto.setBounds(368, 15, 214, 26);
		pnlBuscar.add(txtIngreseTexto);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 67, 832, 395);
		pnlBuscar.add(scrollPane);
		
		modelClientes = new DefaultTableModel(null, nombreColumnas);
		tablaClientes = new JTable(modelClientes);

		tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPane.setViewportView(tablaClientes);
		
		
		 btnBuscar = new JButton("BUSCAR");
		 btnBuscar.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 	}
		 });
		btnBuscar.setBounds(618, 14, 115, 29);
		pnlBuscar.add(btnBuscar);
	}
	
	public JButton getBtnBuscar()
	{
		return btnBuscar;
	}

	public JTextField getTxtIngreseTexto() {
		return txtIngreseTexto;
	}



	public void setTxtIngreseTexto(JTextField txtIngreseTexto) {
		this.txtIngreseTexto = txtIngreseTexto;
	}



	public DefaultTableModel getModelClientes() {
		return modelClientes;
	}



	public void setModelClientes(DefaultTableModel modelClientes) {
		this.modelClientes = modelClientes;
	}



	public String[] getNombreColumnas() {
		return nombreColumnas;
	}



	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}



	public JTable getTablaClientes() {
		return tablaClientes;
	}



	public void setTablaClientes(JTable tablaClientes) {
		this.tablaClientes = tablaClientes;
	}



	public void llenarTabla(List<Cliente> clientesRegistrados) {
		
		this.getModelClientes().setRowCount(0); // Para vaciar la tabla
		this.getModelClientes().setColumnCount(0);
		this.getModelClientes().setColumnIdentifiers(this.getNombreColumnas());
		
		for (Cliente c : clientesRegistrados) {
			
			String nombre = c.getNombre();
			String apellido = c.getApellido();
			String dni = Long.toString(c.getDni());
			String tel = c.getTelefono();
			String cel = c.getCelular();
			String mail = c.getEmail();
			String deuda = Integer.toString(c.getTotalAdeudado());
			String direccion =c.getDireccion();
			String localidad = c.getLocalidad().getDescripcion();
			String tipo = c.getTipoDeCliente().getDescripcion();
			String nacimiento = c.getNacimiento().toString();
			String fechaactualizaciondeuda = c.getActualizacionDeuda().toString();
			String totaladeudado=Integer.toString (c.getTotalAdeudado());
			String turnossolicitados=Integer.toString(c.getCantTurnosSolicitados());
			String turnospresentes=Integer.toString(c.getCantTurnosPresente());
			String turnosausente=Integer.toString(c.getCantTurnosAusente());
			String canceladoscliente=Integer.toString(c.getCantTurnosCanceladoCliente());
			String canceladosempresa=Integer.toString(c.getCantTurnosCanceladoEmpresa());
			String puntos=Integer.toString(c.getPuntos());
			String estado=Boolean.toString(c.getEstado());
			
			Object[] fila = { nombre, apellido, dni, tel,cel, mail, deuda,direccion, localidad, tipo,nacimiento,fechaactualizaciondeuda,
					totaladeudado,turnossolicitados,turnospresentes,turnosausente,canceladoscliente,canceladosempresa,puntos,estado};
			this.getModelClientes().addRow(fila);
		}
	}
}
