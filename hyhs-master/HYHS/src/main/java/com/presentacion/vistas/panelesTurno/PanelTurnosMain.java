package com.presentacion.vistas.panelesTurno;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JTabbedPane;
import java.awt.Font;

public class PanelTurnosMain extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static PanelTurnoAgregar pnlAgregar;
	private static PanelTurnosCancelar pnlCancelar;
	private static PanelTurnosConsultar pnlConsultar;
	private static PanelTurnosModificar pnlModificar;
	private JTabbedPane tabbedPane;

	/**
	 * Create the panel.
	 */

	public PanelTurnosMain() {
		setBackground(Color.WHITE);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 1001, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 522, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		pnlAgregar = new PanelTurnoAgregar();
		pnlAgregar.setBackground(Color.WHITE);
		tabbedPane.addTab("AGREGAR TURNOS", null, pnlAgregar, null);

		pnlCancelar = new PanelTurnosCancelar();
		tabbedPane.addTab("CANCELAR TURNO", null, pnlCancelar, null);

		pnlConsultar = new PanelTurnosConsultar();
		pnlConsultar.setBackground(Color.WHITE);
		tabbedPane.addTab("CONSULTAR TURNOS", null, pnlConsultar, null);

		pnlModificar = new PanelTurnosModificar();
		pnlModificar.setBackground(Color.WHITE);
		tabbedPane.addTab("MODIFICAR TURNOS", null, pnlModificar, null);

		setLayout(groupLayout);
	}

	public static PanelTurnoAgregar getPnlAgregar() {
		return pnlAgregar;
	}

	public static void setPnlAgregar(PanelTurnoAgregar pnlAgregar) {
		PanelTurnosMain.pnlAgregar = pnlAgregar;
	}

	public PanelTurnosCancelar getPnlCancelar() {
		return pnlCancelar;
	}

	public void setPnlCancelar(PanelTurnosCancelar pnlCancelar) {
		PanelTurnosMain.pnlCancelar = pnlCancelar;
	}

	public PanelTurnosConsultar getPnlConsultar() {
		return pnlConsultar;
	}

	public void setPnlConsultar(PanelTurnosConsultar pnlConsultar) {
		PanelTurnosMain.pnlConsultar = pnlConsultar;
	}

	public static PanelTurnosModificar getPnlModificar() {
		return pnlModificar;
	}

	public static void setPnlModificar(PanelTurnosModificar pnlModificar) {
		PanelTurnosMain.pnlModificar = pnlModificar;
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}
}
