package com.presentacion.vistas.panelesCliente;

import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JTabbedPane;

/**
 *
 * @author RojeruSan
 */
public class PanelClientesMain extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static PanelAgregarCliente pnlAgregar;
	private static PanelBuscarCliente pnlBuscar;
	private static PanelModificarCliente pnlModificar;
	
	public static PanelAgregarCliente getPnlAgregar() {

		return pnlAgregar;
	}

	public static void setPnlAgregar(PanelAgregarCliente pnlAgregar) {
		PanelClientesMain.pnlAgregar = pnlAgregar;
	}

	public PanelClientesMain() {
		initComponents();
	}
	
	public static PanelBuscarCliente getPnlBuscar() {
		return pnlBuscar;
	}
	
	public static PanelModificarCliente getPnlModificar() {
		return pnlModificar;
	}

	public static void setPnlBuscar(PanelBuscarCliente pnlBuscar) {
		PanelClientesMain.pnlBuscar = pnlBuscar;
	}

	private void initComponents() {

		setBackground(new java.awt.Color(255, 255, 255));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 14));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 1015, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(104, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(85, Short.MAX_VALUE)));

		pnlAgregar = new PanelAgregarCliente();
		pnlAgregar.setBackground(Color.WHITE);

		tabbedPane.addTab("AGREGAR", null, pnlAgregar, "Tab 2 ");

		pnlModificar = new PanelModificarCliente();
		pnlModificar.setBackground(Color.WHITE);

		tabbedPane.addTab("MODIFICAR", null, pnlModificar, "Tab 1 ");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);

		pnlBuscar = new PanelBuscarCliente();

		tabbedPane.addTab("LISTAR", null, pnlBuscar, "Tab 3 ");

		this.setLayout(layout);
	}
}
