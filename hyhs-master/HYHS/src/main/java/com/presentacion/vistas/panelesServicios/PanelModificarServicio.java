package com.presentacion.vistas.panelesServicios;

import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import validaciones.validaLetras;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelModificarServicio extends JPanel {
	private JTextField txtDescripcion;
	private JTextField txtPrecio;
	private JTextField txtTiempoEstimado;
	private JButton btnRegistrar;


	public PanelModificarServicio() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(-12, 0, 1066, 514);
		add(panel);
		
		JLabel lblDescripcion = new JLabel("DESCRIPCION:");
		lblDescripcion.setBounds(62, 82, 122, 20);
		panel.add(lblDescripcion);
		
		JLabel lblPrecio = new JLabel("PRECIO:");
		lblPrecio.setBounds(62, 115, 111, 20);
		panel.add(lblPrecio);
		
		JLabel lblTiempoEstimado = new JLabel("TIEMPO ESTIMADO:");
		lblTiempoEstimado.setBounds(62, 151, 97, 20);
		panel.add(lblTiempoEstimado);
		
		
		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);

		txtDescripcion.addKeyListener(new validaLetras());
		txtDescripcion.setBounds(194, 79, 146, 26);
		panel.add(txtDescripcion);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(194, 112, 146, 26);
		panel.add(txtPrecio);
		
		txtTiempoEstimado = new JTextField();
		txtTiempoEstimado.setColumns(10);
		txtTiempoEstimado.setBounds(194, 148, 146, 26);
		panel.add(txtTiempoEstimado);
		
		JLabel lblId = new JLabel("ID SERVICIO:");
		lblId.setBounds(50, 34, 157, 20);
		panel.add(lblId);
		
		
		btnRegistrar = new JButton("MODIFICAR");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRegistrar.setBackground(Color.WHITE);
		btnRegistrar.setBounds(194, 228, 146, 29);
		panel.add(btnRegistrar);
		
	}
	
	public JTextField getTxtDescripcion() {
		return txtDescripcion;
	}


	public void setTxtDescripcion(JTextField txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}


	public JTextField getTxtPrecio() {
		return txtPrecio;
	}


	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}


	public JTextField getTxtTiempoEstimado() {
		return txtTiempoEstimado;
	}


	public void setTxtApellido(JTextField txtTiempoEstimado) {
		this.txtTiempoEstimado = txtTiempoEstimado;
	}


}
