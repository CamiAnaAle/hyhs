package com.presentacion.vistas.panelesTurno;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.modelo.clases.Empleado;
import com.modelo.clases.Franja;
import com.modelo.clases.Servicio;

import java.awt.Button;
import java.awt.Canvas;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.lang.Thread.State;
import java.awt.event.ItemEvent;
import javax.swing.JSpinner;
import javax.swing.border.LineBorder;

public class PanelTurno_Servicio extends JPanel{
	
	private JTextField txtHora;
	private JTextField txtTiempo;
	private JTextField txtPrecio;
	private JButton btnAgregarServicio;
	private JComboBox<Franja> cbRango;
	private JComboBox<Empleado> cbEmpleado;
	private JComboBox<Empleado> cbEmpleadoHora;
	private JComboBox<Servicio> cbTipoServicio;
	private JRadioButton rdbtnPorEmpleado;
	
	

	public JComboBox<Franja> getCbRango() {
		return cbRango;
	}

	public void setCbRango(JComboBox<Franja> cbRango) {
		this.cbRango = cbRango;
	}

	public JComboBox<Empleado> getCbEmpleadoHora() {
		return cbEmpleadoHora;
	}

	public void setCbEmpleadoHora(JComboBox<Empleado> cbEmpleadoHora) {
		this.cbEmpleadoHora = cbEmpleadoHora;
	}

	public JComboBox<Empleado> getCbEmpleado() {
		return cbEmpleado;
	}

	public void setCbEmpleado(JComboBox<Empleado> cbEmpleado) {
		this.cbEmpleado = cbEmpleado;
	}

	public JTextField getTxtHora() {
		return txtHora;
	}

	public void setTxtHora(JTextField txtHora) {
		this.txtHora = txtHora;
	}

	public JTextField getTxtTiempo() {
		return txtTiempo;
	}

	public void setTxtTiempo(JTextField txtTiempo) {
		this.txtTiempo = txtTiempo;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}


	public JComboBox<Servicio> getCbTipoServicio() {
		return cbTipoServicio;
	}

	public void setCbTipoServicio(JComboBox<Servicio> cbTipoServicio) {
		this.cbTipoServicio = cbTipoServicio;
	}

	public JRadioButton getRdbtnPorHorario() {
		return rdbtnPorHorario;
	}

	public void setRdbtnPorHorario(JRadioButton rdbtnPorHorario) {
		this.rdbtnPorHorario = rdbtnPorHorario;
	}

	public JButton getBtnAgregarServicio() {
		return btnAgregarServicio;
	}

	public void setBtnAgregarServicio(JButton btnAgregarServicio) {
		this.btnAgregarServicio = btnAgregarServicio;
	}

	public JRadioButton getRdbtnPorEmpleado() {
		return rdbtnPorEmpleado;
	}

	public void setRdbtnPorEmpleado(JRadioButton rdbtnPorEmpleado) {
		this.rdbtnPorEmpleado = rdbtnPorEmpleado;
	}

	
	public PanelTurno_Servicio() {
		setBackground(Color.WHITE);
		setLayout(null);
		setBounds(3, 3, 742, 169);
		
		JLabel lblTipoServicio = new JLabel("TIPO SERVICIO:");
		lblTipoServicio.setBounds(15, 10, 249, 20);
		add(lblTipoServicio);
		
		cbTipoServicio = new JComboBox<Servicio>();
		cbTipoServicio.setBounds(164, 8, 193, 26);
		add(cbTipoServicio);
		
		JLabel lblBusqueda = new JLabel("BUSQUEDA");
		lblBusqueda.setBounds(45, 46, 198, 20);
		add(lblBusqueda);
		
		
		
		JLabel label_2 = new JLabel("EMPLEADO:");
		label_2.setBounds(55, 82, 115, 20);
		add(label_2);
		
		cbEmpleado = new JComboBox<Empleado>();
		cbEmpleado.setBounds(188, 79, 169, 26);
		add(cbEmpleado);
		
		JLabel label_3 = new JLabel("RANGO LIBRE");
		label_3.setBounds(55, 118, 249, 20);
		add(label_3);
		
	
		cbRango = new JComboBox<Franja>();
		cbRango.setBounds(188, 108, 169, 26);
		add(cbRango);
		
		JLabel lblHora = new JLabel("HORA");
		lblHora.setBounds(405, 82, 115, 20);
		add(lblHora);
		
		JLabel lbl = new JLabel("EMPLEADO");
		lbl.setBounds(384, 118, 84, 20);
		add(lbl);
		
		 btnAgregarServicio = new JButton("AGREGAR");
		btnAgregarServicio.setBounds(601, 140, 115, 29);
		add(btnAgregarServicio);
	
	
		
		
		JLabel label_6 = new JLabel("HORA:");
		label_6.setBounds(94, 149, 65, 20);
		add(label_6);
		
		txtHora = new JTextField();
		txtHora.setColumns(10);
		txtHora.setBounds(188, 138, 169, 26);
		add(txtHora);
		
		JLabel label_7 = new JLabel("PRECIO");
		label_7.setBounds(565, 10, 115, 20);
		add(label_7);
		
		txtTiempo = new JTextField();
		txtTiempo.setEditable(false);
		txtTiempo.setColumns(10);
		txtTiempo.setBounds(459, 8, 84, 26);
		add(txtTiempo);
		
		JLabel lblTiempo = new JLabel("TIEMPO:");
		lblTiempo.setBounds(383, 10, 160, 20);
		add(lblTiempo);
		
		cbEmpleadoHora = new JComboBox<Empleado>();
		cbEmpleadoHora.setBounds(483, 108, 169, 26);
		add(cbEmpleadoHora);
		
		txtPrecio = new JTextField();
		txtPrecio.setEditable(false);
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(635, 8, 84, 26);
		add(txtPrecio);
		
		Canvas canvas_2 = new Canvas();
		canvas_2.setBackground(Color.GRAY);
		canvas_2.setBounds(379, 50, 1, 126);
		add(canvas_2);
		
		
		rdbtnPorEmpleado = new JRadioButton("POR EMPLEADO");
		rdbtnPorEmpleado.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED)
				{
					cbEmpleado.setEnabled(true);
					cbRango.setEnabled(true);
					txtHora.setEnabled(true);
				//	cbHora.setEnabled(false);
					cbEmpleadoHora.setEnabled(false);
				}

			}
		});
		rdbtnPorEmpleado.setSelected(true);
		rdbtnPorEmpleado.setBackground(Color.WHITE);
		rdbtnPorEmpleado.setBounds(174, 42, 159, 29);
		add(rdbtnPorEmpleado);
		
	  rdbtnPorHorario = new JRadioButton("POR HORARIO");
		rdbtnPorHorario.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
			
				if(e.getStateChange()==ItemEvent.SELECTED)
				{
					
				//	cbHora.setEnabled(true);
					cbEmpleadoHora.setEnabled(true);
					cbEmpleado.setEnabled(false);
					cbRango.setEnabled(false);
					txtHora.setEnabled(false);
				}
			}
		});
		rdbtnPorHorario.setBackground(Color.WHITE);
		rdbtnPorHorario.setBounds(405, 42, 159, 29);
		add(rdbtnPorHorario);
		cbEmpleadoHora.setEnabled(false);
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(rdbtnPorEmpleado);
		grupo.add(rdbtnPorHorario);
		
		SpinnerNumberModel modelHora = new SpinnerNumberModel(7,7,22,1);
		spHora = new JSpinner();
		spHora.setModel(modelHora);
		spHora.setBounds(483, 79, 55, 26);
		add(spHora);
		
		SpinnerNumberModel modelMinutos = new SpinnerNumberModel(0,0,60,10);
		spMinutos = new JSpinner(modelMinutos);
		spMinutos.setBounds(567, 79, 55, 26);
		add(spMinutos);
		
		JLabel lblHs = new JLabel("Hs");
		lblHs.setBounds(541, 82, 27, 20);
		add(lblHs);
		
		JLabel lblMin = new JLabel("Min.");
		lblMin.setBounds(625, 82, 38, 20);
		add(lblMin);
		
		Button btnOk = new Button("OK");
		btnOk.setBounds(661, 78, 55, 27);
		add(btnOk);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel.setBackground(Color.WHITE);
		panel.setBounds(25, 41, 698, 31);
		add(panel);
		
	}
	
	JSpinner spHora;
	JSpinner spMinutos;
	JRadioButton rdbtnPorHorario;
}
