package com.presentacion.vistas.panelesTurno;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Canvas;

public class PanelTurno_Producto extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtIdProducto;
	private JTextField txtNombre;
	private JTextField txtCantidad;
	private JTextField txtPrecio;
	private JTextField txtImporteTotal;
	private JButton btnBuscarProducto;

	public PanelTurno_Producto()
	{
		setBackground(Color.WHITE);
		setLayout(null);
		setBounds(3, 3, 715, 145);
		
		Canvas canvas = new Canvas();
		canvas.setBackground(Color.LIGHT_GRAY);
		canvas.setBounds(25, 50, 626, 1);
		add(canvas);
		
		JLabel label = new JLabel("ID PRODUCTO");
		label.setBounds(15, 16, 122, 20);
		add(label);
		
		txtIdProducto = new JTextField();
		txtIdProducto.setColumns(10);
		txtIdProducto.setBounds(166, 13, 146, 26);
		add(txtIdProducto);
		
		JLabel label_1 = new JLabel("NOMBRE");
		label_1.setBounds(49, 113, 122, 20);
		add(label_1);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setColumns(10);
		txtNombre.setBounds(166, 110, 146, 26);
		add(txtNombre);
		
		JLabel label_2 = new JLabel("CANTIDAD");
		label_2.setBounds(29, 77, 122, 20);
		add(label_2);
		
		txtCantidad = new JTextField();
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(166, 74, 146, 26);
		add(txtCantidad);
		
		JButton btnAceptarProducto = new JButton("ACEPTAR");
		btnAceptarProducto.setBounds(325, 12, 115, 29);
		add(btnAceptarProducto);
		
		btnBuscarProducto = new JButton("BUSCAR");
		btnBuscarProducto.setBounds(455, 12, 93, 29);
		add(btnBuscarProducto);
		
		JLabel label_3 = new JLabel("PRECIO");
		label_3.setBounds(327, 77, 122, 20);
		add(label_3);
		
		JLabel label_4 = new JLabel("IMP. TOTAL");
		label_4.setBounds(327, 113, 122, 20);
		add(label_4);
		
		txtPrecio = new JTextField();
		txtPrecio.setEditable(false);
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(434, 74, 146, 26);
		add(txtPrecio);
		
		txtImporteTotal = new JTextField();
		txtImporteTotal.setEditable(false);
		txtImporteTotal.setColumns(10);
		txtImporteTotal.setBounds(434, 110, 146, 26);
		add(txtImporteTotal);
		
		JButton btnAgregarProducto = new JButton("AGREGAR");
		btnAgregarProducto.setBounds(595, 109, 115, 29);
		add(btnAgregarProducto);
	}

	public JButton getBtnBuscarProducto() {
		return btnBuscarProducto;
	}

	public void setBtnBuscarProducto(JButton btnBuscarProducto) {
		this.btnBuscarProducto = btnBuscarProducto;
	}

	public JTextField getTxtIdProducto() {
		return txtIdProducto;
	}

	public void setTxtIdProducto(JTextField txtIdProducto) {
		this.txtIdProducto = txtIdProducto;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtCantidad() {
		return txtCantidad;
	}

	public void setTxtCantidad(JTextField txtCantidad) {
		this.txtCantidad = txtCantidad;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}

	public JTextField getTxtImporteTotal() {
		return txtImporteTotal;
	}

	public void setTxtImporteTotal(JTextField txtImporteTotal) {
		this.txtImporteTotal = txtImporteTotal;
	}
}
