/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.presentacion.vistas;

import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.GroupLayout.Alignment;

import com.presentacion.vistas.panelesCliente.PanelClientesMain;
import com.presentacion.vistas.panelesPromos.PanelPromocionMain;
import com.presentacion.vistas.panelesServicios.PanelServiciosMain;
import com.presentacion.vistas.panelesTurno.PanelTurnosMain;

import com.presentacion.vistas.panelesEmpleado.PanelEmpleadosMain;

import javax.swing.GroupLayout;
import java.awt.GridBagConstraints;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.Cursor;

/**
 *
 * @author RojeruSan
 */
public class Vista extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int x, y;
	/**
	 * Creates new form Principal
	 */
	public static PanelTurnosMain pnlTurnos;
	public static PanelClientesMain pnlClientes;
	public static PanelServiciosMain pnlServicios;
	public static PanelPromocionMain pnlPromociones;
    public static PanelEmpleadosMain pnlEmpleado;

	public Vista() {
		setTitle("Recepcionista");
		setResizable(false);
		initComponents();
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setLocationRelativeTo(this);
		this.btnTurnos.setSelected(true);

		pnlTurnos = new PanelTurnosMain();

		pnlClientes = new PanelClientesMain();
		
        pnlEmpleado = new PanelEmpleadosMain();

		pnlServicios = new PanelServiciosMain();
		
		pnlPromociones = new PanelPromocionMain();

		new CambiaPanel(pnlCentral, pnlTurnos);
	}

	@SuppressWarnings("unchecked")

	private void initComponents() {
		java.awt.GridBagConstraints gridBagConstraints;

		pnlContenedor = new javax.swing.JPanel();
		pnlMenu = new javax.swing.JPanel();
		btnTurnos = new rsbuttom.RSButtonMetro();
		pnlTituloMenu = new javax.swing.JPanel();
		lblMenu = new javax.swing.JLabel();
		btnServicios = new rsbuttom.RSButtonMetro();
		btnClientes = new rsbuttom.RSButtonMetro();
		cinco = new rsbuttom.RSButtonMetro();
		cinco.setText("Productos");
		ocho = new rsbuttom.RSButtonMetro();
		siete = new rsbuttom.RSButtonMetro();
		seis = new rsbuttom.RSButtonMetro();
		seis.setText("Servicios");
		btnPromociones = new rsbuttom.RSButtonMetro();
		btnPromociones.setText("Promociones");
		// cuatro.setText("VACIO");
		pnlSuperior = new javax.swing.JPanel();
		btnDeslizar = new javax.swing.JButton();
		lblNombreDeUsuario = new javax.swing.JLabel();
		pnlCentral = new javax.swing.JPanel();
		scrollPanelCentral = new javax.swing.JScrollPane();
		scrollPanelCentral.setSize(200, 300);
		scrollPanelCentral.setBackground(Color.LIGHT_GRAY);
		pnlPrincipal = new javax.swing.JPanel();
		pnlPrincipal.setSize(100, 100);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		pnlContenedor.setBackground(new java.awt.Color(255, 255, 255));
		pnlContenedor.setLayout(new java.awt.GridBagLayout());

		pnlMenu.setBackground(new java.awt.Color(239, 238, 244));
		pnlMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 5, 0, 0, new java.awt.Color(239, 238, 244)));

		btnTurnos.setForeground(new java.awt.Color(128, 128, 131));
		btnTurnos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/home.png"))); // NOI18N
		btnTurnos.setText("Turnos");
		btnTurnos.setColorHover(new java.awt.Color(204, 204, 204));
		btnTurnos.setColorNormal(new java.awt.Color(204, 204, 204));
		btnTurnos.setColorPressed(new java.awt.Color(204, 204, 204));
		btnTurnos.setColorTextHover(new java.awt.Color(128, 128, 131));
		btnTurnos.setColorTextNormal(new java.awt.Color(128, 128, 131));
		btnTurnos.setColorTextPressed(new java.awt.Color(128, 128, 131));
		btnTurnos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		btnTurnos.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnTurnos.setIconTextGap(25);
		btnTurnos.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				unoMousePressed(evt);
			}
		});
		btnTurnos.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				unoActionPerformed(evt);
			}
		});

		pnlTituloMenu.setBackground(new java.awt.Color(239, 238, 244));

		lblMenu.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
		lblMenu.setForeground(new java.awt.Color(128, 128, 131));
		lblMenu.setText("MENU");

		javax.swing.GroupLayout gl_pnlTituloMenu = new javax.swing.GroupLayout(pnlTituloMenu);
		pnlTituloMenu.setLayout(gl_pnlTituloMenu);
		gl_pnlTituloMenu
				.setHorizontalGroup(gl_pnlTituloMenu.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(gl_pnlTituloMenu.createSequentialGroup().addContainerGap().addComponent(lblMenu)
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		gl_pnlTituloMenu
				.setVerticalGroup(gl_pnlTituloMenu.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, gl_pnlTituloMenu.createSequentialGroup()
								.addContainerGap(41, Short.MAX_VALUE).addComponent(lblMenu).addContainerGap()));

		btnServicios.setBackground(new java.awt.Color(239, 238, 244));
		btnServicios.setForeground(new java.awt.Color(128, 128, 131));
		btnServicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/chat.png"))); // NOI18N
		btnServicios.setText("Servicios");
		btnServicios.setColorHover(new java.awt.Color(204, 204, 204));
		btnServicios.setColorNormal(new java.awt.Color(239, 238, 244));
		btnServicios.setColorPressed(new java.awt.Color(204, 204, 204));
		btnServicios.setColorTextHover(new java.awt.Color(128, 128, 131));
		btnServicios.setColorTextNormal(new java.awt.Color(128, 128, 131));
		btnServicios.setColorTextPressed(new java.awt.Color(128, 128, 131));
		btnServicios.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		btnServicios.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnServicios.setIconTextGap(17);
		btnServicios.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				tresMousePressed(evt);
			}
		});
		btnServicios.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				tresActionPerformed(evt);
			}
		});

		btnClientes.setBackground(new java.awt.Color(239, 238, 244));
		btnClientes.setForeground(new java.awt.Color(128, 128, 131));
		btnClientes.setIcon(new ImageIcon(Vista.class.getResource("/img/android.png"))); // NOI18N
		btnClientes.setText("Clientes");
		btnClientes.setColorHover(new java.awt.Color(204, 204, 204));
		btnClientes.setColorNormal(new java.awt.Color(239, 238, 244));
		btnClientes.setColorPressed(new java.awt.Color(204, 204, 204));
		btnClientes.setColorTextHover(new java.awt.Color(128, 128, 131));
		btnClientes.setColorTextNormal(new java.awt.Color(128, 128, 131));
		btnClientes.setColorTextPressed(new java.awt.Color(128, 128, 131));
		btnClientes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		btnClientes.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnClientes.setIconTextGap(25);
		btnClientes.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				dosMousePressed(evt);
			}
		});
		btnClientes.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dosActionPerformed(evt);
			}
		});

		cinco.setBackground(new java.awt.Color(239, 238, 244));
		cinco.setForeground(new java.awt.Color(128, 128, 131));
		cinco.setIcon(new ImageIcon(Vista.class.getResource("/img/web.png"))); // NOI18N
		// cinco.setText("VACIO");
		cinco.setColorHover(new java.awt.Color(204, 204, 204));
		cinco.setColorNormal(new java.awt.Color(239, 238, 244));
		cinco.setColorPressed(new java.awt.Color(204, 204, 204));
		cinco.setColorTextHover(new java.awt.Color(128, 128, 131));
		cinco.setColorTextNormal(new java.awt.Color(128, 128, 131));
		cinco.setColorTextPressed(new java.awt.Color(128, 128, 131));
		cinco.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		cinco.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		cinco.setIconTextGap(19);
		cinco.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				cincoMousePressed(evt);
			}
		});
		cinco.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cincoActionPerformed(evt);
			}
		});

		ocho.setBackground(new java.awt.Color(239, 238, 244));
		ocho.setForeground(new java.awt.Color(128, 128, 131));
		// ocho.setIcon(new
		// javax.swing.ImageIcon(getClass().getResource("/img/web.png"))); // NOI18N
		// ocho.setText("VACIO");
		ocho.setColorHover(new java.awt.Color(204, 204, 204));
		ocho.setColorNormal(new java.awt.Color(239, 238, 244));
		ocho.setColorPressed(new java.awt.Color(204, 204, 204));
		ocho.setColorTextHover(new java.awt.Color(128, 128, 131));
		ocho.setColorTextNormal(new java.awt.Color(128, 128, 131));
		ocho.setColorTextPressed(new java.awt.Color(128, 128, 131));
		ocho.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		ocho.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		ocho.setIconTextGap(19);
		ocho.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				ochoMousePressed(evt);
			}
		});
		ocho.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ochoActionPerformed(evt);
			}
		});

		siete.setBackground(new java.awt.Color(239, 238, 244));
		siete.setForeground(new java.awt.Color(128, 128, 131));
		// siete.setIcon(new
		// javax.swing.ImageIcon(getClass().getResource("/img/ios.png"))); // NOI18N
		// siete.setText("VACIO");
		siete.setColorHover(new java.awt.Color(204, 204, 204));
		siete.setColorNormal(new java.awt.Color(239, 238, 244));
		siete.setColorPressed(new java.awt.Color(204, 204, 204));
		siete.setColorTextHover(new java.awt.Color(128, 128, 131));
		siete.setColorTextNormal(new java.awt.Color(128, 128, 131));
		siete.setColorTextPressed(new java.awt.Color(128, 128, 131));
		siete.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		siete.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		siete.setIconTextGap(19);
		siete.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				sieteMousePressed(evt);
			}
		});
		siete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sieteActionPerformed(evt);
			}
		});

		seis.setBackground(new java.awt.Color(239, 238, 244));
		seis.setForeground(new java.awt.Color(128, 128, 131));
		seis.setIcon(new ImageIcon(Vista.class.getResource("/img/web.png"))); // NOI18N
		// seis.setText("VACIO");
		seis.setColorHover(new java.awt.Color(204, 204, 204));
		seis.setColorNormal(new java.awt.Color(239, 238, 244));
		seis.setColorPressed(new java.awt.Color(204, 204, 204));
		seis.setColorTextHover(new java.awt.Color(128, 128, 131));
		seis.setColorTextNormal(new java.awt.Color(128, 128, 131));
		seis.setColorTextPressed(new java.awt.Color(128, 128, 131));
		seis.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		seis.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		seis.setIconTextGap(25);
		seis.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				seisMousePressed(evt);
			}
		});
		seis.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				seisActionPerformed(evt);
			}
		});

		btnPromociones.setBackground(new java.awt.Color(239, 238, 244));
		btnPromociones.setForeground(new java.awt.Color(128, 128, 131));
		btnPromociones.setIcon(new ImageIcon(Vista.class.getResource("/img/web.png")));
		btnPromociones.setColorHover(new java.awt.Color(204, 204, 204));
		btnPromociones.setColorNormal(new java.awt.Color(239, 238, 244));
		btnPromociones.setColorPressed(new java.awt.Color(204, 204, 204));
		btnPromociones.setColorTextHover(new java.awt.Color(128, 128, 131));
		btnPromociones.setColorTextNormal(new java.awt.Color(128, 128, 131));
		btnPromociones.setColorTextPressed(new java.awt.Color(128, 128, 131));
		btnPromociones.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		btnPromociones.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		btnPromociones.setIconTextGap(19);
		btnPromociones.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				cuatroMousePressed(evt);
			}
		});
		btnPromociones.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cuatroActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout pnlMenuLayout = new javax.swing.GroupLayout(pnlMenu);
		pnlMenuLayout.setHorizontalGroup(pnlMenuLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(pnlTituloMenu, GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
				.addGroup(pnlMenuLayout.createSequentialGroup().addGroup(pnlMenuLayout
						.createParallelGroup(Alignment.LEADING)
						.addComponent(btnTurnos, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnClientes, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnServicios, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnPromociones, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(cinco, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(seis, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(siete, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
						.addComponent(ocho, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE))
						.addGap(0, 0, Short.MAX_VALUE)));
		pnlMenuLayout.setVerticalGroup(pnlMenuLayout.createParallelGroup(Alignment.LEADING).addGroup(pnlMenuLayout
				.createSequentialGroup()
				.addComponent(pnlTituloMenu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addGap(12).addComponent(btnTurnos, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
				.addGap(12).addComponent(btnClientes, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
				.addGap(12).addComponent(btnServicios, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
				.addGap(12).addComponent(btnPromociones, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
				.addGap(12).addComponent(cinco, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(12)
				.addComponent(seis, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(12)
				.addComponent(siete, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(12)
				.addComponent(ocho, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)));
		pnlMenu.setLayout(pnlMenuLayout);

		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
		gridBagConstraints.weighty = 8.3;
		gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 1);
		pnlContenedor.add(pnlMenu, gridBagConstraints);

		pnlSuperior.setBackground(new java.awt.Color(38, 86, 186));
//        pnlSuperior.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
//            public void mouseDragged(java.awt.event.MouseEvent evt) {
//                jPanel2MouseDragged(evt);
//            }
//        });
//        pnlSuperior.addMouseListener(new java.awt.event.MouseAdapter() {
//            public void mousePressed(java.awt.event.MouseEvent evt) {
//                jPanel2MousePressed(evt);
//            }
//        });

		btnDeslizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/menu.png"))); // NOI18N
		btnDeslizar.setBorder(null);
		btnDeslizar.setContentAreaFilled(false);
		btnDeslizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		btnDeslizar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		lblNombreDeUsuario.setBackground(new java.awt.Color(255, 255, 255));
		lblNombreDeUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
		lblNombreDeUsuario.setForeground(new java.awt.Color(255, 255, 255));
		lblNombreDeUsuario.setText("NOMBRE DE RECEPCIONISTA");

		javax.swing.GroupLayout gl_pnlSuperior = new javax.swing.GroupLayout(pnlSuperior);
		gl_pnlSuperior.setHorizontalGroup(gl_pnlSuperior.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlSuperior.createSequentialGroup().addContainerGap().addComponent(btnDeslizar).addGap(18)
						.addComponent(lblNombreDeUsuario).addContainerGap(1486, Short.MAX_VALUE)));
		gl_pnlSuperior.setVerticalGroup(gl_pnlSuperior.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlSuperior.createSequentialGroup().addContainerGap()
						.addGroup(gl_pnlSuperior.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnDeslizar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(lblNombreDeUsuario, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		pnlSuperior.setLayout(gl_pnlSuperior);

		gbc_pnlSuperior = new java.awt.GridBagConstraints();
		gbc_pnlSuperior.gridx = 0;
		gbc_pnlSuperior.gridy = 0;
		gbc_pnlSuperior.gridwidth = 3;
		gbc_pnlSuperior.fill = java.awt.GridBagConstraints.HORIZONTAL;
		gbc_pnlSuperior.anchor = GridBagConstraints.PAGE_START;
		gbc_pnlSuperior.weightx = 0.2;
		pnlContenedor.add(pnlSuperior, gbc_pnlSuperior);

		pnlCentral.setBackground(new java.awt.Color(255, 255, 255));

		scrollPanelCentral.setBorder(null);

		pnlPrincipal.setBackground(new java.awt.Color(255, 255, 255));
		pnlPrincipal.setLayout(new javax.swing.BoxLayout(pnlPrincipal, javax.swing.BoxLayout.LINE_AXIS));
		scrollPanelCentral.setViewportView(pnlPrincipal);

		javax.swing.GroupLayout gl_pnlCentral = new javax.swing.GroupLayout(pnlCentral);
		gl_pnlCentral.setHorizontalGroup(gl_pnlCentral.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlCentral
						.createSequentialGroup().addComponent(scrollPanelCentral, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(1225, Short.MAX_VALUE)));
		gl_pnlCentral.setVerticalGroup(gl_pnlCentral.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlCentral.createSequentialGroup().addGap(2).addComponent(scrollPanelCentral,
						GroupLayout.PREFERRED_SIZE, 558, GroupLayout.PREFERRED_SIZE)));
		pnlCentral.setLayout(gl_pnlCentral);

		gbc_pnlCentral = new java.awt.GridBagConstraints();
		gbc_pnlCentral.gridx = 1;
		gbc_pnlCentral.gridy = 1;
		gbc_pnlCentral.gridwidth = 2;
		gbc_pnlCentral.fill = java.awt.GridBagConstraints.BOTH;
		gbc_pnlCentral.weightx = 0.4;
		gbc_pnlCentral.weighty = 0.1;
		pnlContenedor.add(pnlCentral, gbc_pnlCentral);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(pnlContenedor,
				GroupLayout.DEFAULT_SIZE, 1489, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(pnlContenedor,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void unoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_unoActionPerformed

		// pnlTurnos = new PanelTurnosMain();
		new CambiaPanel(pnlCentral, pnlTurnos);
		if (this.btnTurnos.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(204, 204, 204));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_unoActionPerformed

	private void unoMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_unoMousePressed
		this.btnTurnos.setSelected(true);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_unoMousePressed

	private void tresActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_tresActionPerformed
		new CambiaPanel(pnlCentral, new com.presentacion.vistas.pnlChat());
		if (this.btnServicios.isSelected()) {
			this.btnServicios.setColorNormal(new Color(204, 204, 204));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_tresActionPerformed

	private void tresMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tresMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(true);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_tresMousePressed

	private void dosMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_dosMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(true);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_dosMousePressed

	private void dosActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_dosActionPerformed

		new CambiaPanel(pnlCentral, pnlClientes);
		if (this.btnClientes.isSelected()) {
			this.btnClientes.setColorNormal(new Color(204, 204, 204));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_dosActionPerformed

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		int posicion = pnlMenu.getX();
		if (posicion > -1) {
			Animacion.Animacion.mover_izquierda(0, -264, 2, 2, pnlMenu);
		} else {
			Animacion.Animacion.mover_derecha(-264, 0, 2, 2, pnlMenu);
		}
	}// GEN-LAST:event_jButton1ActionPerformed

	private void ochoMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_ochoMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(true);
	}// GEN-LAST:event_ochoMousePressed

	private void ochoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_ochoActionPerformed
		new CambiaPanel(pnlCentral, new com.presentacion.vistas.pnlWeb());
		if (this.ocho.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(204, 204, 204));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_ochoActionPerformed

	private void sieteMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_sieteMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(true);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_sieteMousePressed

	private void sieteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_sieteActionPerformed
		new CambiaPanel(pnlCentral, new com.presentacion.vistas.pnlIOS());
		if (this.siete.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(204, 204, 204));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_sieteActionPerformed

	private void seisMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_seisMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(false);
		this.seis.setSelected(true);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_seisMousePressed

	private void seisActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_seisActionPerformed
		new CambiaPanel(pnlCentral, new com.presentacion.vistas.pnlAndroid());
		if (this.seis.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(204, 204, 204));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_seisActionPerformed

	private void cincoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cincoActionPerformed
		new CambiaPanel(pnlCentral, new com.presentacion.vistas.pnlMarket());
		if (this.cinco.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(204, 204, 204));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_cincoActionPerformed

	private void cincoMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_cincoMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(false);
		this.cinco.setSelected(true);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_cincoMousePressed

	private void cuatroMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_cuatroMousePressed
		this.btnTurnos.setSelected(false);
		this.btnClientes.setSelected(false);
		this.btnServicios.setSelected(false);
		this.btnPromociones.setSelected(true);
		this.cinco.setSelected(false);
		this.seis.setSelected(false);
		this.siete.setSelected(false);
		this.ocho.setSelected(false);
	}// GEN-LAST:event_cuatroMousePressed

	private void cuatroActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cuatroActionPerformed
		new CambiaPanel(pnlCentral, pnlPromociones);
		if (this.btnPromociones.isSelected()) {
			this.btnTurnos.setColorNormal(new Color(239, 238, 244));
			this.btnTurnos.setColorHover(new Color(204, 204, 204));
			this.btnTurnos.setColorPressed(new Color(204, 204, 204));

			this.btnClientes.setColorNormal(new Color(239, 238, 244));
			this.btnClientes.setColorHover(new Color(204, 204, 204));
			this.btnClientes.setColorPressed(new Color(204, 204, 204));

			this.btnServicios.setColorNormal(new Color(239, 238, 244));
			this.btnServicios.setColorHover(new Color(204, 204, 204));
			this.btnServicios.setColorPressed(new Color(204, 204, 204));

			this.btnPromociones.setColorNormal(new Color(204, 204, 204));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));

			this.cinco.setColorNormal(new Color(239, 238, 244));
			this.cinco.setColorHover(new Color(204, 204, 204));
			this.cinco.setColorPressed(new Color(204, 204, 204));

			this.seis.setColorNormal(new Color(239, 238, 244));
			this.seis.setColorHover(new Color(204, 204, 204));
			this.seis.setColorPressed(new Color(204, 204, 204));

			this.siete.setColorNormal(new Color(239, 238, 244));
			this.siete.setColorHover(new Color(204, 204, 204));
			this.siete.setColorPressed(new Color(204, 204, 204));

			this.ocho.setColorNormal(new Color(239, 238, 244));
			this.ocho.setColorHover(new Color(204, 204, 204));
			this.ocho.setColorPressed(new Color(204, 204, 204));
		} else {
			this.btnPromociones.setColorNormal(new Color(239, 238, 244));
			this.btnPromociones.setColorHover(new Color(204, 204, 204));
			this.btnPromociones.setColorPressed(new Color(204, 204, 204));
		}
	}// GEN-LAST:event_cuatroActionPerformed

	private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jPanel2MousePressed
		x = evt.getX();
		y = evt.getY();
	}// GEN-LAST:event_jPanel2MousePressed

	private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jPanel2MouseDragged
		Point mueve = MouseInfo.getPointerInfo().getLocation();
		this.setLocation(mueve.x - x, mueve.y - y);
	}// GEN-LAST:event_jPanel2MouseDragged

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private rsbuttom.RSButtonMetro cinco;
	private rsbuttom.RSButtonMetro btnPromociones;
	private rsbuttom.RSButtonMetro btnClientes;
	private javax.swing.JButton btnDeslizar;
	private javax.swing.JLabel lblMenu;
	private javax.swing.JLabel lblNombreDeUsuario;
	private javax.swing.JPanel pnlContenedor;
	private javax.swing.JPanel pnlSuperior;
	private javax.swing.JPanel pnlTituloMenu;
	private javax.swing.JScrollPane scrollPanelCentral;
	private rsbuttom.RSButtonMetro ocho;
	private javax.swing.JPanel pnlCentral;
	private javax.swing.JPanel pnlMenu;
	private javax.swing.JPanel pnlPrincipal;
	private rsbuttom.RSButtonMetro seis;
	private rsbuttom.RSButtonMetro siete;
	private rsbuttom.RSButtonMetro btnServicios;
	private rsbuttom.RSButtonMetro btnTurnos;
	private GridBagConstraints gbc_pnlSuperior;
	private GridBagConstraints gbc_pnlCentral;

	public static PanelTurnosMain getPnlTurnos() {
		return pnlTurnos;
	}

	public static PanelServiciosMain getPnlServicios() {
		return pnlServicios;
	}

	public static void setPnlTurnos(PanelTurnosMain pnlTurnos) {
		Vista.pnlTurnos = pnlTurnos;
	}

	public static PanelClientesMain getPnlClientes() {
		return pnlClientes;
	}

	public static void setPnlClientes(PanelClientesMain pnlClientes) {
		Vista.pnlClientes = pnlClientes;
	}

}
