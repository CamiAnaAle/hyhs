package com.presentacion.vistas.panelesServicios;

import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JTabbedPane;

/**
 *
 * @author RojeruSan
 */
public class PanelServiciosMain extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static PanelAgregarServ pnlAgregar;
	private static PanelBuscarServicio pnlBuscar;
	private static PanelModificarServicio pnlModificar;
	
	public static PanelAgregarServ getPnlAgregar() {

		return pnlAgregar;
	}

	public static void setPnlAgregar(PanelAgregarServ pnlAgregar) {
		PanelServiciosMain.pnlAgregar = pnlAgregar;
	}

	public PanelServiciosMain() {
		initComponents();
	}
	
	public static PanelBuscarServicio getPnlBuscar() {
		return pnlBuscar;
	}

	public static PanelModificarServicio getPnlModificar() {
		return pnlModificar;
	}

	public static void setPnlBuscar(PanelBuscarServicio pnlBuscar) {
		PanelServiciosMain.pnlBuscar = pnlBuscar;
	}

	private void initComponents() {

		setBackground(new java.awt.Color(255, 255, 255));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 14));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 1015, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(104, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(85, Short.MAX_VALUE)));

		pnlAgregar = new PanelAgregarServ();
		pnlAgregar.setBackground(Color.WHITE);

		tabbedPane.addTab("AGREGAR", null, pnlAgregar, "Tab 2 ");

		 pnlModificar = new PanelModificarServicio();
		pnlModificar.setBackground(Color.WHITE);

		tabbedPane.addTab("MODIFICAR", null, pnlModificar, "Tab 1 ");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);

		pnlBuscar = new PanelBuscarServicio();

		tabbedPane.addTab("LISTAR", null, pnlBuscar, "Tab 3 ");

		this.setLayout(layout);
	}
}
