package com.presentacion.vistas.panelesTurno;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Canvas;

public class PanelTurno_Promocion extends JPanel{
	private JTextField txtIdPromocion;
	private JTextField txtHora;
	private JTextField txtHora1;
	private JTextField txtHora2;
	private JTextField txtHora3;
	private JTextField txtPrecio;
	
	public PanelTurno_Promocion()
	{
		setBackground(Color.WHITE);
		setLayout(null);
		setBounds(3, 3, 778, 176);
		
		Canvas canvas = new Canvas();
		canvas.setBackground(Color.LIGHT_GRAY);
		canvas.setBounds(26, 51, 496, 1);
		add(canvas);
		
		JLabel label = new JLabel("ID PROMOCIÓN");
		label.setBounds(26, 15, 122, 20);
		add(label);
		
		txtIdPromocion = new JTextField();
		txtIdPromocion.setColumns(10);
		txtIdPromocion.setBounds(163, 10, 146, 26);
		add(txtIdPromocion);
		
		JLabel label_1 = new JLabel("SERVICIO 1:");
		label_1.setBounds(26, 82, 122, 20);
		add(label_1);
		
		JLabel lblTiempo = new JLabel("TIEMPO:");
		lblTiempo.setBounds(630, 34, 77, 26);
		add(lblTiempo);
		
		txtHora = new JTextField();
		txtHora.setColumns(10);
		txtHora.setBounds(695, 31, 77, 26);
		add(txtHora);
		
		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(324, 8, 115, 29);
		add(btnAceptar);
		
		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.setBounds(454, 8, 93, 29);
		add(btnBuscar);
		
		JLabel label_3 = new JLabel("SERVICIO 2:");
		label_3.setBounds(26, 115, 122, 20);
		add(label_3);
		
		JLabel label_4 = new JLabel("SERVICIO 3:");
		label_4.setBounds(26, 150, 122, 20);
		add(label_4);
		
		JComboBox cmbServicio1 = new JComboBox();
		cmbServicio1.setBounds(151, 79, 193, 26);
		add(cmbServicio1);
		
		JComboBox cmbServicio2 = new JComboBox();
		cmbServicio2.setBounds(151, 144, 193, 26);
		add(cmbServicio2);
		
		JComboBox cmbServicio3 = new JComboBox();
		cmbServicio3.setBounds(151, 112, 193, 26);
		add(cmbServicio3);
		
		JLabel label_5 = new JLabel("EMPLEADOS:");
		label_5.setBounds(368, 55, 122, 20);
		add(label_5);
		
		JComboBox cmbEmpleados1 = new JComboBox();
		cmbEmpleados1.setBounds(359, 79, 193, 26);
		add(cmbEmpleados1);
		
		JComboBox cmbEmpleados2 = new JComboBox();
		cmbEmpleados2.setBounds(359, 112, 193, 26);
		add(cmbEmpleados2);
		
		JComboBox cmbEmpleados3 = new JComboBox();
		cmbEmpleados3.setBounds(359, 144, 193, 26);
		add(cmbEmpleados3);
		
		JButton btnGuardar = new JButton("GUARDAR");
		btnGuardar.setBounds(522, 242, 115, 29);
		add(btnGuardar);
		
		txtHora1 = new JTextField();
		txtHora1.setColumns(10);
		txtHora1.setBounds(567, 79, 93, 26);
		add(txtHora1);
		
		txtHora2 = new JTextField();
		txtHora2.setColumns(10);
		txtHora2.setBounds(567, 112, 93, 26);
		add(txtHora2);
		
		txtHora3 = new JTextField();
		txtHora3.setColumns(10);
		txtHora3.setBounds(567, 144, 93, 26);
		add(txtHora3);
		
		JLabel label_6 = new JLabel("PRECIO:");
		label_6.setBounds(628, 12, 122, 20);
		add(label_6);
		
		JLabel label_7 = new JLabel("HORA");
		label_7.setBounds(567, 55, 77, 20);
		add(label_7);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(695, 2, 77, 26);
		add(txtPrecio);
		
		JButton btnAgregarPromocion = new JButton("AGREGAR");
		btnAgregarPromocion.setBounds(663, 136, 115, 29);
		add(btnAgregarPromocion);
	}
}
