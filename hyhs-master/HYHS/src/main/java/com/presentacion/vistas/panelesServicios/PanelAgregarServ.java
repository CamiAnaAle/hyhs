package com.presentacion.vistas.panelesServicios;

import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;

import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;
import com.toedter.calendar.JDateChooser;

import validaciones.validaLetras;
import validaciones.validaNumeros;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PanelAgregarServ extends JPanel{
	
	private JTextField txtId;
	private JTextField txtDescripcion;
	private JTextField txtPrecio;
	private JTextField txtTiempoEstimado;
	
	
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = { "Descripcion", "Precio", "Tiempo Estimado" };
	private JButton btnRegistrar;
	

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}


	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}


	public  PanelAgregarServ() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(-12, 0, 1066, 514);
		add(panel);
		
		JLabel lblDescripcion = new JLabel("DESCRIPCION:");
		lblDescripcion.setBounds(62, 82, 122, 20);
		panel.add(lblDescripcion);
		
		JLabel lblPrecio = new JLabel("PRECIO:");
		lblPrecio.setBounds(62, 115, 111, 20);
		panel.add(lblPrecio);
		
		JLabel lblTiempoEstimado = new JLabel("TIEMPO ESTIMADO:");
		lblTiempoEstimado.setBounds(62, 151, 97, 20);
		panel.add(lblTiempoEstimado);
		
		
		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);

		txtDescripcion.addKeyListener(new validaLetras());
		txtDescripcion.setBounds(194, 79, 146, 26);
		panel.add(txtDescripcion);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(194, 112, 146, 26);
		panel.add(txtPrecio);
		
		txtTiempoEstimado = new JTextField();
		txtTiempoEstimado.setColumns(10);
		txtTiempoEstimado.setBounds(194, 148, 146, 26);
		panel.add(txtTiempoEstimado);
		
		JLabel lblIdCliente = new JLabel("ID SERVICIO:");
		lblIdCliente.setBounds(50, 34, 157, 20);
		panel.add(lblIdCliente);
		
		
		btnRegistrar = new JButton("REGISTRAR");
		btnRegistrar.setBackground(Color.WHITE);
		btnRegistrar.setBounds(194, 228, 146, 29);
		panel.add(btnRegistrar);
		
		modelPersonas = new DefaultTableModel(null, nombreColumnas);
	}


	public JTextField getTxtDescripcion() {
		return txtDescripcion;
	}


	public void setTxtDescripcion(JTextField txtDescripcion) {
		this.txtDescripcion = txtDescripcion;
	}


	public JTextField getTxtPrecio() {
		return txtPrecio;
	}


	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}


	public JTextField getTxtTiempoEstimado() {
		return txtTiempoEstimado;
	}


	public void setTxtApellido(JTextField txtTiempoEstimado) {
		this.txtTiempoEstimado = txtTiempoEstimado;
	}


	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}


	public void setModelPersonas(DefaultTableModel modelPersonas) {
		this.modelPersonas = modelPersonas;
	}


	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
}


