package com.presentacion.vistas.panelesTurno;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;

import com.modelo.clases.Sucursal;
import com.presentacion.vistas.CambiaPanel;
import com.toedter.calendar.JDateChooser;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.awt.event.ItemEvent;
import javax.swing.border.LineBorder;
import javax.swing.JTable;

public class PanelTurnoAgregar extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtNroCliente;
	private JTextField txtNombre;
	private JTextField txtDni;
	private JPanel pnlItemDetalle;
	private JTable table;
	private JButton btnLimpiarCliente;
	private static PanelTurno_Servicio pnlTurnoServicio;
	private PanelTurno_Promocion pnlTurnoPromocion;
	private PanelTurno_Producto pnlTurnoProducto;
	private JRadioButton rbServicio;
	private JRadioButton rbPromocion;
	private JRadioButton rbProducto;
	
	
	
	
	public JRadioButton getRbServicio() {
		return rbServicio;
	}

	public void setRbServicio(JRadioButton rbServicio) {
		this.rbServicio = rbServicio;
	}

	public JRadioButton getRbPromocion() {
		return rbPromocion;
	}

	public void setRbPromocion(JRadioButton rbPromocion) {
		this.rbPromocion = rbPromocion;
	}

	public JRadioButton getRbProducto() {
		return rbProducto;
	}

	public void setRbProducto(JRadioButton rbProducto) {
		this.rbProducto = rbProducto;
	}

	public JTextField getTxtNroCliente() {
		return txtNroCliente;
	}

	public void setTxtNroCliente(JTextField txtNroCliente) {
		this.txtNroCliente = txtNroCliente;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtDni() {
		return txtDni;
	}

	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}

	public JPanel getPnlItemDetalle() {
		return pnlItemDetalle;
	}

	public void setPnlItemDetalle(JPanel pnlItemDetalle) {
		this.pnlItemDetalle = pnlItemDetalle;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public PanelTurno_Servicio getPnlTurnoServicio() {
		return pnlTurnoServicio;
	}

	public void setPnlTurnoServicio(PanelTurno_Servicio pnlTurnoServicio) {
		PanelTurnoAgregar.pnlTurnoServicio = pnlTurnoServicio;
	}

	public PanelTurno_Promocion getPnlTurnoPromocion() {
		return pnlTurnoPromocion;
	}

	public void setPnlTurnoPromocion(PanelTurno_Promocion pnlTurnoPromocion) {
		this.pnlTurnoPromocion = pnlTurnoPromocion;
	}

	public PanelTurno_Producto getPnlTurnoProducto() {
		return pnlTurnoProducto;
	}

	public void setPnlTurnoProducto(PanelTurno_Producto pnlTurnoProducto) {
		this.pnlTurnoProducto = pnlTurnoProducto;
	}

	JButton btnAceptarCliente;
	JComboBox<Sucursal> cmbSucursal;
	JDateChooser dateFecha;
	JButton btnBuscarCliente;
	
	
	
	public PanelTurnoAgregar()
	{
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createTitledBorder("DATOS GENERALES"));
		panel.setBackground(Color.WHITE);
		panel.setBounds(15, 16, 889, 92);
		add(panel);
		
		JLabel lblSucursal = new JLabel("SUCURSAL:");
		lblSucursal.setBounds(15, 25, 247, 20);
		panel.add(lblSucursal);
		
		JLabel lblFecha = new JLabel("FECHA:");
		lblFecha.setBounds(15, 54, 247, 20);
		panel.add(lblFecha);
		
		cmbSucursal = new JComboBox<Sucursal>();
		cmbSucursal.setBounds(104, 22, 193, 26);
		panel.add(cmbSucursal);
		
		dateFecha = new JDateChooser();
		dateFecha.setBounds(104, 54, 193, 20);
		panel.add(dateFecha);
		
	/*
		dateFecha.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
			        @Override
			        public void propertyChange(PropertyChangeEvent e) {
			            if ("date".equals(e.getPropertyName())) {
			                System.out.println(e.getPropertyName()
			                    + ": " + (Date) e.getNewValue());
			            }
			        }
			    });
		*/
		JLabel label_2 = new JLabel("NRO CLIENTE");
		label_2.setBounds(312, 25, 122, 20);
		panel.add(label_2);
		
		txtNroCliente = new JTextField();
		txtNroCliente.setBounds(421, 22, 146, 26);
		panel.add(txtNroCliente);
		txtNroCliente.setColumns(10);
		
		JLabel lblNombreYApellido = new JLabel("NOMBRE");
		lblNombreYApellido.setBounds(312, 54, 159, 20);
		panel.add(lblNombreYApellido);
		
		btnAceptarCliente = new JButton("ACEPTAR");
		btnAceptarCliente.setBounds(573, 21, 107, 29);
		panel.add(btnAceptarCliente);
		
		btnBuscarCliente = new JButton("BUSCAR");
		btnBuscarCliente.setBounds(681, 21, 93, 29);
		panel.add(btnBuscarCliente);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(421, 51, 146, 26);
		panel.add(txtNombre);
		txtNombre.setEditable(false);
		txtNombre.setColumns(10);
		
		JLabel label_5 = new JLabel("DNI");
		label_5.setBounds(582, 54, 50, 20);
		panel.add(label_5);
		
		txtDni = new JTextField();
		txtDni.setBounds(618, 51, 146, 26);
		panel.add(txtDni);
		txtDni.setEditable(false);
		txtDni.setColumns(10);
		
		 btnLimpiarCliente = new JButton("LIMPIAR");
		btnLimpiarCliente.setBounds(774, 21, 108, 29);
		panel.add(btnLimpiarCliente);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(BorderFactory.createTitledBorder("DETALLE"));
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(15, 106, 889, 255);
		add(panel_2);
		
		JLabel label_6 = new JLabel("SELECCIONE UNA OPCION:");
		label_6.setBounds(28, 22, 214, 20);
		panel_2.add(label_6);
		
		
		rbServicio = new JRadioButton("SERVICIO");
		rbServicio.setSelected(true);
		rbServicio.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					
					 pnlItemDetalle.removeAll();
					 pnlItemDetalle.add(pnlTurnoServicio);
				     pnlItemDetalle.revalidate();
				     pnlItemDetalle.repaint();
					//new CambiaPanel(pnlItemDetalle, new PanelTurno_Servicio());
					
			    }
			    else if (e.getStateChange() == ItemEvent.DESELECTED) {
			        // Your deselected code here.
			    }
			}
		});
		rbServicio.setBackground(Color.WHITE);
		rbServicio.setBounds(255, 17, 118, 29);
		panel_2.add(rbServicio);
		pnlTurnoServicio = new PanelTurno_Servicio();
	//	pnlTurnoServicio.getCbHora().setBounds(565, 77, 169, 26);
		pnlTurnoPromocion = new PanelTurno_Promocion();
		pnlTurnoProducto = new PanelTurno_Producto();
		
		rbPromocion = new JRadioButton("PROMOCION");
		rbPromocion.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					 pnlItemDetalle.removeAll();
				        pnlItemDetalle.add(pnlTurnoPromocion);
				        pnlItemDetalle.revalidate();
				        pnlItemDetalle.repaint();
					//new CambiaPanel(pnlItemDetalle, new PanelTurno_Promocion());
			    }
			    else if (e.getStateChange() == ItemEvent.DESELECTED) {
			        // Your deselected code here.
			    }
			}
		});
		rbPromocion.setBackground(Color.WHITE);
		rbPromocion.setBounds(395, 17, 150, 29);
		panel_2.add(rbPromocion);
	
		
		rbProducto = new JRadioButton("PRODUCTO");
		rbProducto.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					
			        pnlItemDetalle.removeAll();
			        pnlItemDetalle.add(pnlTurnoProducto);
			        pnlItemDetalle.revalidate();
			        pnlItemDetalle.repaint();
			    }
			    else if (e.getStateChange() == ItemEvent.DESELECTED) {
			        // Your deselected code here.
			    }
			}
		});
		rbProducto.setBackground(Color.WHITE);
		rbProducto.setBounds(552, 17, 150, 29);
		panel_2.add(rbProducto);
		
		pnlItemDetalle = new JPanel();
		pnlItemDetalle.setBorder(new LineBorder(Color.LIGHT_GRAY));
		pnlItemDetalle.setLayout(null);
		pnlItemDetalle.setBackground(Color.WHITE);
		pnlItemDetalle.setBounds(35, 51, 840, 188);
		
		panel_2.add(pnlItemDetalle);
		new CambiaPanel(pnlItemDetalle, new PanelTurno_Servicio());
		

		ButtonGroup grupo1 = new ButtonGroup();
		grupo1.add(rbProducto);
		grupo1.add(rbPromocion);
		grupo1.add(rbServicio);
		
		table = new JTable();
		table.setBackground(Color.LIGHT_GRAY);
		table.setBounds(25, 377, 854, 73);
		add(table);
		
		 pnlItemDetalle.removeAll();
		 pnlItemDetalle.add(pnlTurnoServicio);
	     pnlItemDetalle.revalidate();
	     pnlItemDetalle.repaint();
	}

	public JButton getBtnAceptarCliente() {
		return btnAceptarCliente;
	}

	public void setBtnAceptarCliente(JButton btnAceptar) {
		this.btnAceptarCliente = btnAceptar;
	}

	public JComboBox<Sucursal> getCmbSucursal() {
		return cmbSucursal;
	}

	public void setCmbSucursal(JComboBox<Sucursal> cmbSucursal) {
		this.cmbSucursal = cmbSucursal;
	}

	public JDateChooser getDateFecha() {
		return dateFecha;
	}

	public void setDateFecha(JDateChooser dateFecha) {
		this.dateFecha = dateFecha;
	}

	public JButton getBtnBuscarCliente() {
		return btnBuscarCliente;
	}

	public void setBtnBuscarCliente(JButton btnBuscarCliente) {
		this.btnBuscarCliente = btnBuscarCliente;
	}

	public JButton getBtnLimpiarCliente() {
		return btnLimpiarCliente;
	}

	public void setBtnLimpiarCliente(JButton btnLimpiar) {
		this.btnLimpiarCliente = btnLimpiar;
	}

	
	
}
