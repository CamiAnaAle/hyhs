package com.presentacion.vistas.panelesEmpleado;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.toedter.calendar.JCalendar;

import rojeru_san.componentes.RSCalendar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;

public class PanelAsignarHorario extends JPanel{

	

	private JLabel lblLegajo;
	private JTextField txt_legajo;
	private JComboBox cmb_sucursal;
	private JComboBox cmb_dia;
	private JFormattedTextField txt_desde;
	private JFormattedTextField txt_hasta;
	private JButton btnRegistrar;

	public PanelAsignarHorario()
	{
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(-12, 0, 1066, 514);
		add(panel);
		
		JLabel lblLegajo = new JLabel("LEGAJO:");
		lblLegajo.setBounds(37, 40, 157, 20);
		panel.add(lblLegajo);
		
		
		txt_legajo = new JTextField();
		txt_legajo.setColumns(10);
		txt_legajo.setBounds(105, 40, 73, 20);
		panel.add(txt_legajo);
		
		
		JLabel lblSucursal = new JLabel("SUCURSAL:");
		lblSucursal.setBounds(37, 80, 157, 20);
		panel.add(lblSucursal);
		
		cmb_sucursal = new JComboBox();
		cmb_sucursal.setBounds(105, 80, 125, 24);
		panel.add(cmb_sucursal);
		
		JLabel lblDia = new JLabel("DIA:");
		lblDia.setBounds(37, 120, 157, 20);
		panel.add(lblDia);
		
		cmb_dia = new JComboBox();
		cmb_dia.setBounds(105, 120, 125, 24);
		panel.add(cmb_dia);
	
		JLabel lblDesde = new JLabel("DESDE:");
		lblDesde.setBounds(37, 160, 157, 20);
		panel.add(lblDesde);
		Format shorTime=DateFormat.getTimeInstance(DateFormat.SHORT);
		
		txt_desde = new JFormattedTextField(shorTime);
		txt_desde.setColumns(10);
		txt_desde.setBounds(105, 160, 73, 20);
		txt_desde.setValue(new Date());
		panel.add(txt_desde);
		
		
		
		JLabel lblHasta = new JLabel("HASTA:");
		lblHasta.setBounds(200, 160, 157, 20);
		panel.add(lblHasta);
		
		
		txt_hasta = new JFormattedTextField(shorTime);
		txt_hasta.setColumns(10);
		txt_hasta.setValue(new Date());
		txt_hasta.setBounds(278, 160, 73, 20);
		panel.add(txt_hasta);
		
		

		btnRegistrar = new JButton("ASIGNAR");
		btnRegistrar.setBackground(Color.WHITE);
		btnRegistrar.setBounds(37, 200, 107, 20);
		panel.add(btnRegistrar);
		
	}

	public JLabel getLblLegajo() {
		return lblLegajo;
	}

	public void setLblLegajo(JLabel lblLegajo) {
		this.lblLegajo = lblLegajo;
	}

	public JTextField getTxt_legajo() {
		return txt_legajo;
	}

	public void setTxt_legajo(JTextField txt_legajo) {
		this.txt_legajo = txt_legajo;
	}

	public JComboBox getCmb_sucursal() {
		return cmb_sucursal;
	}

	public void setCmb_sucursal(JComboBox cmb_sucursal) {
		this.cmb_sucursal = cmb_sucursal;
	}

	public JComboBox getCmb_dia() {
		return cmb_dia;
	}

	public void setCmb_dia(JComboBox cmb_dia) {
		this.cmb_dia = cmb_dia;
	}

	public JFormattedTextField getTxt_desde() {
		return txt_desde;
	}

	public void setTxt_desde(JFormattedTextField txt_desde) {
		this.txt_desde = txt_desde;
	}

	public JFormattedTextField getTxt_hasta() {
		return txt_hasta;
	}

	public void setTxt_hasta(JFormattedTextField txt_hasta) {
		this.txt_hasta = txt_hasta;
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}
}

