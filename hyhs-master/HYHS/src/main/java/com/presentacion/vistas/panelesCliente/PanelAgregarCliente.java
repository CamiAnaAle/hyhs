package com.presentacion.vistas.panelesCliente;

import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;

import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;
import com.toedter.calendar.JDateChooser;

import validaciones.validaLetras;
import validaciones.validaNumeros;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PanelAgregarCliente extends JPanel{
	
	private JTextField txtDNI;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCP;
	private JTextField txtDireccion;
	private JTextField txtTelefono;
	private JTextField txtCelular;
	private JTextField txtEmail;
	private JDateChooser dateNacimiento;

	private JComboBox cmbLocalidad;
	private JComboBox<Provincia> cmbProvincia;
	private JComboBox cmbTipoCliente;
	private JComboBox<Pais> cmbPais;
	private JLabel lblIdCliente;
	
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = { "Nombre", "Apellido", "DNI", "Telefono", "Mail", "Deuda", "Localidad", "Tipo" };
	private JButton btnRegistrar;
	
	
	
	
	public JDateChooser getDateNacimiento() {
		return dateNacimiento;
	}


	public void setDateNacimiento(JDateChooser dateNacimiento) {
		this.dateNacimiento = dateNacimiento;
	}


	public JComboBox<Pais> getCmbPais() {
		return cmbPais;
	}


	public void setCmbPais(JComboBox<Pais> cmbPais) {
		this.cmbPais = cmbPais;
	}



	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}


	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}

	public JComboBox getCmbTipoCliente() {
		return cmbTipoCliente;
	}


	public void setCmbTipoCliente(JComboBox cmbTipoCliente) {
		this.cmbTipoCliente = cmbTipoCliente;
	}

	public PanelAgregarCliente() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(-12, 0, 1066, 514);
		add(panel);
		
		JLabel lblNacimiento = new JLabel("F. NACIMIENTO");
		lblNacimiento.setBounds(68, 190, 122, 20);
		panel.add(lblNacimiento);
		
		JLabel lblApellido = new JLabel("APELLIDO");
		lblApellido.setBounds(68, 154, 111, 20);
		panel.add(lblApellido);
		
		JLabel lblNombre = new JLabel("NOMBRE");
		lblNombre.setBounds(68, 118, 73, 20);
		panel.add(lblNombre);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setBounds(68, 82, 73, 20);
		panel.add(lblDNI);
		
		txtDNI = new JTextField();
		txtDNI.setColumns(10);
		txtDNI.setBounds(194, 79, 146, 26);
		panel.add(txtDNI);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(194, 112, 146, 26);
		panel.add(txtNombre);
		
		txtApellido = new JTextField();
		txtApellido.addKeyListener(new validaLetras());
		txtApellido.setColumns(10);
		txtApellido.setBounds(194, 148, 146, 26);
		panel.add(txtApellido);
		
		JLabel lblCP = new JLabel("CP:");
		lblCP.setBounds(105, 389, 54, 20);
		panel.add(lblCP);
		
		JLabel lblDireccion = new JLabel("DIRECCION");
		lblDireccion.setBounds(552, 79, 90, 20);
		panel.add(lblDireccion);
		
		JLabel lblTelefono = new JLabel("TELEFONO");
		lblTelefono.setBounds(552, 118, 90, 20);
		panel.add(lblTelefono);
		
		JLabel lblCelular = new JLabel("CELULAR");
		lblCelular.setBounds(552, 154, 98, 20);
		panel.add(lblCelular);
		
		JLabel lblEmail = new JLabel("EMAIL");
		lblEmail.setBounds(552, 190, 65, 20);
		panel.add(lblEmail);
		
		txtCP = new JTextField();
		txtCP.setEditable(false);
		txtCP.setColumns(10);
		txtCP.setBounds(194, 378, 146, 26);
		panel.add(txtCP);
		
		txtDireccion = new JTextField();
		txtDireccion.setColumns(10);
		txtDireccion.setBounds(664, 76, 146, 26);
		panel.add(txtDireccion);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(664, 112, 146, 26);
		panel.add(txtTelefono);
		
		txtCelular = new JTextField();
		txtCelular.setColumns(10);
		txtCelular.setBounds(664, 151, 146, 26);
		panel.add(txtCelular);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(664, 187, 146, 26);
		panel.add(txtEmail);
		
		
		
		btnRegistrar = new JButton("REGISTRAR");
		btnRegistrar.setBackground(Color.WHITE);
		btnRegistrar.setBounds(674, 246, 133, 29);
		panel.add(btnRegistrar);
		
		JLabel lblTipoCliente = new JLabel("TIPO CLIENTE");
		lblTipoCliente.setBounds(68, 229, 122, 20);
		panel.add(lblTipoCliente);
		
		cmbTipoCliente = new JComboBox();
		cmbTipoCliente.setBounds(194, 223, 136, 26);
		panel.add(cmbTipoCliente);
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(891, 226, 1, 1);
		panel.add(layeredPane);
		
		dateNacimiento = new JDateChooser();
		dateNacimiento.setBounds(194, 190, 136, 20);
		panel.add(dateNacimiento);
		
		JLabel lblLocalidad = new JLabel("LOCALIDAD");
		lblLocalidad.setBounds(68, 348, 122, 14);
		panel.add(lblLocalidad);
		
		JLabel lblProvincia = new JLabel("PROVINCIA");
		lblProvincia.setBounds(68, 309, 122, 14);
		panel.add(lblProvincia);
		
		cmbLocalidad = new JComboBox();
		cmbLocalidad.setBounds(194, 335, 136, 27);
		panel.add(cmbLocalidad);
		
		cmbProvincia = new JComboBox<Provincia>();
		cmbProvincia.setBounds(194, 299, 136, 26);
		panel.add(cmbProvincia);
		
		cmbProvincia.addItem(new Provincia(0l,"",null));
		
		JLabel lblIdCliente = new JLabel("ID CLIENTE:");
		lblIdCliente.setBounds(37, 40, 157, 20);
		panel.add(lblIdCliente);
		
		this.lblIdCliente = new JLabel("");
		this.lblIdCliente.setBounds(105, 40, 73, 20);
		panel.add(this.lblIdCliente);
		
		JLabel lblNewLabel = new JLabel("(*)");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(37, 82, 69, 20);
		panel.add(lblNewLabel);
		
		JLabel label_1 = new JLabel("(*)");
		label_1.setForeground(Color.RED);
		label_1.setBounds(37, 118, 69, 20);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("(*)");
		label_2.setForeground(Color.RED);
		label_2.setBounds(37, 151, 69, 20);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("(*)");
		label_3.setForeground(Color.RED);
		label_3.setBounds(37, 223, 69, 20);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("(*)");
		label_4.setForeground(Color.RED);
		label_4.setBounds(37, 306, 69, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("(*)");
		label_5.setForeground(Color.RED);
		label_5.setBounds(513, 82, 69, 20);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("(*)");
		label_6.setForeground(Color.RED);
		label_6.setBounds(513, 118, 69, 20);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setForeground(Color.RED);
		label_7.setBounds(513, 190, 69, 20);
		panel.add(label_7);
		
		JLabel msjDni = new JLabel("Valor numérico");
		msjDni.setForeground(Color.RED);
		msjDni.setBounds(353, 82, 111, 20);
		panel.add(msjDni);
		
		JLabel msjNombre = new JLabel("Solo texto");
		msjNombre.setForeground(Color.RED);
		msjNombre.setBounds(355, 118, 111, 20);
		panel.add(msjNombre);
		
		JLabel msjApellido = new JLabel("Solo texto");
		msjApellido.setForeground(Color.RED);
		msjApellido.setBounds(353, 154, 111, 20);
		panel.add(msjApellido);
		
		JLabel msjTelefono = new JLabel("Valor numérico");
		msjTelefono.setForeground(Color.RED);
		msjTelefono.setBounds(841, 115, 216, 20);
		panel.add(msjTelefono);
		
		JLabel msjEmail = new JLabel("Ingrese email válido");
		msjEmail.setForeground(Color.RED);
		msjEmail.setBounds(841, 190, 151, 20);
		panel.add(msjEmail);
		
		JLabel lblCamposObligatorio = new JLabel("(*) Campos obligatorios");
		lblCamposObligatorio.setForeground(Color.RED);
		lblCamposObligatorio.setBounds(636, 478, 242, 20);
		panel.add(lblCamposObligatorio);
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setBounds(37, 345, 69, 20);
		panel.add(label);
		
		JLabel lblPais = new JLabel("PAIS");
		lblPais.setBounds(68, 265, 122, 20);
		panel.add(lblPais);
		
		cmbPais = new JComboBox<Pais>();
		
		cmbPais.setBounds(194, 265, 136, 26);
		cmbPais.addItem(new Pais(0,""));
		panel.add(cmbPais);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setBounds(37, 265, 69, 20);
		panel.add(label_8);
		
		modelPersonas = new DefaultTableModel(null, nombreColumnas);
	}


	public JTextField getTxtDNI() {
		return txtDNI;
	}


	public void setTxtDNI(JTextField txtDNI) {
		this.txtDNI = txtDNI;
	}


	public JTextField getTxtNombre() {
		return txtNombre;
	}


	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}


	public JTextField getTxtApellido() {
		return txtApellido;
	}


	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}


	public JLabel getlblIdCliente() {
		return lblIdCliente;
	}
	public JTextField getTxtCP() {
		return txtCP;
	}


	public void setTxtCP(JTextField txtCP) {
		this.txtCP = txtCP;
	}


	public JTextField getTxtDireccion() {
		return txtDireccion;
	}


	public void setTxtDireccion(JTextField txtDireccion) {
		this.txtDireccion = txtDireccion;
	}


	public JTextField getTxtTelefono() {
		return txtTelefono;
	}


	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}


	public JTextField getTxtCelular() {
		return txtCelular;
	}


	public void setTxtCelular(JTextField txtCelular) {
		this.txtCelular = txtCelular;
	}


	public JTextField getTxtEmail() {
		return txtEmail;
	}


	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}


	public JComboBox getCmbLocalidad() {
		return cmbLocalidad;
	}


	public void setCmbLocalidad(JComboBox cmbLocalidad) {
		this.cmbLocalidad = cmbLocalidad;
	}


	public JComboBox getCmbProvincia() {
		return cmbProvincia;
	}


	public void setCmbProvincia(JComboBox cmbProvincia) {
		this.cmbProvincia = cmbProvincia;
	}


	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}


	public void setModelPersonas(DefaultTableModel modelPersonas) {
		this.modelPersonas = modelPersonas;
	}


	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
}

