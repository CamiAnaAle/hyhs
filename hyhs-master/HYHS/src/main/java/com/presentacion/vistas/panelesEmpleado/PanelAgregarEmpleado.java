package com.presentacion.vistas.panelesEmpleado;

import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;

import com.modelo.clases.Pais;
import com.modelo.clases.Provincia;
import com.toedter.calendar.JDateChooser;

import validaciones.validaLetras;
import validaciones.validaNumeros;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;



public class PanelAgregarEmpleado extends JPanel{
	private JTextField txt_dni;
	private JTextField txt_nombre;
	private JTextField txt_apellido;
	private JTextField txt_cp;
	private JDateChooser dateNacimiento;
	private JTextField txt_direccion;
	private JTextField txt_email;
	private JTextField txt_telefono;
	private JDateChooser fechaIngreso;
	private JTextField txt_celular;
	private JTextField txt_sueldoInicial;
	private JComboBox cmb_pais;
	private JComboBox cmb_provincia;
	private JComboBox cmb_localidad;
	private JButton btn_modificar;
	private JButton btn_Buscar;
	
	private JLabel lblLegajo;
	
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = { "Nombre", "Apellido", "DNI", "Telefono", "Mail", "Deuda", "Localidad", "Tipo" };
	private JButton btnRegistrar;
	
	
	
	
	public JDateChooser getDateNacimiento() {
		return dateNacimiento;
	}


	public void setDateNacimiento(JDateChooser dateNacimiento) {
		this.dateNacimiento = dateNacimiento;
	}


	public JComboBox<Pais> getCmbPais() {
		return cmb_pais;
	}


	public void setCmbPais(JComboBox<Pais> cmbPais) {
		this.cmb_pais = cmbPais;
	}



	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}


	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}



	public PanelAgregarEmpleado() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(-12, 0, 1066, 514);
		add(panel);
		
		JLabel lblNacimiento = new JLabel("F. NACIMIENTO");
		lblNacimiento.setBounds(68, 190, 122, 20);
		panel.add(lblNacimiento);
		
		JLabel lblApellido = new JLabel("APELLIDO");
		lblApellido.setBounds(68, 154, 111, 20);
		panel.add(lblApellido);
		
		JLabel lblNombre = new JLabel("NOMBRE");
		lblNombre.setBounds(68, 118, 73, 20);
		panel.add(lblNombre);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setBounds(68, 82, 73, 20);
		panel.add(lblDNI);
		
		txt_dni = new JTextField();
		txt_dni.setColumns(10);
		txt_dni.setBounds(194, 79, 146, 26);
		panel.add(txt_dni);
		
		txt_nombre = new JTextField();
		txt_nombre.setColumns(10);
		txt_nombre.setBounds(194, 112, 146, 26);
		panel.add(txt_nombre);
		
		txt_apellido = new JTextField();
		txt_apellido.addKeyListener(new validaLetras());
		txt_apellido.setColumns(10);
		txt_apellido.setBounds(194, 148, 146, 26);
		panel.add(txt_apellido);
		
		JLabel lblCP = new JLabel("CP:");
		lblCP.setBounds(105, 389, 54, 20);
		panel.add(lblCP);
		
		JLabel lblDireccion = new JLabel("DIRECCION");
		lblDireccion.setBounds(552, 79, 90, 20);
		panel.add(lblDireccion);
		
		JLabel lblTelefono = new JLabel("TELEFONO");
		lblTelefono.setBounds(552, 118, 90, 20);
		panel.add(lblTelefono);
		
		JLabel lblCelular = new JLabel("CELULAR");
		lblCelular.setBounds(552, 154, 98, 20);
		panel.add(lblCelular);
		
		JLabel lblEmail = new JLabel("EMAIL");
		lblEmail.setBounds(552, 190, 65, 20);
		panel.add(lblEmail);
		
		JLabel lblSueldoInicial= new JLabel("SUELDO INICIAL");
		lblSueldoInicial.setBounds(552, 220, 80, 20);
		panel.add(lblSueldoInicial);
		
		JLabel lblFechaIngreso= new JLabel("F. INGRESO");
		lblFechaIngreso.setBounds(552, 270, 80, 20);
		panel.add(lblFechaIngreso);
		
		
		txt_cp = new JTextField();
		txt_cp.setEditable(false);
		txt_cp.setColumns(10);
		txt_cp.setBounds(194, 378, 146, 26);
		panel.add(txt_cp);
		
		txt_direccion = new JTextField();
		txt_direccion.setColumns(10);
		txt_direccion.setBounds(664, 76, 146, 26);
		panel.add(txt_direccion);
		
		txt_telefono = new JTextField();
		txt_telefono.setColumns(10);
		txt_telefono.setBounds(664, 112, 146, 26);
		panel.add(txt_telefono);
		
		txt_celular = new JTextField();
		txt_celular.setColumns(10);
		txt_celular.setBounds(664, 151, 146, 26);
		panel.add(txt_celular);
		
		txt_email = new JTextField();
		txt_email.setColumns(10);
		txt_email.setBounds(664, 187, 146, 26);
		panel.add(txt_email);
		
		
		txt_sueldoInicial = new JTextField();
		txt_sueldoInicial.setColumns(10);
		txt_sueldoInicial.setBounds(664, 220, 146, 26);
		panel.add(txt_sueldoInicial);
		
		dateNacimiento = new JDateChooser();
		dateNacimiento.setBounds(664, 270, 136, 26);
		panel.add(dateNacimiento);
		
		btnRegistrar = new JButton("REGISTRAR");
		btnRegistrar.setBackground(Color.WHITE);
		btnRegistrar.setBounds(674, 350, 133, 29);
		panel.add(btnRegistrar);
		
		
		
		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(891, 226, 1, 1);
		panel.add(layeredPane);
		
		dateNacimiento = new JDateChooser();
		dateNacimiento.setBounds(194, 190, 136, 20);
		panel.add(dateNacimiento);
		
		JLabel lblLocalidad = new JLabel("LOCALIDAD");
		lblLocalidad.setBounds(68, 348, 122, 14);
		panel.add(lblLocalidad);
		
		JLabel lblProvincia = new JLabel("PROVINCIA");
		lblProvincia.setBounds(68, 309, 122, 14);
		panel.add(lblProvincia);
		
		cmb_localidad = new JComboBox();
		cmb_localidad.setBounds(194, 335, 136, 27);
		panel.add(cmb_localidad);
		
		cmb_provincia = new JComboBox<Provincia>();
		cmb_provincia.setBounds(194, 299, 136, 26);
		panel.add(cmb_provincia);
		
		cmb_provincia.addItem(new Provincia(0l,"",null));
		
		JLabel lblIdCliente = new JLabel("LEGAJO:");
		lblIdCliente.setBounds(37, 40, 157, 20);
		panel.add(lblIdCliente);
		
		
		this.lblLegajo = new JLabel("");
		this.lblLegajo.setBounds(105, 40, 73, 20);
		panel.add(this.lblLegajo);
		
		
		
		JLabel lblNewLabel = new JLabel("(*)");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(37, 82, 69, 20);
		panel.add(lblNewLabel);
		
		JLabel label_1 = new JLabel("(*)");
		label_1.setForeground(Color.RED);
		label_1.setBounds(37, 118, 69, 20);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("(*)");
		label_2.setForeground(Color.RED);
		label_2.setBounds(37, 151, 69, 20);
		panel.add(label_2);
		
		JLabel label_4 = new JLabel("(*)");
		label_4.setForeground(Color.RED);
		label_4.setBounds(37, 306, 69, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("(*)");
		label_5.setForeground(Color.RED);
		label_5.setBounds(513, 82, 69, 20);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("(*)");
		label_6.setForeground(Color.RED);
		label_6.setBounds(513, 118, 69, 20);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("(*)");
		label_7.setForeground(Color.RED);
		label_7.setBounds(513, 190, 69, 20);
		panel.add(label_7);
		
		JLabel msjDni = new JLabel("Valor numérico");
		msjDni.setForeground(Color.RED);
		msjDni.setBounds(353, 82, 111, 20);
		panel.add(msjDni);
		
		JLabel msjNombre = new JLabel("Solo texto");
		msjNombre.setForeground(Color.RED);
		msjNombre.setBounds(355, 118, 111, 20);
		panel.add(msjNombre);
		
		JLabel msjApellido = new JLabel("Solo texto");
		msjApellido.setForeground(Color.RED);
		msjApellido.setBounds(353, 154, 111, 20);
		panel.add(msjApellido);
		
		JLabel msjTelefono = new JLabel("Valor numérico");
		msjTelefono.setForeground(Color.RED);
		msjTelefono.setBounds(841, 115, 216, 20);
		panel.add(msjTelefono);
		
		JLabel msjEmail = new JLabel("Ingrese email válido");
		msjEmail.setForeground(Color.RED);
		msjEmail.setBounds(841, 190, 151, 20);
		panel.add(msjEmail);
		
		JLabel lblCamposObligatorio = new JLabel("(*) Campos obligatorios");
		lblCamposObligatorio.setForeground(Color.RED);
		lblCamposObligatorio.setBounds(636, 478, 242, 20);
		panel.add(lblCamposObligatorio);
		
		JLabel label = new JLabel("(*)");
		label.setForeground(Color.RED);
		label.setBounds(37, 345, 69, 20);
		panel.add(label);
		
		JLabel lblPais = new JLabel("PAIS");
		lblPais.setBounds(68, 265, 122, 20);
		panel.add(lblPais);
		
		cmb_pais = new JComboBox<Pais>();
		
		cmb_pais.setBounds(194, 265, 136, 26);
		cmb_pais.addItem(new Pais(0,""));
		panel.add(cmb_pais);
		
		JLabel label_8 = new JLabel("(*)");
		label_8.setForeground(Color.RED);
		label_8.setBounds(37, 265, 69, 20);
		panel.add(label_8);
		
		modelPersonas = new DefaultTableModel(null, nombreColumnas);
	}

	public JTextField getTxtDNI() {
		return txt_dni;
	}


	public void setTxtDNI(JTextField txtDNI) {
		this.txt_dni = txtDNI;
	}
	
	


	public JTextField getTxtNombre() {
		return txt_nombre;
	}


	public void setTxtNombre(JTextField txtNombre) {
		this.txt_nombre = txtNombre;
	}


	public JTextField getTxtApellido() {
		return txt_apellido;
	}


	public void setTxtApellido(JTextField txtApellido) {
		this.txt_apellido = txtApellido;
	}


	public JLabel getlblLegajo() {
		return lblLegajo;
	}
	public JTextField getTxtCP() {
		return txt_cp;
	}


	public void setTxtCP(JTextField txtCP) {
		this.txt_cp = txtCP;
	}


	public JTextField getTxtDireccion() {
		return txt_direccion;
	}


	public void setTxtDireccion(JTextField txtDireccion) {
		this.txt_direccion = txtDireccion;
	}


	public JTextField getTxtTelefono() {
		return txt_telefono;
	}


	public void setTxtTelefono(JTextField txtTelefono) {
		this.txt_telefono = txtTelefono;
	}


	public JTextField getTxtCelular() {
		return txt_celular;
	}


	public void setTxtCelular(JTextField txtCelular) {
		this.txt_celular = txtCelular;
	}


	public JTextField getTxtEmail() {
		return txt_email;
	}


	public void setTxtEmail(JTextField txtEmail) {
		this.txt_email = txtEmail;
	}


	public JComboBox getCmbLocalidad() {
		return cmb_localidad;
	}


	public void setCmbLocalidad(JComboBox cmbLocalidad) {
		this.cmb_localidad = cmbLocalidad;
	}


	public JComboBox getCmbProvincia() {
		return cmb_provincia;
	}


	public void setCmbProvincia(JComboBox cmbProvincia) {
		this.cmb_provincia = cmbProvincia;
	}


	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}


	public void setModelPersonas(DefaultTableModel modelPersonas) {
		this.modelPersonas = modelPersonas;
	}


	public String[] getNombreColumnas() {
		return nombreColumnas;
	}


	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}


	public JDateChooser getFechaIngreso() {
		return fechaIngreso;
	}


	public void setFechaIngreso(JDateChooser fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}


	public JTextField getTxt_sueldoInicial() {
		return txt_sueldoInicial;
	}


	public void setTxt_sueldoInicial(JTextField txt_sueldoInicial) {
		this.txt_sueldoInicial = txt_sueldoInicial;
	}
}

