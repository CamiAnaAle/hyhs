package com.presentacion.vistas.panelesCliente;

import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Color;

public class PanelModificarCliente extends JPanel {
	private JTextField txt_idcliente;
	private JTextField txt_dni;
	private JTextField txt_nombre;
	private JTextField txt_apellido;
	private JTextField txt_cp;
	private JDateChooser dateNacimiento;
	private JTextField txt_direccion;
	private JTextField txt_email;
	private JTextField txt_telefono;
	private JTextField txt_celular;
	private JTextField txt_totaladeudado;
	private JTextField txt_puntos;
	private JComboBox cmb_pais;

	private JComboBox cmb_provincia;

	private JComboBox cmb_tipocliente;

	private JComboBox cmb_estadocliente;
	private JComboBox cmb_localidad;
	private JButton btn_modificar;
	private JButton btn_Buscar;
	
	public PanelModificarCliente() {
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 967, 450);
		add(panel);
		
		JLabel label = new JLabel("F. NACIMIENTO");
		label.setBounds(25, 267, 122, 20);
		panel.add(label);

		dateNacimiento = new JDateChooser();
		dateNacimiento.setBounds(148, 264, 146, 26);
		panel.add(dateNacimiento);
		
		JLabel label_1 = new JLabel("APELLIDO");
		label_1.setBounds(25, 225, 111, 20);
		panel.add(label_1);
		
		JLabel label_2 = new JLabel("NOMBRE");
		label_2.setBounds(25, 177, 73, 20);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("DNI");
		label_3.setBounds(25, 124, 73, 20);
		panel.add(label_3);
		
		txt_idcliente = new JTextField();
		txt_idcliente.setColumns(10);
		txt_idcliente.setBounds(148, 31, 146, 26);
		panel.add(txt_idcliente);
		
		txt_dni = new JTextField();
		txt_dni.setColumns(10);
		txt_dni.setBounds(148, 121, 146, 26);
		panel.add(txt_dni);
		
		txt_nombre = new JTextField();
		txt_nombre.setColumns(10);
		txt_nombre.setBounds(148, 174, 146, 26);
		panel.add(txt_nombre);
		
		txt_apellido = new JTextField();
		txt_apellido.setColumns(10);
		txt_apellido.setBounds(148, 222, 146, 26);
		panel.add(txt_apellido);
		
		JLabel label_4 = new JLabel("CP:");
		label_4.setBounds(720, 124, 111, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("DIRECCION");
		label_5.setBounds(25, 313, 90, 20);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("TELEFONO");
		label_6.setBounds(337, 127, 90, 20);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("CELULAR");
		label_7.setBounds(337, 180, 98, 20);
		panel.add(label_7);
		
		JLabel label_8 = new JLabel("EMAIL");
		label_8.setBounds(50, 356, 65, 20);
		panel.add(label_8);
		
		txt_cp = new JTextField();
		txt_cp.setColumns(10);
		txt_cp.setBounds(770, 121, 146, 26);
		panel.add(txt_cp);
		
		txt_direccion = new JTextField();
		txt_direccion.setColumns(10);
		txt_direccion.setBounds(148, 306, 146, 26);
		panel.add(txt_direccion);
		
		txt_email = new JTextField();
		txt_email.setColumns(10);
		txt_email.setBounds(148, 353, 146, 26);
		panel.add(txt_email);
		
		txt_telefono = new JTextField();
		txt_telefono.setColumns(10);
		txt_telefono.setBounds(480, 121, 146, 26);
		panel.add(txt_telefono);
		
		txt_celular = new JTextField();
		txt_celular.setColumns(10);
		txt_celular.setBounds(483, 174, 146, 26);
		panel.add(txt_celular);
		
		 btn_Buscar = new JButton("BUSCAR");
		 btn_Buscar.setBounds(321, 30, 133, 29);
		panel.add(btn_Buscar);
		
		 btn_modificar = new JButton("MODIFICAR");
		btn_modificar.setBounds(684, 352, 133, 29);
		panel.add(btn_modificar);
		
		JLabel label_9 = new JLabel("ID CLIENTE:");
		label_9.setBounds(36, 34, 111, 20);
		panel.add(label_9);
		
		JLabel lblPais = new JLabel("PAIS");
		lblPais.setBounds(720, 170, 146, 26);
		panel.add(lblPais);
		
		JLabel lblProvincia = new JLabel("PROVINCIA");
		lblProvincia.setBounds(705, 222, 111, 20);
		panel.add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("LOCALIDAD");
		lblLocalidad.setBounds(705, 274, 111, 20);
		panel.add(lblLocalidad);
		
		cmb_pais = new JComboBox();
		cmb_pais.setBounds(770, 170, 146, 26);
		panel.add(cmb_pais);
		
		cmb_provincia = new JComboBox();
		cmb_provincia.setBounds(770, 222, 146, 26);
		panel.add(cmb_provincia);
		

		cmb_localidad = new JComboBox();
		cmb_localidad.setBounds(770, 274, 146, 26);
		panel.add(cmb_localidad);
		
		JLabel lblTotalAdeudado = new JLabel("TOTAL ADEUDADO");
		lblTotalAdeudado.setBounds(309, 225, 164, 20);
		panel.add(lblTotalAdeudado);
		
		JLabel label_10 = new JLabel("TIPO CLIENTE");
		label_10.setBounds(321, 267, 122, 20);
		panel.add(label_10);
		
		 cmb_tipocliente = new JComboBox();
		cmb_tipocliente.setBounds(480, 267, 136, 26);
		panel.add(cmb_tipocliente);
		
		txt_totaladeudado = new JTextField();
		txt_totaladeudado.setColumns(10);
		txt_totaladeudado.setBounds(480, 222, 146, 26);
		panel.add(txt_totaladeudado);
		
		JLabel lblPuntos = new JLabel("PUNTOS");
		lblPuntos.setBounds(332, 313, 122, 20);
		panel.add(lblPuntos);
		
		txt_puntos = new JTextField();
		txt_puntos.setColumns(10);
		txt_puntos.setBounds(480, 310, 146, 26);
		panel.add(txt_puntos);
		
		JLabel lblEstadoCliente = new JLabel("ESTADO CLIENTE");
		lblEstadoCliente.setBounds(337, 356, 164, 20);
		panel.add(lblEstadoCliente);
		
		 cmb_estadocliente = new JComboBox();
		cmb_estadocliente.setBounds(490, 353, 136, 26);
		panel.add(cmb_estadocliente);
	}
	
	
	public JTextField getTxt_idcliente() {
		return txt_idcliente;
	}
	public void setTxt_idcliente(JTextField txt_idcliente) {
		this.txt_idcliente = txt_idcliente;
	}
	public JTextField getTxt_dni() {
		return txt_dni;
	}
	public void setTxt_dni(JTextField txt_dni) {
		this.txt_dni = txt_dni;
	}
	public JTextField getTxt_nombre() {
		return txt_nombre;
	}
	public void setTxt_nombre(JTextField txt_nombre) {
		this.txt_nombre = txt_nombre;
	}
	public JTextField getTxt_apellido() {
		return txt_apellido;
	}
	public void setTxt_apellido(JTextField txt_apellido) {
		this.txt_apellido = txt_apellido;
	}
	public JTextField getTxt_cp() {
		return txt_cp;
	}
	public void setTxt_cp(JTextField txt_cp) {
		this.txt_cp = txt_cp;
	}
	public JTextField getTxt_direccion() {
		return txt_direccion;
	}
	public void setTxt_direccion(JTextField txt_direccion) {
		this.txt_direccion = txt_direccion;
	}
	public JTextField getTxt_email() {
		return txt_email;
	}
	public void setTxt_email(JTextField txt_email) {
		this.txt_email = txt_email;
	}
	public JTextField getTxt_telefono() {
		return txt_telefono;
	}
	public void setTxt_telefono(JTextField txt_telefono) {
		this.txt_telefono = txt_telefono;
	}
	public JTextField getTxt_celular() {
		return txt_celular;
	}
	public void setTxt_celular(JTextField txt_celular) {
		this.txt_celular = txt_celular;
	}
	public JTextField getTxt_totaladeudado() {
		return txt_totaladeudado;
	}
	public void setTxt_totaladeudado(JTextField txt_totaladeudado) {
		this.txt_totaladeudado = txt_totaladeudado;
	}
	public JTextField getTxt_puntos() {
		return txt_puntos;
	}
	public void setTxt_puntos(JTextField txt_puntos) {
		this.txt_puntos = txt_puntos;
	}
	
	
	
	public JDateChooser getDateNacimiento() {
		return dateNacimiento;
	}


	public void setDateNacimiento(JDateChooser dateNacimiento) {
		this.dateNacimiento = dateNacimiento;
	}


	public JComboBox getCmb_pais() {
		return cmb_pais;
	}


	public void setCmb_pais(JComboBox cmb_pais) {
		this.cmb_pais = cmb_pais;
	}


	public JComboBox getCmb_provincia() {
		return cmb_provincia;
	}


	public void setCmb_provincia(JComboBox cmb_provincia) {
		this.cmb_provincia = cmb_provincia;
	}


	public JComboBox getCmb_tipocliente() {
		return cmb_tipocliente;
	}


	public void setCmb_tipocliente(JComboBox cmb_tipocliente) {
		this.cmb_tipocliente = cmb_tipocliente;
	}


	public JComboBox getCmb_estadocliente() {
		return cmb_estadocliente;
	}


	public void setCmb_estadocliente(JComboBox cmb_estadocliente) {
		this.cmb_estadocliente = cmb_estadocliente;
	}


	public JComboBox getCmb_localidad() {
		return cmb_localidad;
	}


	public void setCmb_localidad(JComboBox cmb_localidad) {
		this.cmb_localidad = cmb_localidad;
	}


	public JButton getBtn_buscar() {
		return btn_Buscar;
	}


	public void setBtn_buscar(JButton btn_buscar) {
		this.btn_Buscar = btn_buscar;
	}


	public JButton getBtn_modificar() {
		return btn_modificar;
	}


	public void setBtn_modificar(JButton btn_modificar) {
		this.btn_modificar = btn_modificar;
	}
}
