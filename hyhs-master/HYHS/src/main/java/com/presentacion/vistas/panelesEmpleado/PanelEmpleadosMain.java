package com.presentacion.vistas.panelesEmpleado;

import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JTabbedPane;

/**
 *
 * @author RojeruSan
 */
public class PanelEmpleadosMain extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static PanelAgregarEmpleado pnlAgregar;
	private static PanelBuscarEmpleado pnlBuscar;
	private static PanelModificarEmpleado pnlModificar;
	private static PanelAsignarHorario pnlAsignarHorario;
	
	public static PanelAgregarEmpleado getPnlAgregar() {

		return pnlAgregar;
	}

	public static void setPnlAgregar(PanelAgregarEmpleado pnlAgregar) {
		PanelEmpleadosMain.pnlAgregar = pnlAgregar;
	}


	public PanelEmpleadosMain() {
		initComponents();
	}
	
	public static PanelBuscarEmpleado getPnlBuscar() {
		return pnlBuscar;
	}
	
	public static PanelAsignarHorario getPnlAsignarHorario() {
		return pnlAsignarHorario;
	}
	public static PanelModificarEmpleado getPnlModificar() {
		return pnlModificar;
	}


	public static void setPnlBuscar(PanelBuscarEmpleado pnlBuscar) {
		PanelEmpleadosMain.pnlBuscar = pnlBuscar;
	}

	private void initComponents() {

		setBackground(new java.awt.Color(255, 255, 255));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 14));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 1015, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(104, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(85, Short.MAX_VALUE)));

		pnlAgregar = new PanelAgregarEmpleado();
		pnlAgregar.setBackground(Color.WHITE);

		tabbedPane.addTab("AGREGAR", null, pnlAgregar, "Tab 2 ");

		pnlModificar = new PanelModificarEmpleado();
		pnlModificar.setBackground(Color.WHITE);
		
		

		tabbedPane.addTab("MODIFICAR", null, pnlModificar, "Tab 1 ");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);

		pnlBuscar = new PanelBuscarEmpleado();

		tabbedPane.addTab("LISTAR", null, pnlBuscar, "Tab 3 ");


		

		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_1);

		pnlAsignarHorario = new PanelAsignarHorario();
		tabbedPane.addTab("ASIGNAR HORARIO", null, pnlAsignarHorario, "Tab 3 ");


		this.setLayout(layout);
	}
}
