package com.presentacion.vistas.panelesTurno;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import rojeru_san.componentes.RSCalendar;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;
import java.awt.SystemColor;

public class PanelTurnosConsultar extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable tablaTurnos;
	private JButton btnModificar;
	private JButton btnActualizar;
	private RSCalendar calendar;
	private DefaultTableModel modeloTurnos;
	private String[] nombreColumnas = {"Numero", "Nombre", "Apellido", "Duracion", "Sucursal", "Estado"};

	/**
	 * Create the panel.
	 */
	public PanelTurnosConsultar() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		calendar = new RSCalendar();
		calendar.setBounds(10, 63, 312, 333);
		add(calendar);
		
		JLabel lblSeleccioneUnaFecha = new JLabel("Seleccione una Fecha");
		lblSeleccioneUnaFecha.setForeground(Color.BLUE);
		lblSeleccioneUnaFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccioneUnaFecha.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		lblSeleccioneUnaFecha.setBounds(10, 11, 312, 41);
		add(lblSeleccioneUnaFecha);
		
		modeloTurnos = new DefaultTableModel(null, nombreColumnas);
		
		tablaTurnos = new JTable(modeloTurnos);
		tablaTurnos.setBounds(379, 237, 485, 159);
		add(tablaTurnos);
		
		JScrollPane scroll = new JScrollPane(tablaTurnos,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(379, 237, 485, 159);
		add(scroll);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBackground(Color.BLUE);
		btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnActualizar.setForeground(SystemColor.desktop);
		btnActualizar.setBounds(379, 63, 104, 41);
		add(btnActualizar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBackground(Color.BLUE);
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnModificar.setForeground(SystemColor.desktop);
		btnModificar.setBounds(379, 400, 104, 41);
		add(btnModificar);
		
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public RSCalendar getCalendar() {
		return calendar;
	}

	public void setCalendar(RSCalendar calendar) {
		this.calendar = calendar;
	}

	public JTable getTablaTurnos() {
		return tablaTurnos;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}

	public DefaultTableModel getModeloTurnos() {
		return modeloTurnos;
	}

	public void setModeloTurnos(DefaultTableModel modeloTurnos) {
		this.modeloTurnos = modeloTurnos;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}	
}
