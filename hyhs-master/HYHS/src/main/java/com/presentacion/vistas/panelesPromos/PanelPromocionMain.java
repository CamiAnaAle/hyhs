package com.presentacion.vistas.panelesPromos;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

public class PanelPromocionMain extends JPanel {

	private static final long serialVersionUID = 1L;
	/**
	 * Create the panel.
	 */
	private JButton btnObtener;
	private JTable tablaPromociones;
	private DefaultTableModel modeloTablaPromos;
	private String[] nombreColumnasPromos = { "Numero", "Descripcion", "Precio", "Puntos", "Desde", "Hasta", "Estado",
			"Tipo" };

	private ButtonGroup group;
	private JRadioButton rdbtnVigentes;
	private JRadioButton rdbtnInactivas;
	private JRadioButton rdbtnTodas;

	private JTable tablaDetalles;
	private DefaultTableModel modeloTablaDetalles;
	private String[] nombreColumnasDetalle = { "ID", "Servicio", "Precio" };

	public PanelPromocionMain() {
		setBackground(Color.WHITE);
		setLayout(null);

		JLabel lblPromociones = new JLabel("Promociones");
		lblPromociones.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromociones.setForeground(Color.BLUE);
		lblPromociones.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblPromociones.setBounds(10, 11, 225, 29);
		add(lblPromociones);

		modeloTablaPromos = new DefaultTableModel(null, nombreColumnasPromos);
		tablaPromociones = new JTable(modeloTablaPromos);
		tablaPromociones.setBounds(10, 131, 900, 170);
		add(tablaPromociones);

		JScrollPane scrollPromociones = new JScrollPane(tablaPromociones,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPromociones.setBounds(10, 131, 900, 170);
		add(scrollPromociones);

		JLabel lblSeleccioneUnCriterio = new JLabel("Seleccione un criterio de búsqueda");
		lblSeleccioneUnCriterio.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccioneUnCriterio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSeleccioneUnCriterio.setBounds(10, 51, 225, 14);
		add(lblSeleccioneUnCriterio);

		rdbtnVigentes = new JRadioButton("Vigentes", false);
		rdbtnVigentes.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnVigentes.setBackground(Color.WHITE);
		rdbtnVigentes.setBounds(22, 92, 109, 23);
		add(rdbtnVigentes);

		rdbtnInactivas = new JRadioButton("Inactivas", false);
		rdbtnInactivas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnInactivas.setBackground(Color.WHITE);
		rdbtnInactivas.setBounds(133, 92, 109, 23);
		add(rdbtnInactivas);

		rdbtnTodas = new JRadioButton("Todas", true);
		rdbtnTodas.setFont(new Font("Tahoma", Font.PLAIN, 13));
		rdbtnTodas.setBackground(Color.WHITE);
		rdbtnTodas.setBounds(252, 92, 109, 23);
		add(rdbtnTodas);

		group = new ButtonGroup();
		group.add(rdbtnInactivas);
		group.add(rdbtnVigentes);
		group.add(rdbtnTodas);

		btnObtener = new JButton("Obtener");
		btnObtener.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnObtener.setBackground(Color.BLUE);
		btnObtener.setBounds(402, 82, 109, 38);
		add(btnObtener);

		JLabel lblDetallesDePromocion = new JLabel("Detalles de Promocion");
		lblDetallesDePromocion.setFont(new Font("Tahoma", Font.ITALIC, 16));
		lblDetallesDePromocion.setBounds(10, 347, 186, 29);
		add(lblDetallesDePromocion);

		modeloTablaDetalles = new DefaultTableModel(null, nombreColumnasDetalle);
		tablaDetalles = new JTable(modeloTablaDetalles);
		tablaDetalles.setBounds(10, 380, 300, 150);
		add(tablaDetalles);

		JScrollPane scrollDetalles = new JScrollPane(tablaDetalles, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollDetalles.setBounds(10, 380, 300, 150);
		add(scrollDetalles);

	}

	public JTable getTablaPromociones() {
		return tablaPromociones;
	}

	public void setTablaPromociones(JTable tablaPromociones) {
		this.tablaPromociones = tablaPromociones;
	}

	public DefaultTableModel getModeloTablaPromos() {
		return modeloTablaPromos;
	}

	public void setModeloTablaPromos(DefaultTableModel modeloTablaPromos) {
		this.modeloTablaPromos = modeloTablaPromos;
	}

	public String[] getNombreColumnasPromos() {
		return nombreColumnasPromos;
	}

	public void setNombreColumnasPromos(String[] nombreColumnasPromos) {
		this.nombreColumnasPromos = nombreColumnasPromos;
	}

	public ButtonGroup getGroup() {
		return group;
	}

	public void setGroup(ButtonGroup group) {
		this.group = group;
	}

	public JTable getTablaDetalles() {
		return tablaDetalles;
	}

	public void setTablaDetalles(JTable tablaDetalles) {
		this.tablaDetalles = tablaDetalles;
	}

	public DefaultTableModel getModeloTablaDetalles() {
		return modeloTablaDetalles;
	}

	public void setModeloTablaDetalles(DefaultTableModel modeloTablaDetalles) {
		this.modeloTablaDetalles = modeloTablaDetalles;
	}

	public String[] getNombreColumnasDetalle() {
		return nombreColumnasDetalle;
	}

	public void setNombreColumnasDetalle(String[] nombreColumnasDetalle) {
		this.nombreColumnasDetalle = nombreColumnasDetalle;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JButton getBtnObtener() {
		return btnObtener;
	}

	public void setBtnObtener(JButton btnObtener) {
		this.btnObtener = btnObtener;
	}

	public JRadioButton getRdbtnVigentes() {
		return rdbtnVigentes;
	}

	public void setRdbtnVigentes(JRadioButton rdbtnVigentes) {
		this.rdbtnVigentes = rdbtnVigentes;
	}

	public JRadioButton getRdbtnInactivas() {
		return rdbtnInactivas;
	}

	public void setRdbtnInactivas(JRadioButton rdbtnInactivas) {
		this.rdbtnInactivas = rdbtnInactivas;
	}

	public JRadioButton getRdbtnTodas() {
		return rdbtnTodas;
	}

	public void setRdbtnTodas(JRadioButton rdbtnTodas) {
		this.rdbtnTodas = rdbtnTodas;
	}
}
