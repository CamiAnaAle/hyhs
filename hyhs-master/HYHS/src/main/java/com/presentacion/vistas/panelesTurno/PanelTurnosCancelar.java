package com.presentacion.vistas.panelesTurno;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class PanelTurnosCancelar extends JPanel {
	private JTextField textField;
	private DefaultTableModel modeloTabla;
	private JTable tablaDeTurnos;
	private String[] nombreColumnas = {"Nombre", "Apellido","Dia", "Hora", "Servicio","Profesional"};
	private JLabel lblTurnosActuales;

	/**
	 * Create the panel.
	 */
	public PanelTurnosCancelar() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JLabel lblIngreseNombreDe = new JLabel("Ingrese Nombre de Cliente");
		lblIngreseNombreDe.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIngreseNombreDe.setBounds(37, 35, 180, 19);
		add(lblIngreseNombreDe);
		
		textField = new JTextField();
		textField.setBounds(259, 34, 140, 20);
		add(textField);
		textField.setColumns(10);
		
		modeloTabla = new DefaultTableModel(null, nombreColumnas);
		
		tablaDeTurnos = new JTable(modeloTabla);
		tablaDeTurnos.setBounds(37, 177, 309, 79);
		add(tablaDeTurnos);
		
		JScrollPane scroll = new JScrollPane(tablaDeTurnos,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(37, 91, 572, 171);
		add(scroll);
		
		lblTurnosActuales = new JLabel("Turnos Actuales");
		lblTurnosActuales.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTurnosActuales.setBounds(37, 60, 140, 20);
		add(lblTurnosActuales);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(409, 33, 89, 23);
		add(btnBuscar);
		
		JButton btnCancelar = new JButton("Cancelar Turno");
		btnCancelar.setBounds(619, 239, 130, 23);
		add(btnCancelar);

	}
}
