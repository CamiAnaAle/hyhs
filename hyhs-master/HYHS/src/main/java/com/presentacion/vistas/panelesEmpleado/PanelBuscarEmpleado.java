package com.presentacion.vistas.panelesEmpleado;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.modelo.clases.Cliente;
import com.modelo.clases.Empleado;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelBuscarEmpleado extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtIngreseTexto;
	private DefaultTableModel modelEmpleado;
	private String[] nombreColumnas = { "Legajo","Nombre", "Apellido", "DNI", "Telefono", "Celular","Mail", "Fecha Nacimiento","Direccion", "Localidad", "Fecha Ingreso" ,"Sueldo","Estado"};
	private JTable tablaClientes;	
	private JButton btnBuscar;
	
	public static PanelBuscarEmpleado INSTANCE;
	
	public static PanelBuscarEmpleado getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PanelBuscarEmpleado();
			return new PanelBuscarEmpleado();
		} else
			return INSTANCE;
	}
	
	public PanelBuscarEmpleado() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel pnlBuscar = new JPanel();
		pnlBuscar.setLayout(null);
		pnlBuscar.setBackground(Color.WHITE);
		pnlBuscar.setBounds(0, 0, 968, 504);
		add(pnlBuscar);
		
		JLabel lblListado = new JLabel();
		lblListado.setText("LISTADO");
		lblListado.setForeground(SystemColor.windowBorder);
		lblListado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblListado.setBounds(15, 16, 101, 22);
		pnlBuscar.add(lblListado);
		
		JLabel lblIngreseTexto = new JLabel("INGRESE TEXTO:");
		lblIngreseTexto.setBounds(204, 18, 162, 20);
		pnlBuscar.add(lblIngreseTexto);
		
		txtIngreseTexto = new JTextField();
		txtIngreseTexto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
			}
		});
		txtIngreseTexto.setColumns(10);
		txtIngreseTexto.setBounds(368, 15, 214, 26);
		pnlBuscar.add(txtIngreseTexto);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(15, 67, 832, 395);
		pnlBuscar.add(scrollPane);
		
		modelEmpleado = new DefaultTableModel(null, nombreColumnas);
		tablaClientes = new JTable(modelEmpleado);

		tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPane.setViewportView(tablaClientes);
		
		
		 btnBuscar = new JButton("BUSCAR");
		 btnBuscar.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 	}
		 });
		btnBuscar.setBounds(618, 14, 115, 29);
		pnlBuscar.add(btnBuscar);
	}
	
	public JButton getBtnBuscar()
	{
		return btnBuscar;
	}

	public JTextField getTxtIngreseTexto() {
		return txtIngreseTexto;
	}



	public void setTxtIngreseTexto(JTextField txtIngreseTexto) {
		this.txtIngreseTexto = txtIngreseTexto;
	}



	public DefaultTableModel getModelEmpleado() {
		return modelEmpleado;
	}



	public void setModelEmpleado(DefaultTableModel modelEmpleado) {
		this.modelEmpleado = modelEmpleado;
	}



	public String[] getNombreColumnas() {
		return nombreColumnas;
	}



	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}



	public JTable getTablaClientes() {
		return tablaClientes;
	}



	public void setTablaClientes(JTable tablaClientes) {
		this.tablaClientes = tablaClientes;
	}



}
